<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Pregătește-te de sezonul rece";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Unelte de sezon</a></li>
        <li class="active"><span>Pregătește-te de sezonul rece</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
                SECTION 0
            ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header">
                    <div class="project_description_title">
                        Pregătește-te de sezonul rece
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            De câte ori ți-ai propus să nu mai uiți de racletă sau antigel când vine prima zăpadă sau înghețul? Sau de sare, pe care s-o presari pe aleea din fața blocului sau a casei?
                        </p>
                        <p>
                            Sigur te-ai gândit în fiecare an că poate ai avea nevoie și de o freză de zăpadă. Nu te lăsa luat pe nepregătite și în iarna aceasta! Chiar dacă mai e până la iarnă, micile investiții de acum te vor răsplăti mai târziu. Gândește-te numai ce bine va fi de Crăciun, când vei avea mai mulți bani de cadouri!
                        </p>
                        <p>
                            Îți aduci aminte ce frumoasă era, în copilărie, prima ninsoare? Primul om de zăpadă, cu o mătură ponosită sau o creangă pe post de mână și un morcov strâmb în loc de nas? Fularul cu țurțuri și picioarele înghețate în sania cu care coborai în viteză derdelușul?
                        </p>
                        <p>
                            Păstrează-ți amintirile și bucură-te în continuare de venirea iernii. Pregătește-te din timp și lasă grijile deoparte! Găsești la noi încă de pe acum tot ce-ți trebuie ca să înfrunți frigul și zăpada când vor veni, dar să mai ai și timp de copilărit.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top: 0px">
                    <a href="#start" class="discover">Descoperă <img
                                src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_1.2/ambianta-proiect1.2.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->


        <!-- ------------------------------------------------------------------
                 SECTION 1
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProduct('131452')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('131452'); ?>">
                    <div class="yellow_hotspot" data-ref="131452" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">FREZĂ DE ZĂPADĂ 2000 W</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FREZĂ DE ZĂPADĂ 5.5 CP",
                "ref" => "131954",
                "sticker" => 'sticker_recomandam.png'

            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FREZĂ DE ZĂPADĂ 6.5 CP",
                "ref" => "131955",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FREZA DE ZAPADA 600 5.5CP",
                "ref" => "141188",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/gradina/masini-gradina-toamna-iarna.html";
            require('assets/partials/discover.php');
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 2
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('135918,135921')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('135921'); ?>">
                    <div class="yellow_hotspot" data-ref="135918" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">RACLETĂ CU PERIE 9 CM</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "RACLETĂ 7,5 CM",
                "ref" => "135916",
                "alternative" => array("135919", "135922"),

            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "RACLETĂ CU MÂNER ABS 9 CM",
                "ref" => "135920",
            ));
            ?>
        </div>


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "RACLETĂ CU MĂNUŞI 15 CM",
                "ref" => "135923",
            ));
            ?>
        </div>


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FOLIE PROTECŢIE PARBRIZ 175 X 70 CM",
                "ref" => "135917",
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION  3
         ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MATERIAL ANTIDERAPANT, 3 KG",
                "ref" => "125771",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LICHID DE PARBRIZ PENTRU IARNĂ 5 L",
                "ref" => "134480",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOLUŢIE DE DEZGHEŢARE PARBRIZE",
                "ref" => "143516",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ANTIGEL CONCENTRAT 1 KG",
                "ref" => "141865",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOLUŢIE DE DEZGHEŢAT YALE",
                "ref" => "143517",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOLUŢIE DE DEZGHEŢAT YALE",
                "ref" => "143518",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOLUŢIE ANTIABURIRE GEAMURI",
                "ref" => "143515",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 2x1 -->
        <div class="block" data-size="1x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/unelte/accesorii-auto.html?filtre/categorie/accesorii-auto-iarna/pret/2,68";
            require('assets/partials/discover.php');
            ?>
        </div>

    </div>

    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>