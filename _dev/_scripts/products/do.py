
import os
import io
import json
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter

xls_file = "input/bricodepot.xlsx"

products = []

def buildFromXLS():
    
    products = []

    print 'loading ' + xls_file + '... '
    workbook = load_workbook(filename=xls_file)
    print 'loaded'
    worksheet = workbook['Sheet1']

    print 'generating products...'
    row_index = 1
    for row in worksheet.rows:
        if row_index != 1:
            cell_index = 1
            product = {}
            for cell in row:
                column_letter = get_column_letter(cell_index)
                cell_index += 1
                if column_letter == 'A':
                    product['cardId'] = cell.value
                if column_letter == 'B':
                    product['productRef'] = cell.value
                if column_letter == 'C':
                    product['title'] = cell.value
                if column_letter == 'D':
                    product['desc'] = cell.value
                    if product['desc'] != None:
                        product['desc'] = product['desc'].replace('\n', '<br>')
                if column_letter == 'E':
                    if cell.value != None:
                        product['cardImg'] = cell.value
                    else:
                        product['cardImg'] = 'no_image.jpg'
                if column_letter == 'F':
                    if cell.value != None:
                        product['productImg'] = cell.value
                    else:
                        product['productImg'] = 'no_image.jpg'
                if column_letter == 'G':
                    product['project'] = cell.value
                if column_letter == 'H':
                    product['category'] = cell.value

            if product['cardId'] != None:
                products.append(product)

        row_index += 1

    print 'parsed ' + str(len(products)) + ' products'

    return products

def writeToJSON (json_data, json_path):
    with io.open(json_path, 'w', encoding='utf8') as json_file:
        data = json.dumps(json_data, ensure_ascii=False, encoding='utf8')
        json_file.write(unicode(data))


products = buildFromXLS();
writeToJSON(products, 'output/products.json')
print 'products saved'

