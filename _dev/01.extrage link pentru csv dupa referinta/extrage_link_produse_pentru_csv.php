<pre>
<?php
	set_time_limit(0);

	// preia fisier
	$csv = file_get_contents('id_produse_in.csv');
	$separator = ",";
	// sparge liniile
	$csv = explode("\n", $csv);
	// preia header din prima linie
	list($header_id, $header_link) = explode($separator ,$csv[0]);
	unset($csv[0]);
	// preaia valori din liniile urmatoare
	foreach($csv as $k => $v){
		list($id, $link) = explode($separator, $csv[$k]);
		$csv[$k] = array(
			"id" => $id,
			"link" => $link
		);
	}

	//
	//preia linkurile pentru fiecare
	$pattern = '/(<a href=")(.*)(" class="product_name">)(.*)(<\/a>)/';

	foreach($csv as $k => $v){
		$csv[$k]['link'] = '';
		if($content =file_get_contents('https://www.bricodepot.ro/orhideea/cauta?Query=' . $csv[$k]['id'])){		
			preg_match($pattern, $content, $matches);
			if(isset($matches[2])){	
				$csv[$k]['link'] = 'https://www.bricodepot.ro' . $matches[2];
			}
		}
	}

	//salveaza intr-un fisier datele
	$data = "$header_id;$header_link;dummy" ;
	foreach($csv as $k => $v){
		$data .= "\n" .$csv[$k]['id'] . ";" . $csv[$k]['link'] . ";" ;
	}

	file_put_contents('id_produse_out.csv', $data);

	print_r($csv);
	
?>