<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);
ini_set('max_execution_time', 0);


$folder = "sourcesTxt/new/";
$refsXml = "allKtaRefs/refs.xml";
$mapsXml = "allKtaRefs/mapRefs.xml";
$datePrices = file_get_contents('forDate/date.txt');
// date_default_timezone_set('Europe/Minsk');
$er=array();

function getClientFilesForToday($folder)
{
	$client_files = array();
	$datePrices = file_get_contents('forDate/date.txt');
	if(is_dir($folder))
	{
		if($handle = opendir($folder))
		{
			while(false !==($intr = readdir($handle)))
			{
				if($intr != '.' && $intr !='..')
				{
					// if(stripos($intr, @date("Y_m_d",time())) !== false)
					if(stripos($intr, $datePrices) !== false)
					{
					 $client_files[] = $intr; 
					}
				}
			}
			if (count($client_files)==0) {
				return false;
			}
			return $client_files;
		}
	}
	return false;
}


function getInfosFromFile($filename)
{
	$allFileRefs = array();
	//$fileContent = file_get_contents($filename);
	if (($handle = fopen($filename, "r")) !== FALSE) {
	
		 while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
	        $num = count($data);
	      
	        //print "<pre>".print_r($data,2)."</pre>";

	        if(count($data)>9)
			{
				$allFileRefs[trim($data[0])] = $data;
			}
			else
			{
				return false;
			}
	    }
	    fclose($handle);
	    return $allFileRefs;
	}
	return false;
	
}

function correspondingStores()
{
	$allStores = array(
		"arad" => "710",
		"baneasa" => "706",
		"braila" => "709",
		"brasov" => "704",
		"calarasi" => "716",
		"constanta" => "707",
		"drobeta" => "715",
		"focsani" => "711",
		"militari" => "701",
		"oradea" => "713",
		"orhideea" => "703",
		"pantelimon" => "702",
		"pitesti" => "708",
		"ploiesti" => "705",
		"suceava" => "712"
		);
	return $allStores;
}  

$allFiles = getClientFilesForToday($folder);
$allStores = correspondingStores();
/*foreach ($allFiles as $key => $file) {
print "<pre>".print_r(getInfosFromFile($folder.$file),2)."</pre>";
}*/
$open = fopen("missingRefs.txt", "wr");
$open2 = fopen("9999Prices.txt", "wr");
if(file_exists($refsXml))
{
	//first xml 
	$xml_pars = new DOMDocument();
	$xml_pars->load($refsXml);
	$new2 = new DOMXPath($xml_pars);
	$refs = $new2->query('//refs/r');

	//second xml
	$arrivages = array();

	if(file_exists('arrivageJson.txt'))
	{
		$arrivagesJson = file_get_contents('arrivageJson.txt');
		$arrivages = json_decode($arrivagesJson,true);
	}
	
	if ($allFiles!==false) 
	{
		foreach ($allStores as $key => $file) {

			$fileToParse = "";
			$fileMatch = $file."_export_";
			//$fileMatch = $file."_export_".@date("Y_m_d",time())."_";
			foreach ($allFiles as $k => $todayFile) {
				if(stripos($todayFile, $fileMatch)!==false)
				{
					$fileToParse = $todayFile;
				}
				
			}
			if($fileToParse == "")
			{
				print "File not found  with '".$fileMatch."' <br/>";
			}
			else
			{

				fwrite($open, "----".$key."----\n");
				fwrite($open2, "----".$key."----\n");
				$elem = new DOMDocument('1.0','UTF-8');
				$elem->xmlStandalone = true;
				$elem ->formatOutput = true;
				$root = $elem->createElement('prices');
				$elem->appendChild($root);
				$currentArrFile = getInfosFromFile($folder.$fileToParse);
				$distinctRefs = array();
				if($currentArrFile == false)
				{
				 	print  "File '".$fileToParse."' - parse error <br/>";
				}
				else
				{
					foreach($refs as $ref)
					{
						$hasPrice = false;
						$priceColor = '';
						if ($ref->hasAttribute("id"))
						{
							$id=trim($ref->getAttribute('id'));

							if(isset($currentArrFile[$id]))
							{
								
								$unitPrice = $currentArrFile[$id][6];
								$packagePrice = $currentArrFile[$id][8];
								$unit = $currentArrFile[$id][9];
								$priceColor = "#".substr(dechex((float)$ref->getAttribute('priceColor')),2,6);
								$hasPrice = true;

								if($unitPrice == "9999")
								{
									$has9999Price = true;
								}
								else
								{
									$has9999Price = false;
								}	
							}
							else
							{
								if(strpos($id, "+") === false)
								{
									$unitPrice = $ref->getAttribute('price');
									$packagePrice = $ref->getAttribute('packagePrice');
									$unit = $ref->getAttribute('unit');
									$priceColor = "#".substr(dechex((float)$ref->getAttribute('priceColor')),2,6);
									$hasPrice = false;
								}
								else
								{
									$hasPrice = true;
									$tmpRefsArr = explode("+", $id);
									$currentSetPriceUnit = 0;
									$currentSetPricePackage = 0;
									foreach ($tmpRefsArr as $refKey => $refM) {
										if(isset($currentArrFile[$refM]))
										{
											$currentSetPriceUnit += $currentArrFile[$refM][6];
											$currentSetPricePackage += $currentArrFile[$refM][8];
										}
										else
										{
											$currentRefTags = $new2->query('//refs/r[@id="'.$refM.'"]');
											if($currentRefTag->length >0)
											{
												$currentSetPriceUnit += $currentRefTags->item(0)->getAttribute('price');
												$currentSetPricePackage += $currentRefTags->item(0)->getAttribute('packagePrice');
												// $priceColor = $currentRefTags->item(0)->getAttribute('priceColor');
											}
										}
										
									}
									$currentRefTags = $new2->query("//refs/r[@id=\"".$tmpRefsArr[0]."\"]");
									
									if ($currentRefTags->length > 0) {
										$priceColor = "#".substr(dechex((float)$currentRefTags->item(0)->getAttribute('priceColor')),2,6);
									} else {
										$priceColor = "#".substr(dechex((float)$ref->getAttribute('priceColor')),2,6);
									}
									
									if($currentSetPriceUnit == 0)
									{
										$currentSetPriceUnit = "";
									}
									if($currentSetPricePackage == 0)
									{
										$currentSetPricePackage = "";
									}

									$unitPrice = $currentSetPriceUnit;
									$packagePrice = $currentSetPricePackage;
									// $unit = $ref->getAttribute('unit');
									$unit = "set";
								}
							}

							if(!in_array($id, $distinctRefs))
							{
								$prodrf = $elem->createElement('p');
								$root->appendChild($prodrf);

								$idprod = $elem->createAttribute('id');
								$prodrf->appendChild($idprod);
								$idprod->value = $id;

								$priceprod = $elem->createAttribute('price');
								$prodrf->appendChild($priceprod);
								$priceprod->value = $unitPrice;

								$pricepackage = $elem->createAttribute('packagePrice');
								$prodrf->appendChild($pricepackage);
								$pricepackage->value = $packagePrice;

								$unitAttr = $elem->createAttribute('unit');
								$prodrf->appendChild($unitAttr);
								$unitAttr->value = $unit;

								$priceColorx = $elem->createAttribute('priceColor');
								$prodrf->appendChild($priceColorx);
								$priceColorx->value = $priceColor;

								if(isset($arrivages[$id]) && isset($currentArrFile[$id]))
								{
									$stock = $elem->createAttribute('stock');
									$prodrf->appendChild($stock);
									
									if($currentArrFile[$id][2] == '0' || $currentArrFile[$id][2] == '')
									{
										$stock->value = "initial";
									}
									else
									{
										$stock->value = $currentArrFile[$id][2];
										// print "ID : ".$currentArrFile[$id][0]."<br>";
										// print "Stock : ".$currentArrFile[$id][2]."<br>";
									}
								}
								

								if($hasPrice === false)
								{
									
									fwrite($open, $id." - ".$refsXml."\n");
								}
								if(isset($has9999Price) && $has9999Price === true)
								{
									fwrite($open2, $id."\n");
								}
								array_push($distinctRefs,$id);
							}
						}
						else
						{
							$er['RefsId'] = "Ref ".$ref." hasn't id !";
						}
					
					}

					$elem->save("outputXmls/default_".$key.".xml");
				}
			}

		}
		
	}
	else
	{
		$er['ClientFiles'] = "Error read client files ! Not files with date  ".$datePrices." ! Please check file 'forDate/date.txt' !!!";
	}
}
else
{
	$er['refs'] = "File refs.xml not found !";
}
if(count($er)>0)
{
	print "<pre>".print_r($er,2)."</pre>";
}
else
{
	print "Success !<br/>";
	print "Count of files : ".count($allStores);

}
fclose($open);
fclose($open2);
?>
