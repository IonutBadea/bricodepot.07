<pre>
<?php





function csv_to_array($filename, $delimiter=',', $enclosure='"', $escape = '\\')
{
	if(!file_exists($filename) || !is_readable($filename)) return false;

	$header = null;
	$data = array();
	$lines = file($filename);

	foreach($lines as $line) {
	
	  	if(trim($line ) != "" ){
		    $values = str_getcsv($line, $delimiter, $enclosure, $escape);
		    if(!$header) $header = $values;
		    else $data[] = array_combine($header, $values);
		}
	}

  	return $data;
}

$csv = csv_to_array('base.csv',';');


$refsxml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'.  "\n" .'<refs>' . "\n";

foreach($csv as $k => $v){
	$refsxml .= '<r id="' . $v['id'] . '" price="" packagePrice="" unit=""/>' . "\n";
}

$refsxml .= '</refs>';

file_put_contents('new_refs.xml', $refsxml);



// print_r($csv);
print_r($refsxml);

?>
</pre>