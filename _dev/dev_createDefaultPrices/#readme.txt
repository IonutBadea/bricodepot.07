Pasi :
1. Pui fisierele xml produse de noi :
	
	1.1. Pui fisierele xml de preturi (exemplu : 0001p.xml) in folderul 1.createRefsXmlForPricesFromPrices/xml (le iei din 05.out/catalogue/common/prices)
	1.2. Pui fisierele xml de mapuri (exemplu : 0001.xml ) in folderul 2.createRefsXmlForPricesFromMaps/xml (le iei din 05.out/catalogue/flash/data)

2. Pui fisierele furnizate de ei (O intrebi pe Laura Voica de ele ): 

	2.1. Golesti folderul 3.createDefaultXml\sourcesTxt\new si folderul 3.createDefaultXml\sourcesTxt ( nu sterge folderul 'new' )
	2.2. Pui fisierele .txt cu preturi de la client in folderul 3.createDefaultXml\sourcesTxt (Fisiere pe care ti le va da Laura Voica)
	2.3. Schimbi data pentru noile fisiere .txt in fisierul '3.createDefaultXml/forDate/date.txt'
	2.4. Daca clientul furnizeaza fisierele .xls cu preturi arivaj si KVI (Aceasta informatie o iei de la Laura Voica ) , le exporti csv cu delmitatorul de coloane "^" si le pui in folderul 3.createDefaultXml/xls cu numele arrivage.csv si KVI.csv

3. Exporti defaulturile ruland scriptul 04.prod/dynamicPricing/index.php (de langa acest readme.txt)

4. Verifici 3.createDefaultXm/9999Prices.txt si 3.createDefaultXm/missingRefs.txt. (Acestea vor contine referintele care au preturi cu 9999 respectiv daca lipsesc preturi pentru anumite referinte )

5. Fisierele finale sunt in 3.createDefaultXm/outputXmls

6. Copiezi fisierele finale in catalog in 05.out/catalogue/common/prices

Bafta.