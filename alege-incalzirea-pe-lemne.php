<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Alege încăzirea pe lemne";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Soluții de încălzire potrivite pentru tine</a></li>
        <li class="active"><span>Alege încăzirea pe lemne</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
                SECTION 0
            ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header">
                    <div class="project_description_title">
                        Alege încăzirea pe lemne
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            Toamna e anotimpul ideal pentru o cană de vin fiert cu scorțișoară și mere coapte. Dacă simți deja mirosul de măr copt și ți-e poftă de nuci uscate pe sobă, nu mai sta pe gânduri! Te putem ajuta să-ți transformi acest vis în realitate, dar și să te bucuri de un confort mai mare atunci când e frig.
                        </p>
                        <p>
                            Îți propunem produsele pe care le avem și noi în vedere pentru un proiect de îmbunătățire a locuinței. Nu suntem chiar atât de diferiți, nu-i așa? Ce spui de o sobă de bucătărie din teracotă, în care lemnele trosnesc domol, iar merele se coc încetișor pe poliță?
                        </p>
                        <p>
                            Pentru o atmosferă romantică și un stil retro, poți alege din gama variată de șeminee. La căldura unui șemineu, pe canapea și la lumina difuză a unei veioze, îți poți savura vinul fiert cu scorțișoară sau cana de ceai cu miere și lămâie. Sau poate ciocolata caldă? Cappuccino? Lapte cu cacao? Posibilitățile sunt nenumărate!
                        </p>
                        <p>
                            Iar dacă vrei să faci niște reparații produselor pe care deja le ai, vei găsi la noi tot ce ai nevoie. N-am uitat nici de accesorii: lemne, cărbuni sau peleți. Te așteptăm, grăbește-te!

                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top:0px">
                    <a href="#start" class="discover">Descoperă <img
                                src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_2.2/ambianta-proiect2.2.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->


        <!-- ------------------------------------------------------------------
                 SECTION 1
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProduct('127081')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('127081'); ?>">
                    <div class="yellow_hotspot" data-ref="127081" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">SOBĂ BUCĂTĂRIE TK 205 6 KW</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOBĂ TERACOTĂ 7 KW 44 X 44 X 112 CM",
                "ref" => "146395",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOBĂ BUCĂTĂRIE NARODNA 7 KW",
                "ref" => "134321",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "BRICHETE XXL 10 KG",
                "ref" => "137820",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LEMN DE FOC FAG 1 X 1 X 1 M",
                "ref" => "137081",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 2
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProduct('136536')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('136536'); ?>">
                    <div class="yellow_hotspot" data-ref="136536" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">ŞEMINEU Z1 ALPINA 8 KW</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ŞEMINEU JAR 8 KW",
                "ref" => "146946",
                "sticker" => 'sticker_alte-optiuni.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "TERMOŞEMINEU MIRANO H 22 KW",
                "ref" => "136981",
                "sticker" => 'sticker_alte-optiuni.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FOCAR FONTĂ CU CLAPETĂ 14 KW",
                "ref" => "146396",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FOCAR OŢEL SERPENTINA 16 KW",
                "ref" => "140328",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>


        <!-- ------------------------------------------------------------------
            SECTION 3
        ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "BRICHETE RUF 10KG",
                "ref" => "137712",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "BRICHETE FAG",
                "ref" => "140336",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LEMN DE FOC FAG 1 X 1 X 1,8 M",
                "ref" => "137821",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/incalzire-racire-si-instalatii/incalzire-cu-lemne.html";
            require('assets/partials/discover.php');
            ?>
        </div>



    </div>

    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>