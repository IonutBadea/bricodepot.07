<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Alege un sistem electric eficient";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Sisteme electrice și iluminat</a></li>
        <li class="active"><span>Alege un sistem electric eficient</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
                SECTION 0
            ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header">
                    <div class="project_description_title">
                        Alege un sistem electric eficient
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            Casele evoluează odată cu nevoile noastre. Vrei ca locuința ta să fie „verde”, confortabilă, să aibă stil, să fie organizată și să arate foarte bine. Îți iubești casa, de aceea te gândești mereu cum ai putea să o faci mai bună. Îți oferim idei, soluții inteligente și puțină inspirație. Mai ales când e vorba despre tot ce înseamnă sistem electric.
                        </p>
                        <p>
                            Descoperi, în timp, după ce în casa ta și-au făcut loc diverse dispozitive și aparate electrocasnice, ca nu-ți mai ajung prizele și întrerupătoarele. Sau că cele existente sunt deja uzate,crăpate, topite pe alocuri și periculoase pentru utilizarea zilnică. Nu crezi că e timpul să modernizezi prizele și întrerupătoarele și să le aduci la nivelul echipamentelor electronice performante din casa ta?
                        </p>
                        <p>
                            Indiferent de proiect, te ajutăm să-ți fie cât mai simplu și să nu depășești bugetul alocat. E posibil chiar să ai o surpriză și să-ți mai rămână bani de alte mici lucrări prin casă. Vezi ce ți se potrivește și vino în magazinele noastre să te convingi de oferta variată de produse și modele.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top:0px">
                    <a href="#start" class="discover">Descoperă <img
                                src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_3.1/ambianta-proiect.3.1.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->


        <!-- ------------------------------------------------------------------
                 SECTION 1
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('59010,58951,58912,58922')">
                    <img class="map__image" src="assets/img/PROJECT_3.1/ambianta-gama-1.jpg" style="cursor:default;">
                    <div class="yellow_hotspot" data-ref="59010,58951,58912,58922" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">Sistem alb</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS SUPORT 2 POSTURI",
                "ref" => "59010",
                "alternative" => array("59007","59011","82261")
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PLACĂ ALBĂ 2 POSTURI TOP",
                "ref" => "58951",
                "alternative" => array("58950","58952","58953","82262")
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ÎNTRERUPĂTOR MODULAR 16A 1 POST",
                "ref" => "58912",
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRIZĂ SCHUKO MODULARĂ 16A 2 POSTURI",
                "ref" => "58922",
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 2
         ------------------------------------------------------------------ -->

        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS RAMĂ DECORATIVĂ CU CAPAC",
                "ref" => "69297",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS ÎNTRERUPĂTOR MODULAR CU LED SYSTEM",
                "ref" => "58913",
                "alternative" => array("58933"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PRIZĂ TV MODULARĂ REZISTIVĂ DIRECTĂ SYSTEM",
                "ref" => "58923",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PRIZĂ TELEFON MODULARĂ RJ11",
                "ref" => "58925",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PLACĂ ALBĂ 3 POTURI VIRNA",
                "ref" => "123685",
                "alternative" => array("123683","123684","123686","123687"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS ÎNTRERUPĂTOR CAP SCARĂ 16 A",
                "ref" => "58914",
                "alternative" => array("58934"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS VARIATOR ROTATIV 900W ALB",
                "ref" => "58927",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PRIZĂ DATE MODULARĂ RJ45",
                "ref" => "58926",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 3
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">

                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('59010,58959,58932,58942')">
                    <img class="map__image" src="assets/img/PROJECT_3.1/ambianta-gama-2.jpg" style="cursor:default;">
                    <div class="yellow_hotspot" data-ref="59010,58959,58932,58942" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">Sistem negru</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS SUPORT 2 POSTURI",
                "ref" => "59010",
                "alternative" => array("59007","59011","82261"),
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PLACĂ NEAGRĂ 2 POSTURI TOP",
                "ref" => "58959",
                "alternative" => array("58958","58961","58960","86433"),
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS ÎNTRERUPĂTOR 16 A 1P NEGRU",
                "ref" => "58932",
            ));
            ?> 
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PRIZĂ SCHUKO 16 A NEAGRĂ",
                "ref" => "58942",
            ));
            ?>
        </div>

        <!--  ------------------------------------------------------------------ -->

        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PLACĂ NEAGRĂ 3 POSTURI VIRNA",
                "ref" => "123690",
                "alternative" => array("123688","123689","123691","123692"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS ÎNTRERUPĂTOR CAP SCARĂ/LED 16 A NEGRU",
                "ref" => "58935",
                "alternative" => array("58915"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS PRIZĂ MODULARĂ SYSTEM 10 A NEAGRĂ",
                "ref" => "58941",
                "alternative" => array("118572"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GEWISS TASTĂ FALSĂ 1 POST NEAGRĂ",
                "ref" => "58939",
                "alternative" => array("58919"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>


        <!-- ------------------------------------------------------------------
             SECTION 4
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('82118,79982,82099,82108')">
                <img class="map__image" src="assets/img/PROJECT_3.1/ambianta-gama-3.jpg" style="cursor:default;">
                    <div class="yellow_hotspot" data-ref="82118,79982,82099,82108" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">Sistem modul</p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MODUL RAMĂ FIXARE METAL 2 POSTURI",
                "ref" => "82118",
                "alternative" => array("82117","82114","82115","82116"),
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "RAMĂ DECORATIVĂ SOFT TEM 2 POSTURI ALBĂ",
                "ref" => "79982",
                "alternative" => array("79981","79983","79984","79985"),
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "INTRERUPATOR SIMPLU MODUL 1M ALB",
                "ref" => "82099",
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MODUL PRIZA SIMPLA CP ,2 M ,ALB",
                "ref" => "82108",
            ));
            ?>
        </div>

        <!--  ------------------------------------------------------------------ -->

        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "BUTON SONERIE MODULAR CU LED 10 A",
                "ref" => "82103",
                "alternative" => array("82122"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MODUL PRIZĂ CALCULATOR ALB",
                "ref" => "68425",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRIZĂ TV/ SATELIT MODULARĂ DE CAPĂT ALBĂ",
                "ref" => "82109",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MODUL PRIZĂ TELEFON 2 POSTURI ALB",
                "ref" => "68426",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!--  ------------------------------------------------------------------ -->

    </div>

    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>