<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Pregătește-ți grădina pentru iarnă";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Unelte de sezon</a></li>
        <li class="active"><span>Pregătește-ți grădina pentru iarnă</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
                SECTION 0
            ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header">
                    <div class="project_description_title">
                        Pregătește-ți grădina pentru iarnă
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            Toamna, zilele sunt mai scurte, așa că e important să-ți stabilești din timp un program de curățenie și împrospătare a grădinii. Nu știi cu ce să începi? Îți dăm o mână de ajutor.
                            Curăță mai întâi cu o greblă gazonul, straturile de flori sau legume de frunzele căzute și asigură-te că nu ai plante care nu sunt rezistente la frig. Dacă ai, plantează bulbii în ghivece cu pământ proaspăt și așează-le în locuri întunecoase.
                        </p>
                        <p>
                            Aprovizionează-te din timp cu lemne pentru iarna friguroasă și așează-le într-un loc ferit de intemperii. Alege un fierastrau electric și o mașina de despicat lemne care îți vor face munca mai ușoară.
                        </p>
                        <p>
                            Pentru a strange frunzele căzute în grădină mai rapid, echipează-te cu un aspirator electric.
                        </p>
                        <p>
                            Îți e dor de serile călduroase și luminoase petrecute în grădină cu familia? Iarna nu este un impediment pentru că la noi găsești încălzitor de terasă și soluții de iluminat exterior care aduc atmosfera de vară în căminul tău.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top: 0px">
                    <a href="#start" class="discover">Descoperă <img
                                src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_1.1/ambianta-proiect1.1.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->

        <!-- ------------------------------------------------------------------
                SECTION 1
            ------------------------------------------------------------------ -->


        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('143460,143459')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('143460'); ?>">
                    <div class="yellow_hotspot" data-ref="143460" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">MASINA DE DESPICAT LEMNE 1500 W</p>
                    </div>
                    <div class="addontext_ambianta addontext_ambianta--right">
                        Puternice, durabile si eficiente!
                        <br/>
                        Alege modelul potrivit
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FIERĂSTRĂU ELECTRIC 1 800 W",
                "ref" => "128246",
                "sticker" => 'sticker_recomandam.png',
                "badge" => "badge-preturi-mici.jpg",
                "price" => array(
                    'old_price' => '164.00',
                    'unit' => 'buc',
                    'currency' => 'Lei'
                )
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FERĂSTRĂU ELECTRIC 2000 W ÎN LINIE",
                "ref" => "144035",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MOTOFERĂSTRĂU PE BENZINĂ 45 CC",
                "ref" => "144807",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "MOTOFERĂSTRĂU PE BENZINĂ MACALLISTER 40,1 CC",
                "ref" => "144029",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>


        <!-- ------------------------------------------------------------------
                 SECTION 2
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProduct('136929')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('136929'); ?>">
                    <div class="yellow_hotspot" data-ref="136929" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">FIERĂSTRĂU GARD VIU 450 W</p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "FIERĂSTRĂU GARD VIU 520 W",
                "ref" => "136930",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "DISPOZITIV PENTRU ASCUŢIREA LANŢULUI 220 W",
                "ref" => "144027",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>


        <!-- 2x1 -->
        <div class="block" data-size="2x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/gradina/masini-gradina-toamna-iarna.html";
            require('assets/partials/discover.php');
            ?>
        </div>


        <!-- ------------------------------------------------------------------
                 SECTION 3
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProduct('136955')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('136955'); ?>">
                    <div class="yellow_hotspot" data-ref="136955" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">GREBLĂ PLASTIC DE PELUZĂ</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GREBLĂ METALICĂ 14 DINŢI",
                "ref" => "136956",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GREBLĂ CU DINŢI DIN OŢEL",
                "ref" => "141518",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GREBLĂ MÂNER AJUSTABIL",
                "ref" => "137001",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "VERVE GREBLĂ FRUNZE 16 DINŢI",
                "ref" => "133136",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
                 SECTION 4
             ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "UNEALTĂ MULTIFUNCŢIONALĂ MAC ALLISTER 25 CC 4 ÎN 1",
                "ref" => "144032",
                "sticker" => 'sticker_alte-optiuni.png'
            ));
            ?>
        </div>


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ASPIRATOR ELECTRIC DE GRĂDINĂ MAC ALLISTER 3000W",
                "ref" => "144074",
                "sticker" => 'sticker_alte-optiuni.png'

            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ÎNCĂLZITOR TERASĂ 10 KW",
                "ref" => "132479",
                "sticker" => "sticker_accesorii.png"
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "STÂLP EXTERIOR 60 W",
                "ref" => "140680",
                "sticker" => "sticker_accesorii.png",
            ));
            ?>
        </div>


    </div>


    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>