var RestrictedArea = (function () {
    return {
        init: function () {
            var area = V7_Store.get();
            // ascunde elementele ce au atributul restricted_area daca corespund zonei
            $.each($('*[restricted-area]'), function (i, element) {
                var restricted_area = JSON.parse($(element).attr('restricted-area'));
                $.each(restricted_area, function (i, v) {
                    if (area === v) {
                        $(element).hide();
                    }
                });
            });
        },

        // prevent pentru accesare pagini specificate daca se afla in magazinele specificate
        prevent: function (pages, areas) {
            //verifica daca e cazul sa puna restrictie
            var area = V7_Store.get(),
                is_in_area = false;

            $.each(areas, function (i, v) {
                if (area === v) {
                    is_in_area = true;
                }
            });

            //pune restrictie
            if (is_in_area) {
                var href = (window.location.pathname).split('/');
                href = href[href.length - 1];
                $.each(pages, function (i, v) {
                    if ((href === v) || (href === (v + ".php"))) {
                        window.history.go(-1);
                    }
                });
            }


        }
    };
})();


window.addEventListener('load', function () {
    RestrictedArea.init();
});


window.addEventListener('load', function () {
    RestrictedArea.prevent(
        ['usi-si-pereti-de-dus-beloya'],
        ["militari", "orhideea", "arad", "braila", "focsani", "suceava", "calarasi", "drobeta"]
    );
});


