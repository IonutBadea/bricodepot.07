//axios
/* promise polyfill */
!function (t, e) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : t.ES6Promise = e()
}(this, function () {
    "use strict";

    function t(t) {
        var e = typeof t;
        return null !== t && ("object" === e || "function" === e)
    }

    function e(t) {
        return "function" == typeof t
    }

    function n(t) {
        B = t
    }

    function r(t) {
        G = t
    }

    function o() {
        return function () {
            return process.nextTick(a)
        }
    }

    function i() {
        return "undefined" != typeof z ? function () {
            z(a)
        } : c()
    }

    function s() {
        var t = 0, e = new J(a), n = document.createTextNode("");
        return e.observe(n, {characterData: !0}), function () {
            n.data = t = ++t % 2
        }
    }

    function u() {
        var t = new MessageChannel;
        return t.port1.onmessage = a, function () {
            return t.port2.postMessage(0)
        }
    }

    function c() {
        var t = setTimeout;
        return function () {
            return t(a, 1)
        }
    }

    function a() {
        for (var t = 0; t < W; t += 2) {
            var e = V[t], n = V[t + 1];
            e(n), V[t] = void 0, V[t + 1] = void 0
        }
        W = 0
    }

    function f() {
        try {
            var t = Function("return this")().require("vertx");
            return z = t.runOnLoop || t.runOnContext, i()
        } catch (e) {
            return c()
        }
    }

    function l(t, e) {
        var n = this, r = new this.constructor(p);
        void 0 === r[Z] && O(r);
        var o = n._state;
        if (o) {
            var i = arguments[o - 1];
            G(function () {
                return P(o, r, i, n._result)
            })
        } else E(n, r, t, e);
        return r
    }

    function h(t) {
        var e = this;
        if (t && "object" == typeof t && t.constructor === e) return t;
        var n = new e(p);
        return g(n, t), n
    }

    function p() {
    }

    function v() {
        return new TypeError("You cannot resolve a promise with itself")
    }

    function d() {
        return new TypeError("A promises callback cannot return that same promise.")
    }

    function _(t) {
        try {
            return t.then
        } catch (e) {
            return nt.error = e, nt
        }
    }

    function y(t, e, n, r) {
        try {
            t.call(e, n, r)
        } catch (o) {
            return o
        }
    }

    function m(t, e, n) {
        G(function (t) {
            var r = !1, o = y(n, e, function (n) {
                r || (r = !0, e !== n ? g(t, n) : S(t, n))
            }, function (e) {
                r || (r = !0, j(t, e))
            }, "Settle: " + (t._label || " unknown promise"));
            !r && o && (r = !0, j(t, o))
        }, t)
    }

    function b(t, e) {
        e._state === tt ? S(t, e._result) : e._state === et ? j(t, e._result) : E(e, void 0, function (e) {
            return g(t, e)
        }, function (e) {
            return j(t, e)
        })
    }

    function w(t, n, r) {
        n.constructor === t.constructor && r === l && n.constructor.resolve === h ? b(t, n) : r === nt ? (j(t, nt.error), nt.error = null) : void 0 === r ? S(t, n) : e(r) ? m(t, n, r) : S(t, n)
    }

    function g(e, n) {
        e === n ? j(e, v()) : t(n) ? w(e, n, _(n)) : S(e, n)
    }

    function A(t) {
        t._onerror && t._onerror(t._result), T(t)
    }

    function S(t, e) {
        t._state === $ && (t._result = e, t._state = tt, 0 !== t._subscribers.length && G(T, t))
    }

    function j(t, e) {
        t._state === $ && (t._state = et, t._result = e, G(A, t))
    }

    function E(t, e, n, r) {
        var o = t._subscribers, i = o.length;
        t._onerror = null, o[i] = e, o[i + tt] = n, o[i + et] = r, 0 === i && t._state && G(T, t)
    }

    function T(t) {
        var e = t._subscribers, n = t._state;
        if (0 !== e.length) {
            for (var r = void 0, o = void 0, i = t._result, s = 0; s < e.length; s += 3) r = e[s], o = e[s + n], r ? P(n, r, o, i) : o(i);
            t._subscribers.length = 0
        }
    }

    function M(t, e) {
        try {
            return t(e)
        } catch (n) {
            return nt.error = n, nt
        }
    }

    function P(t, n, r, o) {
        var i = e(r), s = void 0, u = void 0, c = void 0, a = void 0;
        if (i) {
            if (s = M(r, o), s === nt ? (a = !0, u = s.error, s.error = null) : c = !0, n === s) return void j(n, d())
        } else s = o, c = !0;
        n._state !== $ || (i && c ? g(n, s) : a ? j(n, u) : t === tt ? S(n, s) : t === et && j(n, s))
    }

    function x(t, e) {
        try {
            e(function (e) {
                g(t, e)
            }, function (e) {
                j(t, e)
            })
        } catch (n) {
            j(t, n)
        }
    }

    function C() {
        return rt++
    }

    function O(t) {
        t[Z] = rt++, t._state = void 0, t._result = void 0, t._subscribers = []
    }

    function k() {
        return new Error("Array Methods must be provided an Array")
    }

    function F(t) {
        return new ot(this, t).promise
    }

    function Y(t) {
        var e = this;
        return new e(U(t) ? function (n, r) {
            for (var o = t.length, i = 0; i < o; i++) e.resolve(t[i]).then(n, r)
        } : function (t, e) {
            return e(new TypeError("You must pass an array to race."))
        })
    }

    function q(t) {
        var e = this, n = new e(p);
        return j(n, t), n
    }

    function D() {
        throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")
    }

    function K() {
        throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")
    }

    function L() {
        var t = void 0;
        if ("undefined" != typeof global) t = global; else if ("undefined" != typeof self) t = self; else try {
            t = Function("return this")()
        } catch (e) {
            throw new Error("polyfill failed because global object is unavailable in this environment")
        }
        var n = t.Promise;
        if (n) {
            var r = null;
            try {
                r = Object.prototype.toString.call(n.resolve())
            } catch (e) {
            }
            if ("[object Promise]" === r && !n.cast) return
        }
        t.Promise = it
    }

    var N = void 0;
    N = Array.isArray ? Array.isArray : function (t) {
        return "[object Array]" === Object.prototype.toString.call(t)
    };
    var U = N, W = 0, z = void 0, B = void 0, G = function (t, e) {
        V[W] = t, V[W + 1] = e, W += 2, 2 === W && (B ? B(a) : X())
    }, H = "undefined" != typeof window ? window : void 0, I = H || {}, J = I.MutationObserver || I.WebKitMutationObserver, Q = "undefined" == typeof self && "undefined" != typeof process && "[object process]" === {}.toString.call(process), R = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel, V = new Array(1e3), X = void 0;
    X = Q ? o() : J ? s() : R ? u() : void 0 === H && "function" == typeof require ? f() : c();
    var Z = Math.random().toString(36).substring(2), $ = void 0, tt = 1, et = 2, nt = {error: null}, rt = 0, ot = function () {
        function t(t, e) {
            this._instanceConstructor = t, this.promise = new t(p), this.promise[Z] || O(this.promise), U(e) ? (this.length = e.length, this._remaining = e.length, this._result = new Array(this.length), 0 === this.length ? S(this.promise, this._result) : (this.length = this.length || 0, this._enumerate(e), 0 === this._remaining && S(this.promise, this._result))) : j(this.promise, k())
        }

        return t.prototype._enumerate = function (t) {
            for (var e = 0; this._state === $ && e < t.length; e++) this._eachEntry(t[e], e)
        }, t.prototype._eachEntry = function (t, e) {
            var n = this._instanceConstructor, r = n.resolve;
            if (r === h) {
                var o = _(t);
                if (o === l && t._state !== $) this._settledAt(t._state, e, t._result); else if ("function" != typeof o) this._remaining--, this._result[e] = t; else if (n === it) {
                    var i = new n(p);
                    w(i, t, o), this._willSettleAt(i, e)
                } else this._willSettleAt(new n(function (e) {
                    return e(t)
                }), e)
            } else this._willSettleAt(r(t), e)
        }, t.prototype._settledAt = function (t, e, n) {
            var r = this.promise;
            r._state === $ && (this._remaining--, t === et ? j(r, n) : this._result[e] = n), 0 === this._remaining && S(r, this._result)
        }, t.prototype._willSettleAt = function (t, e) {
            var n = this;
            E(t, void 0, function (t) {
                return n._settledAt(tt, e, t)
            }, function (t) {
                return n._settledAt(et, e, t)
            })
        }, t
    }(), it = function () {
        function t(e) {
            this[Z] = C(), this._result = this._state = void 0, this._subscribers = [], p !== e && ("function" != typeof e && D(), this instanceof t ? x(this, e) : K())
        }

        return t.prototype["catch"] = function (t) {
            return this.then(null, t)
        }, t.prototype["finally"] = function (t) {
            var e = this, n = e.constructor;
            return e.then(function (e) {
                return n.resolve(t()).then(function () {
                    return e
                })
            }, function (e) {
                return n.resolve(t()).then(function () {
                    throw e
                })
            })
        }, t
    }();
    return it.prototype.then = l, it.all = F, it.race = Y, it.resolve = h, it.reject = q, it._setScheduler = n, it._setAsap = r, it._asap = G, it.polyfill = L, it.Promise = it, it.polyfill(), it
});

/* axios v0.18.0 | (c) 2018 by Matt Zabriskie */
!function (e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define([], t) : "object" == typeof exports ? exports.axios = t() : e.axios = t()
}(this, function () {
    return function (e) {
        function t(r) {
            if (n[r]) return n[r].exports;
            var o = n[r] = {exports: {}, id: r, loaded: !1};
            return e[r].call(o.exports, o, o.exports, t), o.loaded = !0, o.exports
        }

        var n = {};
        return t.m = e, t.c = n, t.p = "", t(0)
    }([function (e, t, n) {
        e.exports = n(1)
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            var t = new s(e), n = i(s.prototype.request, t);
            return o.extend(n, s.prototype, t), o.extend(n, t), n
        }

        var o = n(2), i = n(3), s = n(5), u = n(6), a = r(u);
        a.Axios = s, a.create = function (e) {
            return r(o.merge(u, e))
        }, a.Cancel = n(23), a.CancelToken = n(24), a.isCancel = n(20), a.all = function (e) {
            return Promise.all(e)
        }, a.spread = n(25), e.exports = a, e.exports.default = a
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            return "[object Array]" === R.call(e)
        }

        function o(e) {
            return "[object ArrayBuffer]" === R.call(e)
        }

        function i(e) {
            return "undefined" != typeof FormData && e instanceof FormData
        }

        function s(e) {
            var t;
            return t = "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
        }

        function u(e) {
            return "string" == typeof e
        }

        function a(e) {
            return "number" == typeof e
        }

        function c(e) {
            return "undefined" == typeof e
        }

        function f(e) {
            return null !== e && "object" == typeof e
        }

        function p(e) {
            return "[object Date]" === R.call(e)
        }

        function d(e) {
            return "[object File]" === R.call(e)
        }

        function l(e) {
            return "[object Blob]" === R.call(e)
        }

        function h(e) {
            return "[object Function]" === R.call(e)
        }

        function m(e) {
            return f(e) && h(e.pipe)
        }

        function y(e) {
            return "undefined" != typeof URLSearchParams && e instanceof URLSearchParams
        }

        function w(e) {
            return e.replace(/^\s*/, "").replace(/\s*$/, "")
        }

        function g() {
            return ("undefined" == typeof navigator || "ReactNative" !== navigator.product) && ("undefined" != typeof window && "undefined" != typeof document)
        }

        function v(e, t) {
            if (null !== e && "undefined" != typeof e) if ("object" != typeof e && (e = [e]), r(e)) for (var n = 0, o = e.length; n < o; n++) t.call(null, e[n], n, e); else for (var i in e) Object.prototype.hasOwnProperty.call(e, i) && t.call(null, e[i], i, e)
        }

        function x() {
            function e(e, n) {
                "object" == typeof t[n] && "object" == typeof e ? t[n] = x(t[n], e) : t[n] = e
            }

            for (var t = {}, n = 0, r = arguments.length; n < r; n++) v(arguments[n], e);
            return t
        }

        function b(e, t, n) {
            return v(t, function (t, r) {
                n && "function" == typeof t ? e[r] = E(t, n) : e[r] = t
            }), e
        }

        var E = n(3), C = n(4), R = Object.prototype.toString;
        e.exports = {isArray: r, isArrayBuffer: o, isBuffer: C, isFormData: i, isArrayBufferView: s, isString: u, isNumber: a, isObject: f, isUndefined: c, isDate: p, isFile: d, isBlob: l, isFunction: h, isStream: m, isURLSearchParams: y, isStandardBrowserEnv: g, forEach: v, merge: x, extend: b, trim: w}
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t) {
            return function () {
                for (var n = new Array(arguments.length), r = 0; r < n.length; r++) n[r] = arguments[r];
                return e.apply(t, n)
            }
        }
    }, function (e, t) {
        function n(e) {
            return !!e.constructor && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e)
        }

        function r(e) {
            return "function" == typeof e.readFloatLE && "function" == typeof e.slice && n(e.slice(0, 0))
        }

        /*!
	 * Determine if an object is a Buffer
	 *
	 * @author   Feross Aboukhadijeh <https://feross.org>
	 * @license  MIT
	 */
        e.exports = function (e) {
            return null != e && (n(e) || r(e) || !!e._isBuffer)
        }
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            this.defaults = e, this.interceptors = {request: new s, response: new s}
        }

        var o = n(6), i = n(2), s = n(17), u = n(18);
        r.prototype.request = function (e) {
            "string" == typeof e && (e = i.merge({url: arguments[0]}, arguments[1])), e = i.merge(o, {method: "get"}, this.defaults, e), e.method = e.method.toLowerCase();
            var t = [u, void 0], n = Promise.resolve(e);
            for (this.interceptors.request.forEach(function (e) {
                t.unshift(e.fulfilled, e.rejected)
            }), this.interceptors.response.forEach(function (e) {
                t.push(e.fulfilled, e.rejected)
            }); t.length;) n = n.then(t.shift(), t.shift());
            return n
        }, i.forEach(["delete", "get", "head", "options"], function (e) {
            r.prototype[e] = function (t, n) {
                return this.request(i.merge(n || {}, {method: e, url: t}))
            }
        }), i.forEach(["post", "put", "patch"], function (e) {
            r.prototype[e] = function (t, n, r) {
                return this.request(i.merge(r || {}, {method: e, url: t, data: n}))
            }
        }), e.exports = r
    }, function (e, t, n) {
        "use strict";

        function r(e, t) {
            !i.isUndefined(e) && i.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
        }

        function o() {
            var e;
            return "undefined" != typeof XMLHttpRequest ? e = n(8) : "undefined" != typeof process && (e = n(8)), e
        }

        var i = n(2), s = n(7), u = {"Content-Type": "application/x-www-form-urlencoded"}, a = {
            adapter: o(), transformRequest: [function (e, t) {
                return s(t, "Content-Type"), i.isFormData(e) || i.isArrayBuffer(e) || i.isBuffer(e) || i.isStream(e) || i.isFile(e) || i.isBlob(e) ? e : i.isArrayBufferView(e) ? e.buffer : i.isURLSearchParams(e) ? (r(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : i.isObject(e) ? (r(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
            }], transformResponse: [function (e) {
                if ("string" == typeof e) try {
                    e = JSON.parse(e)
                } catch (e) {
                }
                return e
            }], timeout: 0, xsrfCookieName: "XSRF-TOKEN", xsrfHeaderName: "X-XSRF-TOKEN", maxContentLength: -1, validateStatus: function (e) {
                return e >= 200 && e < 300
            }
        };
        a.headers = {common: {Accept: "application/json, text/plain, */*"}}, i.forEach(["delete", "get", "head"], function (e) {
            a.headers[e] = {}
        }), i.forEach(["post", "put", "patch"], function (e) {
            a.headers[e] = i.merge(u)
        }), e.exports = a
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = function (e, t) {
            r.forEach(e, function (n, r) {
                r !== t && r.toUpperCase() === t.toUpperCase() && (e[t] = n, delete e[r])
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2), o = n(9), i = n(12), s = n(13), u = n(14), a = n(10), c = "undefined" != typeof window && window.btoa && window.btoa.bind(window) || n(15);
        e.exports = function (e) {
            return new Promise(function (t, f) {
                var p = e.data, d = e.headers;
                r.isFormData(p) && delete d["Content-Type"];
                var l = new XMLHttpRequest, h = "onreadystatechange", m = !1;
                if ("undefined" == typeof window || !window.XDomainRequest || "withCredentials" in l || u(e.url) || (l = new window.XDomainRequest, h = "onload", m = !0, l.onprogress = function () {
                }, l.ontimeout = function () {
                }), e.auth) {
                    var y = e.auth.username || "", w = e.auth.password || "";
                    d.Authorization = "Basic " + c(y + ":" + w)
                }
                if (l.open(e.method.toUpperCase(), i(e.url, e.params, e.paramsSerializer), !0), l.timeout = e.timeout, l[h] = function () {
                    if (l && (4 === l.readyState || m) && (0 !== l.status || l.responseURL && 0 === l.responseURL.indexOf("file:"))) {
                        var n = "getAllResponseHeaders" in l ? s(l.getAllResponseHeaders()) : null, r = e.responseType && "text" !== e.responseType ? l.response : l.responseText, i = {data: r, status: 1223 === l.status ? 204 : l.status, statusText: 1223 === l.status ? "No Content" : l.statusText, headers: n, config: e, request: l};
                        o(t, f, i), l = null
                    }
                }, l.onerror = function () {
                    f(a("Network Error", e, null, l)), l = null
                }, l.ontimeout = function () {
                    f(a("timeout of " + e.timeout + "ms exceeded", e, "ECONNABORTED", l)), l = null
                }, r.isStandardBrowserEnv()) {
                    var g = n(16), v = (e.withCredentials || u(e.url)) && e.xsrfCookieName ? g.read(e.xsrfCookieName) : void 0;
                    v && (d[e.xsrfHeaderName] = v)
                }
                if ("setRequestHeader" in l && r.forEach(d, function (e, t) {
                    "undefined" == typeof p && "content-type" === t.toLowerCase() ? delete d[t] : l.setRequestHeader(t, e)
                }), e.withCredentials && (l.withCredentials = !0), e.responseType) try {
                    l.responseType = e.responseType
                } catch (t) {
                    if ("json" !== e.responseType) throw t
                }
                "function" == typeof e.onDownloadProgress && l.addEventListener("progress", e.onDownloadProgress), "function" == typeof e.onUploadProgress && l.upload && l.upload.addEventListener("progress", e.onUploadProgress), e.cancelToken && e.cancelToken.promise.then(function (e) {
                    l && (l.abort(), f(e), l = null)
                }), void 0 === p && (p = null), l.send(p)
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(10);
        e.exports = function (e, t, n) {
            var o = n.config.validateStatus;
            n.status && o && !o(n.status) ? t(r("Request failed with status code " + n.status, n.config, null, n.request, n)) : e(n)
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(11);
        e.exports = function (e, t, n, o, i) {
            var s = new Error(e);
            return r(s, t, n, o, i)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t, n, r, o) {
            return e.config = t, n && (e.code = n), e.request = r, e.response = o, e
        }
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
        }

        var o = n(2);
        e.exports = function (e, t, n) {
            if (!t) return e;
            var i;
            if (n) i = n(t); else if (o.isURLSearchParams(t)) i = t.toString(); else {
                var s = [];
                o.forEach(t, function (e, t) {
                    null !== e && "undefined" != typeof e && (o.isArray(e) ? t += "[]" : e = [e], o.forEach(e, function (e) {
                        o.isDate(e) ? e = e.toISOString() : o.isObject(e) && (e = JSON.stringify(e)), s.push(r(t) + "=" + r(e))
                    }))
                }), i = s.join("&")
            }
            return i && (e += (e.indexOf("?") === -1 ? "?" : "&") + i), e
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2), o = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
        e.exports = function (e) {
            var t, n, i, s = {};
            return e ? (r.forEach(e.split("\n"), function (e) {
                if (i = e.indexOf(":"), t = r.trim(e.substr(0, i)).toLowerCase(), n = r.trim(e.substr(i + 1)), t) {
                    if (s[t] && o.indexOf(t) >= 0) return;
                    "set-cookie" === t ? s[t] = (s[t] ? s[t] : []).concat([n]) : s[t] = s[t] ? s[t] + ", " + n : n
                }
            }), s) : s
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = r.isStandardBrowserEnv() ? function () {
            function e(e) {
                var t = e;
                return n && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), {href: o.href, protocol: o.protocol ? o.protocol.replace(/:$/, "") : "", host: o.host, search: o.search ? o.search.replace(/^\?/, "") : "", hash: o.hash ? o.hash.replace(/^#/, "") : "", hostname: o.hostname, port: o.port, pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname}
            }

            var t, n = /(msie|trident)/i.test(navigator.userAgent), o = document.createElement("a");
            return t = e(window.location.href), function (n) {
                var o = r.isString(n) ? e(n) : n;
                return o.protocol === t.protocol && o.host === t.host
            }
        }() : function () {
            return function () {
                return !0
            }
        }()
    }, function (e, t) {
        "use strict";

        function n() {
            this.message = "String contains an invalid character"
        }

        function r(e) {
            for (var t, r, i = String(e), s = "", u = 0, a = o; i.charAt(0 | u) || (a = "=", u % 1); s += a.charAt(63 & t >> 8 - u % 1 * 8)) {
                if (r = i.charCodeAt(u += .75), r > 255) throw new n;
                t = t << 8 | r
            }
            return s
        }

        var o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        n.prototype = new Error, n.prototype.code = 5, n.prototype.name = "InvalidCharacterError", e.exports = r
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = r.isStandardBrowserEnv() ? function () {
            return {
                write: function (e, t, n, o, i, s) {
                    var u = [];
                    u.push(e + "=" + encodeURIComponent(t)), r.isNumber(n) && u.push("expires=" + new Date(n).toGMTString()), r.isString(o) && u.push("path=" + o), r.isString(i) && u.push("domain=" + i), s === !0 && u.push("secure"), document.cookie = u.join("; ")
                }, read: function (e) {
                    var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
                    return t ? decodeURIComponent(t[3]) : null
                }, remove: function (e) {
                    this.write(e, "", Date.now() - 864e5)
                }
            }
        }() : function () {
            return {
                write: function () {
                }, read: function () {
                    return null
                }, remove: function () {
                }
            }
        }()
    }, function (e, t, n) {
        "use strict";

        function r() {
            this.handlers = []
        }

        var o = n(2);
        r.prototype.use = function (e, t) {
            return this.handlers.push({fulfilled: e, rejected: t}), this.handlers.length - 1
        }, r.prototype.eject = function (e) {
            this.handlers[e] && (this.handlers[e] = null)
        }, r.prototype.forEach = function (e) {
            o.forEach(this.handlers, function (t) {
                null !== t && e(t)
            })
        }, e.exports = r
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            e.cancelToken && e.cancelToken.throwIfRequested()
        }

        var o = n(2), i = n(19), s = n(20), u = n(6), a = n(21), c = n(22);
        e.exports = function (e) {
            r(e), e.baseURL && !a(e.url) && (e.url = c(e.baseURL, e.url)), e.headers = e.headers || {}, e.data = i(e.data, e.headers, e.transformRequest), e.headers = o.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers || {}), o.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function (t) {
                delete e.headers[t]
            });
            var t = e.adapter || u.adapter;
            return t(e).then(function (t) {
                return r(e), t.data = i(t.data, t.headers, e.transformResponse), t
            }, function (t) {
                return s(t) || (r(e), t && t.response && (t.response.data = i(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t)
            })
        }
    }, function (e, t, n) {
        "use strict";
        var r = n(2);
        e.exports = function (e, t, n) {
            return r.forEach(n, function (n) {
                e = n(e, t)
            }), e
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return !(!e || !e.__CANCEL__)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
        }
    }, function (e, t) {
        "use strict";
        e.exports = function (e, t) {
            return t ? e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "") : e
        }
    }, function (e, t) {
        "use strict";

        function n(e) {
            this.message = e
        }

        n.prototype.toString = function () {
            return "Cancel" + (this.message ? ": " + this.message : "")
        }, n.prototype.__CANCEL__ = !0, e.exports = n
    }, function (e, t, n) {
        "use strict";

        function r(e) {
            if ("function" != typeof e) throw new TypeError("executor must be a function.");
            var t;
            this.promise = new Promise(function (e) {
                t = e
            });
            var n = this;
            e(function (e) {
                n.reason || (n.reason = new o(e), t(n.reason))
            })
        }

        var o = n(23);
        r.prototype.throwIfRequested = function () {
            if (this.reason) throw this.reason
        }, r.source = function () {
            var e, t = new r(function (t) {
                e = t
            });
            return {token: t, cancel: e}
        }, e.exports = r
    }, function (e, t) {
        "use strict";
        e.exports = function (e) {
            return function (t) {
                return e.apply(null, t)
            }
        }
    }])
});

//zenscroll
!function (t, e) {
    "function" == typeof define && define.amd ? define([], e()) : "object" == typeof module && module.exports ? module.exports = e() : function n() {
        document && document.body ? t.zenscroll = e() : setTimeout(n, 9)
    }()
}(this, function () {
    "use strict";
    var t = function (t) {
        return t && "getComputedStyle" in window && "smooth" === window.getComputedStyle(t)["scroll-behavior"]
    };
    if ("undefined" == typeof window || !("document" in window)) return {};
    var e = function (e, n, o) {
        n = n || 999, o || 0 === o || (o = 9);
        var i, r = function (t) {
            i = t
        }, u = function () {
            clearTimeout(i), r(0)
        }, c = function (t) {
            return Math.max(0, e.getTopOf(t) - o)
        }, a = function (o, i, c) {
            if (u(), 0 === i || i && i < 0 || t(e.body)) e.toY(o), c && c(); else {
                var a = e.getY(), f = Math.max(0, o) - a, s = (new Date).getTime();
                i = i || Math.min(Math.abs(f), n), function t() {
                    r(setTimeout(function () {
                        var n = Math.min(1, ((new Date).getTime() - s) / i), o = Math.max(0, Math.floor(a + f * (n < .5 ? 2 * n * n : n * (4 - 2 * n) - 1)));
                        e.toY(o), n < 1 && e.getHeight() + o < e.body.scrollHeight ? t() : (setTimeout(u, 99), c && c())
                    }, 9))
                }()
            }
        }, f = function (t, e, n) {
            a(c(t), e, n)
        }, s = function (t, n, i) {
            var r = t.getBoundingClientRect().height, u = e.getTopOf(t) + r, s = e.getHeight(), l = e.getY(), d = l + s;
            c(t) < l || r + o > s ? f(t, n, i) : u + o > d ? a(u - s + o, n, i) : i && i()
        }, l = function (t, n, o, i) {
            a(Math.max(0, e.getTopOf(t) - e.getHeight() / 2 + (o || t.getBoundingClientRect().height / 2)), n, i)
        };
        return {
            setup: function (t, e) {
                return (0 === t || t) && (n = t), (0 === e || e) && (o = e), {defaultDuration: n, edgeOffset: o}
            }, to: f, toY: a, intoView: s, center: l, stop: u, moving: function () {
                return !!i
            }, getY: e.getY, getTopOf: e.getTopOf
        }
    }, n = document.documentElement, o = function () {
        return window.scrollY || n.scrollTop
    }, i = e({
        body: document.scrollingElement || document.body, toY: function (t) {
            window.scrollTo(0, t)
        }, getY: o, getHeight: function () {
            return window.innerHeight || n.clientHeight
        }, getTopOf: function (t) {
            return t.getBoundingClientRect().top + o() - n.offsetTop
        }
    });
    if (i.createScroller = function (t, o, i) {
        return e({
            body: t, toY: function (e) {
                t.scrollTop = e
            }, getY: function () {
                return t.scrollTop
            }, getHeight: function () {
                return Math.min(t.clientHeight, window.innerHeight || n.clientHeight)
            }, getTopOf: function (t) {
                return t.offsetTop
            }
        }, o, i)
    }, "addEventListener" in window && !window.noZensmooth && !t(document.body)) {
        var r = "history" in window && "pushState" in history, u = r && "scrollRestoration" in history;
        u && (history.scrollRestoration = "auto"), window.addEventListener("load", function () {
            u && (setTimeout(function () {
                history.scrollRestoration = "manual"
            }, 9), window.addEventListener("popstate", function (t) {
                t.state && "zenscrollY" in t.state && i.toY(t.state.zenscrollY)
            }, !1)), window.location.hash && setTimeout(function () {
                var t = i.setup().edgeOffset;
                if (t) {
                    var e = document.getElementById(window.location.href.split("#")[1]);
                    if (e) {
                        var n = Math.max(0, i.getTopOf(e) - t), o = i.getY() - n;
                        0 <= o && o < 9 && window.scrollTo(0, n)
                    }
                }
            }, 9)
        }, !1);
        var c = new RegExp("(^|\\s)noZensmooth(\\s|$)");
        window.addEventListener("click", function (t) {
            for (var e = t.target; e && "A" !== e.tagName;) e = e.parentNode;
            if (!(!e || 1 !== t.which || t.shiftKey || t.metaKey || t.ctrlKey || t.altKey)) {
                if (u) {
                    var n = history.state && "object" == typeof history.state ? history.state : {};
                    n.zenscrollY = i.getY();
                    try {
                        history.replaceState(n, "")
                    } catch (t) {
                    }
                }
                var o = e.getAttribute("href") || "";
                if (0 === o.indexOf("#") && !c.test(e.className)) {
                    var a = 0, f = document.getElementById(o.substring(1));
                    if ("#" !== o) {
                        if (!f) return;
                        a = i.getTopOf(f)
                    }
                    t.preventDefault();
                    var s = function () {
                        window.location = o
                    }, l = i.setup().edgeOffset;
                    l && (a = Math.max(0, a - l), r && (s = function () {
                        history.pushState({}, "", o)
                    })), i.toY(a, null, s)
                }
            }
        }, !1)
    }
    return i
});

//ExtCore
(function () {

    if (typeof window.MainFrame === 'undefined') {
        window.MainFrame = {};
    }
    if (typeof MainFrame.Ext === 'undefined') {
        MainFrame.Ext = {};
    }


    MainFrame.Ext.EnvDetection = function () {

        var names = {
            ios: 'iOS',
            android: 'Android',
            webos: 'webOS',
            blackberry: 'BlackBerry',
            rimTablet: 'RIMTablet',
            mac: 'MacOS',
            win: 'Windows',
            linux: 'Linux',
            bada: 'Bada',
            other: 'Other'
        };


        var prefixes = {
            ios: 'i(?:Pad|Phone|Pod)(?:.*)CPU(?: iPhone)? OS ',
            android: '(Android |HTC_|Silk/)',
            windowsPhone: 'IEMobile',
            blackberry: '(?:BlackBerry|BB)(?:.*)Version\/',
            rimTablet: 'RIM Tablet OS ',
            webos: '(?:webOS|hpwOS)\/',
            bada: 'Bada\/'
        };


        /**
         * - iOS
         * - iPad
         * - iPhone
         * - iPhone5 (also true for 4in iPods).
         * - iPod
         * - Android
         * - WebOS
         * - BlackBerry
         * - Bada
         * - MacOS
         * - Windows
         * - Linux
         * - Other
         * @param {String} value The OS name to check.
         * @return {Boolean}
         */
        var is = {};


        /**
         * - iOS
         * - Android
         * - WebOS
         * - BlackBerry,
         * - MacOS
         * - Windows
         * - Linux
         * - Other
         */
        var name = null;

        var version = null;

        var userAgent = navigator.userAgent;

        var deviceType;


        var setFlag = function (name, value) {
            if (typeof value == 'undefined') {
                value = true;
            }
            is[name] = value;
            is[name.toLowerCase()] = value;
        };


        for (var i in prefixes) {
            if (prefixes.hasOwnProperty(i)) {
                var prefix = prefixes[i];
                var match = userAgent.match(new RegExp('(?:' + prefix + ')([^\\s;]+)'));
                if (match) {
                    name = names[i];
                    version = match[match.length - 1];
                }
            }
        }


        if (!name) {
            name = names[(userAgent.toLowerCase().match(/mac|win|linux/) || ['other'])[0]];
        }


        // set flag
        setFlag(name);


        for (var i in names) {
            if (names.hasOwnProperty(i)) {
                var item = names[i];
                if (!is.hasOwnProperty(name)) {
                    setFlag(item, (name === item));
                }
            }
        }


        // iPad, iPod, iPhone deviceType
        if (is.iOS) {
            deviceType = userAgent.toLowerCase().match(/ipad/) ? 'Tablet' : 'Phone';

            if (userAgent.match(/iPad;.*CPU.*OS 7_\d/i)) {
                var htmlNode = document.documentElement;
                htmlNode.setAttribute('class', 'v7__bugfix--ipad-ios7');
            }
        }

        var isWindowsPhoneInDesktopMode = (navigator.userAgent.match('Windows') && navigator.userAgent.match('Touch') && navigator.userAgent.match('ARM') && navigator.userAgent.match('WPDesktop'));
        if (navigator.userAgent.match(/IEMobile/i) || isWindowsPhoneInDesktopMode) {
            is.windowsMobile = true;
        }


        // Set device type
        if (!deviceType) {

            if (!is.windowsMobile && !is.Android && !is.iOS && /Windows|Linux|MacOS/.test(name)) {
                deviceType = 'Desktop';
            }
            //else if (is.iPad || is.RIMTablet || is.Android3 || (is.Android4 && userAgent.search(/mobile/i) == -1)) {
            else if (is.iPad || is.RIMTablet || (is.Android && userAgent.search(/mobile/i) == -1)) {
                deviceType = 'Tablet';
            }
            else {
                deviceType = 'Phone';
            }

        }


        return {
            os: name,
            deviceType: deviceType,
            isAndroid: is.Android,
            isIOS: is.iOS,
            is: is
        };
    }();


    MainFrame.Ext.Class = {


        include: function (properties) {
            MainFrame.Ext.Class.extend(this.prototype, properties);
        },


        mergeOptions: function (options) {
            MainFrame.Ext.Class.extend(this.prototype.options, options);
        },


        extend: function (properties) {


            // extended class with the new prototype
            var NewClass = function () {

                this.uid = new Date().getTime() + '_' + Math.random();

                if (this.initialize) {
                    this.initialize.apply(this, arguments);
                }
            };


            // instantiate class without calling constructor
            var F = function () {
            };
            F.prototype = this.prototype;


            var proto = new F();
            proto.constructor = NewClass;

            NewClass.prototype = proto;


            //inherit parent's statics
            for (var i in this) {
                if (this.hasOwnProperty(i) && i !== 'prototype') {
                    NewClass[i] = this[i];
                }
            }


            // mix static properties into the class
            if (properties.statics) {
                MainFrame.Ext.Class.extendProperties(NewClass, properties.statics);
                delete properties.statics;
            }

            // mix includes into the prototype
            if (properties.includes) {
                MainFrame.Ext.Class.extendProperties.apply(null, [proto].concat(properties.includes));
                delete properties.includes;
            }

            // merge options
            if (properties.options && proto.options) {
                properties.options = MainFrame.Ext.Class.extendProperties({}, proto.options, properties.options);
            }

            // mix given properties into the prototype
            MainFrame.Ext.Class.extendProperties(proto, properties);


            return NewClass;
        },


        extendProperties: function (destination) {

            var sources = Array.prototype.slice.call(arguments, 1);

            for (var j = 0, len = sources.length, src; j < len; j++) {
                src = sources[j] || {};
                for (var i in src) {
                    if (src.hasOwnProperty(i)) {
                        destination[i] = src[i];
                    }
                }
            }

            return destination;
        },
    };


    MainFrame.Ext.Module = MainFrame.Ext.Class.extend({

        initialize: function (options) {

            var self = this;
            self._initialize(options);
        },


        _initialize: function (options) {

            var self = this;

            if (typeof options.details === 'undefined') {
                options.details = {};
            }

            if (typeof options.target !== 'undefined' && typeof options.target.appendChild === 'function') {
                options.target = {
                    element: options.target,
                    options: {
                        items: 'all'
                    }
                }
            }

            self.DETAILS = options.details;
            self.TARGET = options.target;

            if (options.details.static === true) {

                self.customInit();
                self.afterInit();

            } else {

                if (typeof options.data === 'object') {

                    if (typeof options.data.object !== 'undefined') {
                        var processed_data = self.processData(options.data.object);
                        self.DATA = processed_data.length ? processed_data : [processed_data];
                        self.customInit();
                        self.afterInit();

                    } else if (typeof options.data.url !== 'undefined') {

                        MainFrame.Ext.Utils.request.get(options.data.url)
                            .then(function (response) {

                                if (response.request.responseXML) {
                                    var xml_data = MainFrame.Ext.Parser.parseXmlFlux(response.request.responseXML.documentElement);
                                    self.DATA = self.processData((xml_data && xml_data.length) ? xml_data : xml_data.childNodes);
                                    self.customInit();
                                    self.afterInit();
                                } else if (response.data) {
                                    self.DATA = self.processData(response.data);
                                    self.customInit();
                                    self.afterInit();
                                }
                            })
                            .catch(function (error) {
                                MainFrame.Ext.Utils.log(error);
                            });
                    } else if (typeof options.data.import !== 'undefined') {

                        self.DATA = self.processData(self.importData(options.data.import));
                        self.customInit();
                        self.afterInit();

                    } else {
                        MainFrame.Ext.Utils.log('No valid options for data!');
                    }

                } else {
                    MainFrame.Ext.Utils.log('No data!');
                }
            }
        },


        afterInit: function () {

            var self = this;

            var init_callback = self.DETAILS.init_callback;

            if (init_callback) {
                if (typeof init_callback === 'function') {
                    init_callback(self);
                } else if (typeof window[init_callback] === 'function') {
                    window[init_callback](self);
                }
            }
        },


        importData: function (import_string) {

            var self = this;
            return self._importData(import_string);
        },


        _importData: function (import_string) {

            var self = this;
            switch (import_string) {

                case '#{catalog:pages}':
                    return self.importData_catalogPages();

                default:
                    MainFrame.Ext.Utils.log('Invalid import string ' + import_string);
                    return [];
            }
        },


        processData: function (data) {

            return data;
        },


        importData_catalogPages: function () {

            var self = this;

            if (typeof MainFrame.Manager !== 'undefined' || typeof window.parent.MainFrame.Manager !== 'undefined') {

                if (typeof MainFrame.Manager !== 'undefined') {
                    var scoping_manager = MainFrame.Manager.Scoping;
                } else {
                    var scoping_manager = window.parent.MainFrame.Manager.Scoping;
                }

                var catalog = scoping_manager.getById('catalog'),
                    pages_copy = JSON.parse(JSON.stringify(catalog.pages)),
                    pages = [];

                pages_copy.forEach(function (page_data) {
                    // verificam deoarece prima pagina din catalog e null
                    if (page_data) {
                        // datele propriu-zise sunt intr-un array
                        pages.push(page_data[0]);
                    }
                });

                return pages;

            } else {
                MainFrame.Ext.Utils.log('Data import failed! No catalog integration detected');
            }
        },


        getTemplate: function () {

            var self = this;

            if (MainFrame.Ext.EnvDetection.deviceType === 'Phone') {
                if (typeof self.TARGET.options.template_phone_id !== 'undefined') {
                    return self.getTemplateString(self.TARGET.options.template_phone_id);
                }
            }

            if (typeof self.TARGET.options.template_id !== 'undefined') {
                return self.getTemplateString(self.TARGET.options.template_id);
            } else if (typeof self.TARGET.options.template !== 'undefined') {
                return self.TARGET.options.template;
            } else {
                return self.TARGET.element.innerHTML.trim();
            }
        },


        getTemplateString: function (template_id) {

            var self = this;

            var template_element = document.getElementById(template_id);
            return template_element.innerHTML.trim();
        },


        getItems: function (items_string) {

            var self = this;

            if (!items_string) {
                items_string = 'all';
            }

            var items = [],
                item_index = 0;

            items_string = items_string.replace(/ /g, '');

            var list_parts = items_string.split(',');
            list_parts.forEach(function (list_part) {

                var is_all = (list_part === 'all'),
                    is_interval = (list_part.indexOf('-') !== -1),
                    is_insert = (!is_interval && list_part.indexOf('insert') === 0);

                if (is_insert) {

                    var insert_item = self.getInsertItem(list_part);
                    if (self.shouldAddItem(insert_item)) {
                        items.push(self.processItem(insert_item), item_index);
                        item_index++;
                    }

                } else if (is_all) {

                    var index = 0,
                        total_items = self.DATA.length;
                    for (; index < total_items; index++) {
                        var item = self.DATA[index];
                        if (self.shouldAddItem(item)) {
                            items.push(self.processItem(item, item_index));
                            item_index++;
                        }
                    }

                } else if (is_interval) {

                    var interval_parts = list_part.split('-'),
                        interval_start = interval_parts[0],
                        interval_end = interval_parts[1];

                    interval_start = parseInt(interval_start, 10);
                    if (interval_end === 'last') {
                        interval_end = self.DATA.length - 1;
                    } else {
                        interval_end = parseInt(interval_end, 10);
                    }

                    for (var index = interval_start; index <= interval_end; index++) {

                        var item = self.DATA[index];
                        if (self.shouldAddItem(self.processItem(item, item_index))) {
                            items.push(item);
                            item_index++;
                        }
                    }

                } else {

                    var index = parseInt(list_part, 10);

                    var item = self.DATA[index];
                    if (self.shouldAddItem(self.processItem(item, item_index))) {
                        items.push(item);
                        item_index++;
                    }
                }
            });

            return items;
        },


        shouldAddItem: function (item) {

            return true;
        },


        getInsertItem: function (insert_id) {

            var insert_element = document.getElementById(insert_id),
                insert_type = insert_element.getAttribute('data-insert-type') || '1x1';

            return {
                insert: true,
                insert_id: insert_id,
                insert_type: insert_type,
                insert_template: insert_element.innerHTML.trim(),

            };
        },


        createItems: function (options) {

            var self = this;
            options.target.innerHTML = self._createItems(options);
        },


        _createItems: function (options) {

            var self = this;

            var items_html = options.target.innerHTML;

            if (options.paginate_items) {
                var start_index = (options.page - 1) * options.paginate_items,
                    end_index = options.page * options.paginate_items;

                if (end_index > options.items.length - 1) {
                    end_index = options.items.length - 1;
                }
            } else {
                var start_index = 0,
                    end_index = options.items.length - 1;
            }

            options.items.forEach(function (item, index) {

                if (index >= start_index && index <= end_index) {
                    if (item.insert) {
                        var interpreted_template = self.interpretTemplate(item.item, item.insert_template, options.target);
                    } else {
                        var interpreted_template = self.interpretTemplate(item, options.template, options.target);
                    }
                    items_html += interpreted_template;
                }
            });

            return items_html;
        },


        processItem: function (item, options) {

            if (typeof options === 'undefined') {
                options = {};
            }

            return item;
        },


        interpretTemplate: function (item, template, target) {

            var self = this;
            return MainFrame.Ext.Utils.fillStringWithData(template, item);
        },


        getResponsiveSettings: function (target, settings) {

            var self = this;

            if (settings.responsive) {

                var current_width = target.clientWidth,
                    responsive_widths = [],
                    responsive_settings = {};

                for (var setting in settings) {
                    if (setting !== 'responsive') {
                        responsive_settings[setting] = settings[setting];
                    }
                }

                for (var width in settings.responsive) {
                    responsive_widths.push(width);
                }
                responsive_widths.sort();

                var index = 0,
                    total_widths = responsive_widths.length;
                for (; index < total_widths; index++) {
                    var width = responsive_widths[index];
                    if (current_width <= width) {
                        rows = settings.responsive[width].rows;
                        for (var setting in settings.responsive[width]) {
                            responsive_settings[setting] = settings.responsive[width][setting];
                        }
                        return responsive_settings;
                    }
                }

                return responsive_settings;

            } else {
                return settings;
            }
        },


        /** Screens **/
        createWrapper: function () {

            var self = this;

            var target_element = self.TARGET.element;
            target_element.innerHTML = '';

            var wrapper = document.createElement('div');
            wrapper.setAttribute('class', self.css_classes['wrapper']);
            target_element.appendChild(wrapper);

            self.TARGET.wrapper = wrapper;
        },


        createLoadingScreen: function () {

            var self = this;

            var target_element = self.TARGET.element,
                loading_screen = document.createElement('div');
            loading_screen.setAttribute('class', self.css_classes['loading']);

            if (self.TARGET.options.template_loading_id) {
                loading_screen.innerHTML = self.getTemplateString(self.TARGET.options.template_loading_id);
            }

            target_element.appendChild(loading_screen);

            self.TARGET.loading_screen = loading_screen;
        },


        showLoadingScreen: function (reset_scroll) {

            var self = this;

            self.TARGET.loading_screen.style.display = 'block';

            if (reset_scroll) {
                self.resetScroll();
            }
        },


        hideLoadingScreen: function () {

            var self = this;

            self.TARGET.loading_screen.style.display = '';
        },
        /** END Screens **/


        /** Sectiuni **/
        loadImagesForSectionIndex: function (section_index) {

            var self = this;

            var current_section_index = section_index,
                previous_section_index = section_index - 1,
                next_section_index = section_index + 1;

            var sections = self.SCROLLING_STATUS.sections,
                index = 0,
                total_sections = sections.length;
            for (; index < total_sections; index++) {

                var section_data = sections[index];

                // vor fi maxim 3 sectiuni vizibile
                // cea curenta, cea din spatele ei si cea din fata ei (daca exista)
                var should_hide_section = (index < previous_section_index || index > next_section_index),
                    should_show_section = (index === previous_section_index || index === current_section_index || index === next_section_index);

                if (should_hide_section) {
                    if (section_data.status !== 'hidden') {
                        self.hideSection(section_data);
                    }
                }

                if (should_show_section) {
                    if (section_data.status === 'hidden') {
                        self.showSection(section_data);
                    } else {
                        section_data.element.setAttribute('data-section-status', '');
                    }
                }

                if (index === current_section_index) {
                    section_data.element.setAttribute('data-section-status', 'current');
                }
            }
        },


        getSectionIndexForOffsetY: function (offset) {

            var self = this;

            var sections = self.SCROLLING_STATUS.sections,
                index = 0,
                total_sections = sections.length;
            for (; index < total_sections; index++) {

                var section_data = sections[index];

                if (offset >= section_data.start_y && offset <= section_data.end_y) {
                    return index;
                }
            }
        },


        hideSection: function (section_data) {

            var self = this;

            var bg_items = section_data.element.getElementsByClassName(self.css_classes['async-bg']),
                index = 0,
                total_items = bg_items.length;
            for (; index < total_items; index++) {

                var bg_item = bg_items[index];
                bg_item.style.backgroundImage = '';
            }

            var src_items = section_data.element.getElementsByClassName(self.css_classes['async-src']),
                index = 0,
                total_items = src_items.length;
            for (; index < total_items; index++) {

                var src_item = src_items[index];
                src_item.src = MainFrame.Ext.Utils.emptyImageUrl;
            }

            section_data.element.setAttribute('data-section-status', 'hidden');
            section_data.element.style.display = 'none';
            section_data.status = 'hidden';
        },


        showSection: function (section_data) {

            var self = this;

            var bg_items = section_data.element.getElementsByClassName(self.css_classes['async-bg']),
                index = 0,
                total_items = bg_items.length;
            for (; index < total_items; index++) {

                var bg_item = bg_items[index],
                    bg_src = bg_item.getAttribute('data-bg-src');

                bg_item.style.backgroundImage = 'url(\'' + bg_src + '\')';
            }

            var src_items = section_data.element.getElementsByClassName(self.css_classes['async-src']),
                index = 0,
                total_items = src_items.length;
            for (; index < total_items; index++) {

                var src_item = src_items[index],
                    src = src_item.getAttribute('data-src');

                src_item.src = src;
            }

            section_data.element.setAttribute('data-section-status', '');
            section_data.element.style.display = '';
            section_data.status = 'visible';
        },
        /** END Sectiuni **/


        /** Event binding **/
        bindResize: function () {

            var self = this;

            var resize_timeout;
            MainFrame.Ext.Utils.bind(window, 'resize', function () {
                clearTimeout(resize_timeout);
                resize_timeout = setTimeout(self.onResizeEnd.bind(self), 100);
            });
        },


        onResizeEnd: function () {

            var self = this;
        },
        /** END Event binding **/


        resetScroll: function () {
            window.scrollTo(0, 0);
        },


        destroy: function () {

            var self = this;
            self._destroy();
        },


        _destroy: function () {

            var self = this;

            self.DATA = null;
            self.TARGET.element.__autoloaded = false;
            self.TARGET.element.innerHTML = '';
        }
    });


    MainFrame.Ext.Utils = {

        /**
         * 1x1px transparentx gif in Base64
         */
        emptyImageUrl: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',


        bind: function (element, name, observer, options) {

            if (element && name) {

                var eventNames = name.split(' ');

                eventNames.forEach(function (name) {
                    if ('addEventListener' in element) {
                        try {
                            element.addEventListener(name, observer, options);
                        }
                        catch (e) {
                            if (typeof observer == 'object' && observer.handleEvent) {
                                element.addEventListener(name, function (e) {
                                    observer.handleEvent.call(observer, e);
                                }, options);
                            }
                            else {
                                throw e;
                            }
                        }
                    }
                    else if ('attachEvent' in element) {
                        if (typeof observer == 'object' && observer.handleEvent) {
                            element.attachEvent('on' + name, function () {
                                observer.handleEvent.call(observer);
                            });
                        }
                        else {
                            element.attachEvent('on' + name, observer);
                        }
                    }
                });
            } else {
                console.error('Bind error! Element: ' + element + ', event: ' + name);
            }
        },


        unbind: function (element, name, observer, bubble) {

            if ('removeEventListener' in element) {
                try {
                    element.removeEventListener(name, observer, bubble);
                }
                catch (e) {
                    if (typeof observer == 'object' && observer.handleEvent) {
                        element.removeEventListener(name, function (e) {
                            observer.handleEvent.call(observer, e);
                        }, bubble);
                    }
                    else {
                        throw e;
                    }
                }
            }
            else if ('detachEvent' in element) {
                if (typeof observer == 'object' && observer.handleEvent) {
                    element.detachEvent('on' + name, function () {
                        observer.handleEvent.call(observer);
                    });
                }
                else {
                    element.detachEvent('on' + name, observer);
                }
            }

        },


        fillStringWithData: function (template, data) {

            // in prima faza evaluam standard expresiile de tip { if }
            // daca nu gasim rezultat valid, returnam expresia in forma originala
            // deoarece e posibil sa fie vorba de un if in interiorul unui { repeat }
            // caz in care la prima evaluare nu exista variabila verificata
            var ifRegExp = /\{ if (.*?) \}((.|\n)*?)\{ endif \}/gim;
            template = template.replace(ifRegExp, function (whole_match, if_match, if_template) {

                var var_to_evaluate = if_match.slice(2, -1);
                value = MainFrame.Ext.Utils.interpretDataString(var_to_evaluate, data);
                if (typeof value !== 'object') {
                    if (value) {
                        return if_template;
                    } else {
                        return ''
                    }
                }

                return whole_match;
            });

            var repeatRegExp = /\{ repeat (.*?) in (.*?) \}((.|\n)*)\{ endrepeat \}/gim;

            template = template.replace(repeatRegExp, function (whole_match, repeat_match, repeat_from_match, repeat_template, end_repeat_match) {
                return MainFrame.Ext.Utils.interpretRepeatWithData(whole_match, repeat_match, repeat_from_match, repeat_template.trim(), data);
            });

            // dupa evaluarea repeat-urilor re-verificam if-urile ramase
            // de data aceasta ce nu trece e clar invalid si nu afisam
            var ifRegExp = /\{ if (.*?) \}((.|\n)*?)\{ endif \}/gim;
            template = template.replace(ifRegExp, function (whole_match, if_match, if_template) {

                var var_to_evaluate = if_match.slice(2, -1);
                value = MainFrame.Ext.Utils.interpretDataString(var_to_evaluate, data);
                if (typeof value !== 'object') {
                    if (value) {
                        return if_template;
                    } else {
                        return ''
                    }
                }

                return '';
            });

            var regExp = /#{(.*?)}/gi;
            var dynamicVariables = template.match(regExp);
            if (dynamicVariables) {

                var variableIndex = 0,
                    totalVariables = dynamicVariables.length;
                for (; variableIndex < totalVariables; variableIndex++) {

                    var dynamicVariable = dynamicVariables[variableIndex]

                    // we remove '#{' and '}' from the var to get the product property name
                    var propertiesString = dynamicVariable.slice(2, -1);

                    var value = MainFrame.Ext.Utils.interpretDataString(propertiesString, data);

                    var valueHasChanged = (value !== data);

                    var dynamicValue = (valueHasChanged) ? MainFrame.Ext.Utils.addNewlines(value) : '';

                    // escaping for the JSON parse method
                    dynamicValue = dynamicValue.replace(/’/g, "'");
                    dynamicValue = dynamicValue.replace(/\r/g, '');
                    dynamicValue = dynamicValue.replace(/\f/g, '');
                    dynamicValue = dynamicValue.replace(/\t/g, '    ');

                    template = template.replace(dynamicVariable, dynamicValue);
                }
            }

            return template;
        },


        interpretRepeatWithData: function (whole_match, repeat_match, repeat_from_match, repeat_template, data) {

            var source_item = MainFrame.Ext.Utils.parseRepeatSourceItem(data, repeat_from_match);
            if (source_item) {

                var template = '';

                var data_item = {};
                for (var key in data) {
                    if (key !== repeat_from_match) {
                        data_item[key] = data[key];
                    }
                }

                source_item.forEach(function (item) {

                    data_item[repeat_match] = item;

                    var parsed_repeat_template = MainFrame.Ext.Utils.fillStringWithData(repeat_template + '', data_item)
                    template += parsed_repeat_template;
                });

                return template;

            } else {
                return '';
            }
        },


        parseRepeatSourceItem: function (data, repeat_from_match) {

            var is_direct_key = (repeat_from_match.indexOf('.') === -1);
            if (is_direct_key) {
                if (data[repeat_from_match] && (typeof data[repeat_from_match] === 'object' && data[repeat_from_match].length)) {
                    return data[repeat_from_match];
                }
            } else {

                var keys = repeat_from_match.split('.'),
                    total_keys = keys.length,
                    index = 0;
                for (; index < total_keys; index++) {

                    var key = keys[index];
                    if (typeof data[key] === 'undefined') {
                        return null;
                    }

                    data = data[key];
                }

                if (data && typeof data === 'object' && data.length) {
                    return data;
                }
            }

            return null;
        },


        interpretDataString: function (dataString, data) {

            var value = data;

            var properties = dataString.split('.'),
                propertyIndex = 0,
                totalProperties = properties.length;
            for (; propertyIndex < totalProperties; propertyIndex++) {

                var property = properties[propertyIndex];

                var hasIndex = false,
                    validIndex = false,
                    index = null;

                var match = property.match(/\[(.*)\]/);
                if (match) {
                    index = match[1];
                    index = index.replace(/\'|\"/g, '');
                    hasIndex = true;
                    property = property.split('[')[0];
                }

                if (typeof value[property] !== 'undefined') {

                    if (hasIndex && value[property][index]) {
                        value = value[property][index];
                        validIndex = true;
                    } else {
                        value = value[property];
                    }

                } else if (value.attributes && typeof value.attributes[property] !== 'undefined') {

                    value = value.attributes[property];

                } else if (value.children && typeof value.children[property] !== 'undefined') {

                    if (hasIndex && value.children[property][index]) {
                        value = value.children[property][index];
                        validIndex = true;
                    } else {
                        value = value.children[property];
                    }

                } else {
                    if (hasIndex && !validIndex) {
                        value = '';
                    } else {
                        MainFrame.Ext.Utils.log('template parsing error, no value found for the property "' + dataString + '"');
                    }
                }

            }

            if (value['value']) {
                value = value['value'];
            }

            return value;
        },


        addNewlines: function (str) {

            str += '';

            if (str) {
                return str.split(/\r?\n/).join('<br/>');
            }

            return str;
        },


        log: function (msg, type) {
            if (typeof type === 'undefined') {
                type = 'log';
            }

            switch (type) {
                case 'log':
                default:
                    console.log(msg);
                    break;
            }
        }
    };
    // decorator
    if (typeof axios !== 'undefined') {
        MainFrame.Ext.Utils.request = axios;
    } else {
        MainFrame.Ext.Utils.log('Libraria axios nu este inclusă. Daca nu este necesară puteți ignora acest mesaj.');
    }


    MainFrame.Ext.Parser = {

        parseXml: function (xmlNode) {

            var jsonObject = {};

            jsonObject.nodeName = xmlNode.nodeName;

            jsonObject.childNodes = [];

            jsonObject.attributes = MainFrame.Ext.Parser.parseAttributes(xmlNode);

            if (jsonObject.nodeName !== 'html') {

                if (xmlNode.childNodes && xmlNode.childNodes.length) {
                    // sometimes the first child could be a whitespace node and the CDATA is the next node
                    var hasCdata = (xmlNode.childNodes[0].nodeType === 4 || (xmlNode.childNodes[1] && xmlNode.childNodes[1].nodeType === 4)),
                        hasTextContent = (xmlNode.childNodes.length === 1 && xmlNode.childNodes[0].nodeType === 3) ? true : false;
                    if (hasCdata) {
                        var cdataNode = (xmlNode.childNodes[0].nodeType === 4) ?
                            xmlNode.childNodes[0] :
                            xmlNode.childNodes[1];
                        var cdata = cdataNode.textContent || cdataNode.text;
                        jsonObject.value = cdata;
                    } else if (hasTextContent) {
                        jsonObject.value = xmlNode.textContent || xmlNode.text;
                    } else {

                        var childIndex = 0,
                            totalChildren = xmlNode.childNodes.length;
                        for (; childIndex < totalChildren; childIndex++) {
                            var childNode = xmlNode.childNodes[childIndex];

                            if (childNode.nodeType === 1) {
                                var jsonChild = MainFrame.Ext.Parser.parseXml(childNode);
                                jsonObject.childNodes.push(jsonChild);
                            }
                        }
                    }
                }
            } else {

                if (xmlNode.childNodes.length) {

                    if (XMLSerializer) {
                        var codeNode = e.getElementsByTagName("code")[0];
                        var htmlString = MainFrame.Ext.Parser.serializeXmlNode(codeNode);
                        htmlString = htmlString.replace('<code>', '');
                        htmlString = htmlString.replace('</code>', '');
                    }

                    jsonObject.html = htmlString;
                }
            }

            return jsonObject;
        },


        parseXmlFlux: function (xmlNode) {

            var data = {};

            if (xmlNode.attributes && xmlNode.attributes.length) {
                data.attributes = MainFrame.Ext.Parser.parseAttributes(xmlNode);
            }

            data.nodeName = xmlNode.nodeName;

            if (xmlNode.childNodes && xmlNode.childNodes.length) {

                var cdata = MainFrame.Ext.Parser.getCDATA(xmlNode);
                if (cdata !== null) {
                    data.value = cdata;
                } else {
                    MainFrame.Ext.Parser.addChildren(xmlNode, data);
                }
            }

            return data;
        },


        getCDATA: function (xmlNode) {

            // sometimes the first child could be a whitespace node and the CDATA is the next node
            var hasCdata = (xmlNode.childNodes[0].nodeType === 4 || (xmlNode.childNodes[1] && xmlNode.childNodes[1].nodeType === 4)),
                hasTextContent = (xmlNode.childNodes.length === 1 && xmlNode.childNodes[0].nodeType === 3) ? true : false;
            if (hasCdata) {
                var cdataNode = (xmlNode.childNodes[0].nodeType === 4) ? xmlNode.childNodes[0] : xmlNode.childNodes[1];
                return (typeof cdataNode.textContent !== 'undefined') ? cdataNode.textContent : cdataNode.text;
            } else if (hasTextContent) {
                return (typeof xmlNode.textContent !== 'undefined') ? xmlNode.textContent : xmlNode.text;
            }

            return null;
        },


        addChildren: function (xmlNode, data) {
            var childIndex = 0,
                totalChildren = xmlNode.childNodes.length;
            if (totalChildren) {

                data.nodeName = xmlNode.nodeName;

                data.children = {};
                data.childNodes = [];

                for (; childIndex < totalChildren; childIndex++) {

                    var childNode = xmlNode.childNodes[childIndex],
                        nodeName = childNode.nodeName;

                    if (childNode.attributes) {

                        var parsedChild = MainFrame.Ext.Parser.parseXmlFlux(childNode);
                        data.childNodes.push(parsedChild);

                        if (MainFrame.Ext.Parser.isChildWithUniqueTagName(xmlNode, nodeName)) {
                            data.children[nodeName] = parsedChild;
                        } else {
                            if (!data.children[nodeName]) {
                                data.children[nodeName] = [];
                            }
                            data.children[nodeName].push(parsedChild);
                        }
                    }
                }
            }
        },


        isChildWithUniqueTagName: function (parentNode, childTagName) {

            // first we check with the regular method, maybe we get lucky
            var totalElementsWithTagName = parentNode.getElementsByTagName(childTagName).length;
            if (totalElementsWithTagName === 0 || totalElementsWithTagName === 1) {
                return true;
            }

            // even if there are more nodes with the same tag name
            // they aren't necessary direct children of the parent
            // so we still need to check each child
            var childIndex = 0,
                totalChildren = parentNode.childNodes.length,
                foundChildTags = 0,
                childNames = {};
            for (; childIndex < totalChildren; childIndex++) {

                var childNode = parentNode.childNodes[childIndex],
                    nodeName = childNode.nodeName;

                if (nodeName === childTagName && childNames[childTagName]) {
                    return false;
                } else {
                    childNames[nodeName] = true;
                }

            }

            return true;
        },


        parseAttributes: function (xmlNode) {

            var jsonObject = {};

            if (xmlNode.attributes) {
                var attributes = xmlNode.attributes,
                    attributeIndex = 0,
                    totalAttributes = attributes.length;
                for (; attributeIndex < totalAttributes; attributeIndex++) {

                    var attribute = attributes[attributeIndex],
                        attributeName = attribute.nodeName,
                        attributeValue;
                    if (typeof attribute.value !== 'undefined') {
                        attributeValue = attribute.value;
                    } else if (typeof attribute.textContent !== 'undefined') {
                        attributeValue = attribute.textContent;
                    } else {
                        attributeValue = '';
                    }

                    jsonObject[attributeName] = attributeValue;
                }
            }

            return jsonObject;
        },


        serializeXmlNode: function (xmlNode) {

            if (typeof window.XMLSerializer != "undefined") {
                return (new window.XMLSerializer()).serializeToString(xmlNode);
            } else if (typeof xmlNode.xml != "undefined") {
                return xmlNode.xml;
            }
            return "";
        },
    };


    MainFrame.Ext.Publisher = {

        subscriptions: {},


        publish: function (event, params) {

            var event_subscriptions = MainFrame.Ext.Publisher.subscriptions[event];

            if (event_subscriptions) {

                var index = 0,
                    total_subscriptions = event_subscriptions.length;
                for (; index < total_subscriptions; index++) {

                    var subscription_data = event_subscriptions[index];
                    if (subscription_data && typeof subscription_data.callback === 'function') {
                        subscription_data.callback(event, params);
                    }
                }
            }
        },


        subscribe: function (event, callback) {

            if (this.validate(event, callback)) {

                if (!MainFrame.Ext.Publisher.subscriptions[event]) {
                    MainFrame.Ext.Publisher.subscriptions[event] = [];
                }

                var uid = 'subscriber_' + Date.now();
                MainFrame.Ext.Publisher.subscriptions[event].push({
                    id: uid,
                    event: event,
                    callback: callback
                });

                return MainFrame.Ext.Publisher.unsubscribe.bind(null, event, uid);
            }

            return false;
        },


        unsubscribe: function (event, callback_id) {

            var subscriptions = MainFrame.Ext.Publisher.subscriptions[event];

            if (subscriptions) {

                if (typeof uid === 'undefined') {
                    delete MainFrame.Ext.Publisher.subscriptions[event];
                    return true;
                } else {

                    var index = 0,
                        total_subscriptions = subscriptions.length;

                    for (; index < total_subscriptions; index++) {

                        var subscription_data = subscriptions[index];
                        if (subscription_data && subscription_data.id === callback_id) {
                            subscriptions.splice(index, 1);
                            if (subscriptions.length === 0) {
                                delete MainFrame.Ext.Publisher.subscriptions[event];
                            }
                        }
                    }
                }
            }

            return false;
        },


        validate: function () {

            var argIndex = 0,
                totalArguments = arguments.length;
            for (; argIndex < totalArguments; argIndex++) {

                var arg = arguments[argIndex];

                if (typeof arg === 'undefined' || arg === null) {
                    return false;
                }
            }

            return true;
        }
    };


    MainFrame.Ext.Router = (function () {


        var routes = {};


        function addDynamicRoute(route_string) {

            // dintr-o ruta de forma "tags/:tag_id" se construieste o expresie regulara
            // ce va identifica rutele de acest gen (ex. /tags\/(.*?)/)
            // si se salveaza numele parametrilor dinamici (in exemplul de mai sus e vorba de "tag_id")

            var route_parts = route_string.split('/'),
                route_params = {},
                route_regex = route_parts.map(function (route_part, index) {
                    var is_dynamic_part = (route_part.indexOf(':') !== -1);
                    if (is_dynamic_part) {
                        route_params[index] = route_part.replace(':', '');
                        return '(.*?)';
                    }

                    return route_part;
                }).join('\\/');

            routes[route_regex] = {
                params: route_params
            };
        }


        function getDynamicRouteEventAndParams(current_route, route_data) {

            // o ruta de forma "tags/1" corespunde unui event de ruta dinamica
            // de forma "tags/:tag_id"
            // deci decompunem ruta pentru a obtine forma generalizata de mai sus
            // si extragem parametrii (ex. { tag_id: 1 })

            var route_parts = current_route.split('/'),
                route_event_parts = [],
                route_event_params = {};
            route_parts.forEach(function (route_part, index) {

                var route_param = route_data.params[index];
                if (route_param) {
                    route_event_parts.push(':' + route_param);
                    route_event_params[route_param] = route_part;
                } else {
                    route_event_parts.push(route_part);
                }
            });

            var route_event = route_event_parts.join('/');

            return {
                event: route_event,
                params: route_event_params
            };
        }


        function on(route_string, callback) {

            var is_dynamic_route = (route_string.indexOf(':') !== -1);
            if (is_dynamic_route) {
                addDynamicRoute(route_string);
            }

            return MainFrame.Ext.Publisher.subscribe('route:' + route_string, callback);
        }

        function onHashchange() {

            var current_route = window.location.hash.replace('#', '');
            if (current_route) {

                MainFrame.Ext.Publisher.publish('route:' + current_route);

                for (var route_regex in routes) {
                    if (current_route.match(new RegExp(route_regex))) {

                        var route_data = routes[route_regex];
                        dynamic_route = getDynamicRouteEventAndParams(current_route, route_data);

                        MainFrame.Ext.Publisher.publish('route:' + dynamic_route.event, dynamic_route.params);
                    }
                }

            } else {
                MainFrame.Ext.Publisher.publish('route:' + '/');
            }
        }


        MainFrame.Ext.Utils.bind(window, 'hashchange', function () {
            onHashchange();
        });
        MainFrame.Ext.Utils.bind(window, 'load', function () {
            onHashchange();
        });

        return {
            on: on,
            routes: routes
        };
    })();


    MainFrame.Ext.Autoload = (function () {


        function getOptions(options_id) {

            var options_element = document.getElementById(options_id);
            options_string = options_element.innerHTML.trim(),
                options = {};

            try {
                options = JSON.parse(options_string);
            } catch (err) {
                MainFrame.Ext.Utils.log('Options for "' + options_id + '" parsing failed with error');
                MainFrame.Ext.Utils.log(err);
            }

            return options;
        }


        function autoload() {
            autoloadModules();
            autoloadImages();
        }

        function autoloadModules() {

            var ext_modules = document.getElementsByClassName('ext-module-js');

            var index = 0,
                total_modules = ext_modules.length;
            for (; index < total_modules; index++) {

                var ext_module_element = ext_modules[index];
                autoloadModule(ext_module_element);
            }
        }

        function autoloadModule(ext_module_element) {

            if (typeof ext_module_element.__autoloaded === 'undefined' || ext_module_element.__autoloaded !== true) {

                var module_name = ext_module_element.getAttribute('data-module'),
                    options_id = ext_module_element.getAttribute('data-options-id'),
                    options;

                if (options_id) {
                    options = getOptions(options_id);

                    if (typeof options.target === 'undefined') {
                        options.target = {
                            options: {
                                template_actions: {}
                            }
                        };
                    }

                    if (typeof options.data === 'undefined') {
                        options.data = {};
                    }
                } else {
                    options = {
                        target: {
                            options: {
                                template_actions: {}
                            }
                        },
                        data: {},
                        details: {}
                    };
                }

                if (typeof options.details === 'undefined') {
                    options.details = {};
                }

                options.target.element = ext_module_element;

                var data_items = ext_module_element.getAttribute('data-items');
                if (data_items) {
                    options.target.options.items = data_items;
                }

                var data_item_action_click = ext_module_element.getAttribute('data-item-action-click');
                if (data_item_action_click) {
                    options.target.options.template_actions['click'] = data_item_action_click;
                }

                var data_object_string = ext_module_element.getAttribute('data-data-object');
                if (data_object_string) {
                    var data = window[data_object_string];
                    if (data) {
                        options.data.object = data;
                    }
                }

                var data_url = ext_module_element.getAttribute('data-data-url');
                if (data_url) {
                    options.data.url = data_url;
                }

                var data_import = ext_module_element.getAttribute('data-data-import');
                if (data_import) {
                    options.data.import = data_import;
                }

                var data_static = ext_module_element.getAttribute('data-static');
                if (data_static && data_static === 'true') {
                    options.details.static = true;
                } else {
                    if (typeof options.data.object === 'undefined' &&
                        typeof options.data.url === 'undefined' &&
                        typeof options.data.import === 'undefined') {
                        options.details.static = true;
                    }
                }

                var attribute_index = 0,
                    total_attributes = ext_module_element.attributes.length;
                for (; attribute_index < total_attributes; attribute_index++) {

                    var attr = ext_module_element.attributes[attribute_index];
                    if (attr.name.indexOf('data-option-') === 0) {
                        var option_name = attr.name.replace('data-option-', '');
                        options.details[option_name] = attr.value;
                    }

                    if (attr.name.indexOf('data-template-') === 0) {
                        // atributul de forma data-template-id
                        // devine proprietatea options.template_id
                        var option_name = attr.name.replace('data-', '');
                        option_name = option_name.replace(/\-/g, '_');
                        options.target.options[option_name] = attr.value;
                    }
                }

                var data_bind_to = ext_module_element.getAttribute('data-bind-to');

                var module_instance = new MainFrame.Ext[module_name](options);
                if (data_bind_to) {
                    window[data_bind_to] = module_instance;
                }

                ext_module_element.__autoloaded = true;
                ext_module_element.style.visibility = 'visible';
            }
        }

        var load_on_scroll_containers,
            load_on_scroll_offsets;

        function setLoadOnScrollContainerOffsets() {

            load_on_scroll_containers = document.getElementsByClassName('load-on-scroll-js');
            load_on_scroll_offsets = {};

            var index = 0,
                total_containers = load_on_scroll_containers.length;
            for (; index < total_containers; index++) {

                var container = load_on_scroll_containers[index],
                    offset = container.offsetTop;

                load_on_scroll_offsets[offset] = container;
            }
        }

        var OFFSET_THRESHOLD = -200;

        function checkAndLoadImages() {

            var scroll_top = document.body.scrollTop;

            for (var offset in load_on_scroll_offsets) {

                var container = load_on_scroll_offsets[offset];
                if (scroll_top - OFFSET_THRESHOLD >= offset && !container._async_loaded) {

                    var async_bg_images = container.getElementsByClassName('async-bg-js'),
                        index = 0,
                        total_images = async_bg_images.length;
                    for (; index < total_images; index++) {

                        var bg_image = async_bg_images[index],
                            bg_image_src = bg_image.getAttribute('data-src');
                        bg_image.style.backgroundImage = 'url(\'' + bg_image_src + '\')';
                    }

                    var async_images = container.getElementsByClassName('async-img-js'),
                        index = 0,
                        total_images = async_images.length;
                    for (; index < total_images; index++) {

                        var regular_image = async_images[index],
                            regular_image_src = regular_image.getAttribute('data-src');
                        regular_image.src = regular_image_src;
                    }

                    container._async_loaded = true;
                }
            }
        }

        function autoloadImages() {

            setLoadOnScrollContainerOffsets();

            MainFrame.Ext.Utils.bind(document.body, 'scroll', function (ev) {
                checkAndLoadImages();
            });

            checkAndLoadImages();

            MainFrame.Ext.Utils.bind(window, 'resize', function () {
                setLoadOnScrollContainerOffsets();
                checkAndLoadImages();
            });
        }


        MainFrame.Ext.Utils.bind(window, 'load', autoload);

        return {
            autoload: autoload,
            autoloadImages: autoloadImages,
            autoloadModule: autoloadModule,
            autoloadModules: autoloadModules
        }
    })();

})();

//ExtMap


MainFrame.Ext.Map = MainFrame.Ext.Module.extend({


    css_classes: {

        'zones': 'map__zones',
        'zone': 'map__zone',
        'hotspot': 'map__hotspot',
        'hotspot_label': 'map__hotspot__label'
    },


    customInit: function () {

        var self = this;
        self.init();

        self.bindEvents();
    },


    init: function () {

        var self = this;

        self.TARGET.element.innerHTML += '<div class="' + self.css_classes['zones'] + '"></div>';
        self.TARGET.map = self.TARGET.element.getElementsByTagName('img')[0];
        self.TARGET.zones = self.TARGET.element.getElementsByTagName('div')[0];

        if (self.DETAILS.size) {
            var size_parts = self.DETAILS.size.split('|'),
                width = parseInt(size_parts[0], 10),
                height = parseInt(size_parts[1], 10);

            self.DETAILS.normal_size = {
                width: width,
                height: height
            };
        }

        MainFrame.Ext.Utils.request.get(self.DETAILS.data)
            .then(function (response) {

                if (response.request.responseXML) {
                    var xml_data = response.request.responseXML.documentElement;
                    self.DATA = self.processXMLData(xml_data);
                    self.initMap();
                } else if (response.data) {
                    self.DATA = self.processData(response.data);
                    self.initMap();
                }
            })
            .catch(function (error) {
                MainFrame.Ext.Utils.log(error);
            });

    },


    bindEvents: function () {

        var self = this;
    },


    processXMLData: function (xml_data) {

        var self = this;

        var refs = [],
            zones = [];

        var index = 0,
            total_nodes = xml_data.childNodes.length;
        for (; index < total_nodes; index++) {

            var child_node = xml_data.childNodes[index];
            switch (child_node.nodeName) {
                case 'ref':
                    var ref = self.parseRef(child_node);
                    refs.push(ref);
                    break;

                case 'zone':
                    var zone = self.parseZone(child_node);
                    zones.push(zone);
                    break;
            }
        }

        return {
            refs: refs,
            zones: zones
        };
    },


    parseRef: function (ref_node) {

        var ref = {};

        ref.type = ref_node.getAttribute('type');

        var index = 0,
            total_nodes = ref_node.childNodes.length;
        for (; index < total_nodes; index++) {

            var child_node = ref_node.childNodes[index];
            if (child_node.getAttribute) {
                ref[child_node.nodeName] = MainFrame.Ext.Parser.getCDATA(child_node);
            }
        }

        return ref;
    },


    parseZone: function (zone_node) {

        var self = this;

        var zone = {};

        zone.refs = [];

        zone.zone_id = zone_node.getAttribute('id');

        var display = zone_node.getAttribute('display');
        if (display) {
            zone.display = display;
        } else {
            zone.display = 'hs';
        }

        var index = 0,
            total_nodes = zone_node.childNodes.length;
        for (; index < total_nodes; index++) {

            var child_node = zone_node.childNodes[index];
            switch (child_node.nodeName) {

                case 'rec':
                    var rec = MainFrame.Ext.Parser.getCDATA(child_node),
                        rec_parts = rec.split('|'),
                        rec_x = parseFloat(rec_parts[0], 10),
                        rec_y = parseFloat(rec_parts[1], 10),
                        rec_width = parseFloat(rec_parts[2], 10),
                        rec_height = parseFloat(rec_parts[3], 10);

                    zone.x = rec_x * 100 / self.DETAILS.normal_size.width;
                    zone.y = rec_y * 100 / self.DETAILS.normal_size.height;
                    zone.width = rec_width * 100 / self.DETAILS.normal_size.width;
                    zone.height = rec_height * 100 / self.DETAILS.normal_size.height;
                    break;

                case 'ref':
                    zone.refs.push(MainFrame.Ext.Parser.getCDATA(child_node));
                    break;

                // falling through
                case 'id':
                case 'alt':
                default:
                    if (child_node.getAttribute) {
                        zone[child_node.nodeName] = MainFrame.Ext.Parser.getCDATA(child_node);
                    }
                    break;

            }
        }

        return zone;
    },


    getRefs: function (zone) {

        var self = this;

        var refs = [];

        if (zone.refs.length) {
            zone.refs.forEach(function (ref_id) {
                var ref = self.getRefById(ref_id);
                if (ref) {
                    refs.push(ref);
                }
            });
        }

        return refs;
    },


    getRefById: function (ref_id) {

        var self = this;

        var index = 0,
            total_refs = self.DATA.refs.length;
        for (; index < total_refs; index++) {

            var ref = self.DATA.refs[index];
            if (ref.id === ref_id) {
                return ref;
            }
        }

        return null;
    },


    initMap: function () {

        var self = this;

        self.createZones();
        self.bindZonesActions();
    },


    createZones: function () {

        var self = this;

        var zones_string = '';

        self.DATA.zones.forEach(function (zone) {

            zones_string += '<div class="' + self.css_classes['zone'] + '" data-zone-id="' + zone.id + '" style="'
                + 'left: ' + zone.x + '%; top: ' + zone.y + '%; width: ' + zone.width + '%; height: ' + zone.height + '%;'
                + '">' + self.getZoneHotspotString(zone) + '</div>';
        });

        self.TARGET.zones.innerHTML = zones_string;
    },


    getZoneHotspotString: function (zone) {

        var self = this;

        var hotspot_string = '';

        if (zone.display === 'hs') {

            var refs = self.getRefs(zone);
            if (refs.length) {
                // TODO: teoretic ar trebui verificari de zone multi...
                // practic sarim o etapa
                var ref = refs[0];
                if (ref.alt) {
                    hotspot_string += '<span class="' + self.css_classes['hotspot_label'] + '">' + ref.alt + '</span>';
                }
            }
            hotspot_string += '<img class="' + self.css_classes['hotspot'] + '" src="assets/img/hotspot.png"/>';
        }

        return hotspot_string;
    },


    bindZonesActions: function () {

        var self = this;

        var zone_elements = self.TARGET.zones.getElementsByClassName(self.css_classes['zone']),
            index = 0,
            total_zones = zone_elements.length;
        for (; index < total_zones; index++) {

            var zone_element = zone_elements[index],
                zone_id = zone_element.getAttribute('data-zone-id'),
                zone = self.getZoneById(zone_id);

            if (zone && zone.refs.length) {
                self.bindZoneAction(zone_element, zone);
            }
        }
    },


    getZoneById: function (id) {

        var self = this;

        var index = 0,
            total_zones = self.DATA.zones.length;
        for (; index < total_zones; index++) {

            var zone = self.DATA.zones[index];
            if (zone.id === id) {
                return zone;
            }
        }

        return null;
    },


    bindZoneAction: function (zone_element, zone_data) {

        var self = this;

        if (zone_data.display === 'hs') {
            var target = zone_element.getElementsByClassName(self.css_classes['hotspot'])[0];
        } else {
            var target = zone_element;
        }

        target.onclick = function () {
            console.log(zone_data)
        }
    }

});

//ExtTemplate


MainFrame.Ext.Template = MainFrame.Ext.Module.extend({


    customInit: function () {

        var self = this;
        self.init();

        self.bindEvents();
    },


    init: function () {

        var self = this;

        var items_string = self.TARGET.options.items,
            template_string = self.getTemplate(),
            template_actions = self.TARGET.options.template_actions,
            target_element = self.TARGET.element,
            items = self.getItems(items_string);

        if (typeof self.DETAILS.append === 'undefined' || self.DETAILS.append === 'false' || !self.DETAILS.append) {
            target_element.innerHTML = '';
        }

        var create_result = self.createItems({
            target: target_element,
            items: items,
            template: template_string
        });

        self.CONTENT = {
            items: [],
            data: items,
            element: target_element,
            template_string: template_string
        };


        var target_elements = target_element.childNodes,
            index = 0,
            total_items_in_target = target_elements.length;
        for (; index < total_items_in_target; index++) {

            var element = target_elements[index],
                data = self.DATA[index];

            self.CONTENT.items.push({
                element: element,
                data: data
            });

            if (template_actions) {
                for (var event_name in template_actions) {
                    (function (event_name, data, element, DATA) {
                        MainFrame.Ext.Utils.bind(element, event_name, function (ev) {
                            if (typeof template_actions[event_name] === 'string') {
                                var event_action = window[template_actions[event_name]];
                            } else {
                                var event_action = template_actions[event_name];
                            }
                            event_action(ev, data, element, DATA);
                        });
                    })(event_name, data, element, self.DATA);
                }
            }
        }
    },


    bindEvents: function () {

        var self = this;
    },


    processItem: function (item, options) {

        if (typeof options === 'undefined') {
            options = {};
        }

        return item;
    },


    interpretTemplate: function (item, template, target) {

        var self = this;
        return MainFrame.Ext.Utils.fillStringWithData(template, item);
    }

});

//ExtBlocksGrid


MainFrame.Ext.BlocksGrid = MainFrame.Ext.Module.extend({

    _SCROLLBAR_WIDTH: 0,


    customInit: function () {

        var self = this;
        self.initialComputations();

        self.init();

        self.bindEvents();
    },


    init: function () {

        var self = this;

        if (self.DETAILS.static) {
            self.init_static();
        } else {
            self.init_dynamic();
        }
    },


    init_static: function () {

        var self = this;

        var target_element = self.TARGET.element;

        self.CONTENT = {
            items: [],
            data: null,
            element: target_element
        };

        var target_elements = target_element.childNodes,
            index = 0,
            total_items_in_target = target_elements.length;
        for (; index < total_items_in_target; index++) {

            var element = target_elements[index];

            if (element.getAttribute) {

                var data = self.getStaticBlockData(element);

                self.CONTENT.items.push({
                    element: element,
                    data: data
                });
            }
        }

        self.resizeItems();
    },


    getStaticBlockData: function (element) {

        var data = {};

        var size = element.getAttribute('data-size');
        if (size) {
            data.size = size;
        }

        data.responsive_size = {};

        var index = 0,
            total_attributes = element.attributes.length;
        for (; index < total_attributes; index++) {

            var attribute = element.attributes[index];
            if (attribute.name.indexOf('data-size-') !== -1) {
                var size_name = attribute.name.split('data-size-')[1];
                data.responsive_size[size_name] = element.getAttribute(attribute.name);
            }
        }

        return data;
    },


    init_dynamic: function () {

        var self = this;

        var items_string = self.TARGET.options.items,
            template_string = self.getTemplate(),
            template_actions = self.TARGET.options.template_actions,
            target_element = self.TARGET.element,
            items = self.getItems(items_string);

        target_element.style.fontSize = '0';

        target_element.innerHTML = '';

        var create_result = self.createItems({
            target: target_element,
            items: items,
            template: template_string
        });

        self.CONTENT = {
            items: [],
            data: items,
            element: target_element,
            template_string: template_string
        };


        var target_elements = target_element.childNodes,
            index = 0,
            total_items_in_target = target_elements.length;
        for (; index < total_items_in_target; index++) {

            var element = target_elements[index],
                data = self.DATA[index];

            self.CONTENT.items.push({
                element: element,
                data: data
            });

            if (template_actions) {
                for (var event_name in template_actions) {
                    (function (data, element, DATA) {
                        MainFrame.Ext.Utils.bind(element, event_name, function (ev) {
                            if (typeof template_actions[event_name] === 'string') {
                                var event_action = window[template_actions[event_name]];
                            } else {
                                var event_action = template_actions[event_name];
                            }
                            event_action(ev, data, element, DATA);
                        });
                    })(data, element, self.DATA);
                }
            }
        }
    },


    bindEvents: function () {

        var self = this;
        MainFrame.Ext.Utils.bind(window, 'resize', function (e) {
            self.resizeItems();
        });
    },


    interpretTemplate: function (item, template, target) {

        var self = this;

        var settings = self.getResponsiveSettings(target, self.DETAILS.block_settings),
            items_per_line = self.getItemsPerLine(target),
            width = Math.floor((target.clientWidth - self._SCROLLBAR_WIDTH) / items_per_line),
            height = width / settings.ratio;

        item.WIDTH = width;
        item.HEIGHT = height;

        return MainFrame.Ext.Utils.fillStringWithData(template, item);
    },


    getItemsPerLine: function (target) {

        var self = this;

        var settings = self.DETAILS.block_settings,
            items_per_line = settings.items_per_line;

        if (settings.responsive) {

            var current_width = target.clientWidth,
                responsive_widths = [];

            for (var width in settings.responsive) {
                responsive_widths.push(width);
            }
            responsive_widths.sort();

            responsive_widths.forEach(function (width) {
                if (current_width <= width) {
                    items_per_line = settings.responsive[width].items_per_line;
                }
            });
        }

        return items_per_line;
    },


    resizeItems: function () {

        var self = this;

        if (self.DETAILS.static) {
            self.resizeItems_static();
        } else {
            self.resizeItems_dynamic();
        }
    },


    resizeItems_static: function () {

        var self = this;

        var settings = self.getResponsiveSettings(self.CONTENT.element, self.DETAILS.block_settings),
            items_per_line = settings.items_per_line,
            available_width = self.CONTENT.element.clientWidth - self._SCROLLBAR_WIDTH,
            ratio = settings.ratio;

        self.CONTENT.items.forEach(function (item) {

            var size = self.getResponsiveSizeForItem(item),
                size_parts = size.split('x')
            size_width = parseFloat(size_parts[0], 10),
                size_height = size_parts[1];

            if (size_height === 'auto') {

                var item_width = size_width / items_per_line * available_width;

                item.element.style.width = item_width + 'px';
                item.element.style.height = 'auto';

            } else {

                var size_height = parseFloat(size_parts[1], 10),
                    ratio = size_width / size_height;

                var item_width = size_width / items_per_line * available_width,
                    item_height = item_width / ratio;

                item.element.style.width = item_width + 'px';
                item.element.style.height = item_height + 'px';
            }
        });
    },


    getResponsiveSizeForItem: function (item) {

        var self = this;

        var current_width = self.TARGET.element.clientWidth,
            responsive_sizes = [];

        for (var width in item.data.responsive_size) {
            responsive_sizes.push(width);
        }
        responsive_sizes.sort();

        var index = 0,
            total_sizes = responsive_sizes.length;
        for (; index < total_sizes; index++) {
            var width = responsive_sizes[index];
            if (current_width <= width) {
                return item.data.responsive_size[width];
            }
        }

        return item.data.size;
    },


    resizeItems_dynamic: function () {

        var self = this;

        var settings = self.getResponsiveSettings(self.CONTENT.element, self.DETAILS.block_settings),
            items_per_line = settings.items_per_line,
            width = Math.floor((self.CONTENT.element.clientWidth) / items_per_line),
            height = width / settings.ratio;

        self.CONTENT.items.forEach(function (item) {
            item.element.style.width = width + 'px';
            item.element.style.height = height + 'px';
        });
    },


    initialComputations: function () {

        var self = this;

        var scroll_div = document.createElement("div");
        scroll_div.style.width = '100px';
        scroll_div.style.height = '100px';
        scroll_div.style.overflow = 'scroll';
        scroll_div.style.position = 'absolute';
        scroll_div.style.top = '-9999px';

        document.body.appendChild(scroll_div);

        self._SCROLLBAR_WIDTH = scroll_div.offsetWidth - scroll_div.clientWidth;

        document.body.removeChild(scroll_div);
    }

});

//ExtWishlist


MainFrame.Ext.Wishlist = MainFrame.Ext.Module.extend({


    initialize: function (options) {

        var self = this;

        self._initialize(options);

        if (options.id) {
            self._id = options.id;
        } else {
            self._id = 'wishlist';
        }

        if (options.id_property) {
            self._id_property = options.id_property;
        } else {
            self._id_property = 'id';
        }

        if (options.price_property) {
            self._price_property = options.price_property;
        } else {
            self._price_property = 'price';
        }

        self._storage_key = 'ext.wishlist.' + self._id;

        var list = [];

        var saved_wishlist = window.localStorage.getItem(self._storage_key);
        if (saved_wishlist) {
            self.list = JSON.parse(saved_wishlist);
        } else {
            self.list = [];
        }

        MainFrame.Ext.Publisher.publish('wishlist:update');
    },


    addProduct: function (product, quantity) {

        var self = this;

        if (typeof quantity === 'undefined') {
            quantity = 1;
        } else {
            quantity = parseInt(quantity, 10);
        }

        var product_copy = JSON.parse(JSON.stringify(product));
        product_copy._quantity = quantity;

        var existing_product = self.getProductById(product_copy[self._id_property]);
        if (existing_product) {
            existing_product._quantity += quantity;
        } else {
            self.list.push(product_copy);
        }

        self.save();

        MainFrame.Ext.Publisher.publish('wishlist:update');
    },


    getProductById: function (id) {

        var self = this;

        var index = 0,
            total_products = self.list.length;
        for (; index < total_products; index++) {

            var product = self.list[index];
            if (product[self._id_property] === id) {
                return product;
            }
        }

        return null;
    },


    removeProduct: function (product_id) {

        var self = this;

        var index = 0,
            index_to_remove = -1,
            total_products = self.list.length;
        for (; index < total_products; index++) {

            var product = self.list[index];
            if (product[self._id_property] === product_id) {
                index_to_remove = index;
            }
        }

        if (index_to_remove != -1) {

            self.list.splice(index_to_remove, 1);

            self.save();

            MainFrame.Ext.Publisher.publish('wishlist:update');
        }
    },


    incrementProductQuantity: function (product_id, increment) {

        var self = this;

        var product = self.getProductById(product_id);
        if (product) {
            product._quantity += increment;

            self.save();

            MainFrame.Ext.Publisher.publish('wishlist:update');
        }
    },


    empty: function () {

        var self = this;

        self.list = [];

        self.save();

        MainFrame.Ext.Publisher.publish('wishlist:update');
    },


    save: function () {

        var self = this;

        window.localStorage.setItem(self._storage_key, JSON.stringify(self.list));
    },


    getProducts: function () {

        return this.list;
    },


    getTotal: function () {

        var self = this;

        var index = 0,
            total_quantities = 0,
            total_price = 0,
            total_products = self.list.length;
        for (; index < total_products; index++) {

            var product = self.list[index];
            total_quantities += product._quantity;

            if (product[self._price_property]) {
                total_price += parseFloat(product[self._price_property], 10) * product._quantity;
            }
        }

        total_price = total_price.toFixed(2);

        return {
            price: total_price,
            products: total_products,
            quantities: total_quantities
        };
    }

});

//ExtWishlistBadge


MainFrame.Ext.WishlistBadge = MainFrame.Ext.Module.extend({

    customInit: function () {

        var self = this;

        if (self.DETAILS.wishlist_id) {
            self.DETAILS.wishlist = window[self.DETAILS.wishlist_id];
        }

        MainFrame.Ext.Publisher.subscribe('wishlist:update', self.onUpdate.bind(self));
        self.onUpdate();
    },


    onUpdate: function () {

        var self = this;

        var text = self.DETAILS.text,
            total = self.DETAILS.wishlist.getTotal();
        text = text.replace(/\#products/gmi, total.products + '');
        text = text.replace(/\#quantities/gmi, total.quantities + '');
        text = text.replace(/\#price/gmi, total.price + '');

        self.TARGET.element.innerHTML = text;
    }

});


//store


/*!
    query-string
    Parse and stringify URL query strings
    https://github.com/sindresorhus/query-string
    by Sindre Sorhus
    MIT License
*/
(function () {
    'use strict';
    var queryString = {};

    if (typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
        }
    }

    // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/keys
    if (!Object.keys) {
        Object.keys = (function () {
            'use strict';
            var hasOwnProperty = Object.prototype.hasOwnProperty,
                hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
                dontEnums = [
                    'toString',
                    'toLocaleString',
                    'valueOf',
                    'hasOwnProperty',
                    'isPrototypeOf',
                    'propertyIsEnumerable',
                    'constructor'
                ],
                dontEnumsLength = dontEnums.length;

            return function (obj) {
                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                    throw new TypeError('Object.keys called on non-object');
                }

                var result = [], prop, i;

                for (prop in obj) {
                    if (hasOwnProperty.call(obj, prop)) {
                        result.push(prop);
                    }
                }

                if (hasDontEnumBug) {
                    for (i = 0; i < dontEnumsLength; i++) {
                        if (hasOwnProperty.call(obj, dontEnums[i])) {
                            result.push(dontEnums[i]);
                        }
                    }
                }
                return result;
            };
        }());
    }


    // Production steps of ECMA-262, Edition 5, 15.4.4.19
    // Reference: http://es5.github.com/#x15.4.4.19
    if (!Array.prototype.map) {
        Array.prototype.map = function (callback, thisArg) {

            var T, A, k;

            if (this == null) {
                throw new TypeError(" this is null or not defined");
            }

            // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
            var O = Object(this);

            // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
            // 3. Let len be ToUint32(lenValue).
            var len = O.length >>> 0;

            // 4. If IsCallable(callback) is false, throw a TypeError exception.
            // See: http://es5.github.com/#x9.11
            if (typeof callback !== "function") {
                throw new TypeError(callback + " is not a function");
            }

            // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
            if (thisArg) {
                T = thisArg;
            }

            // 6. Let A be a new array created as if by the expression new Array(len) where Array is
            // the standard built-in constructor with that name and len is the value of len.
            A = new Array(len);

            // 7. Let k be 0
            k = 0;

            // 8. Repeat, while k < len
            while (k < len) {

                var kValue, mappedValue;

                // a. Let Pk be ToString(k).
                //   This is implicit for LHS operands of the in operator
                // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
                //   This step can be combined with c
                // c. If kPresent is true, then
                if (k in O) {

                    // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
                    kValue = O[k];

                    // ii. Let mappedValue be the result of calling the Call internal method of callback
                    // with T as the this value and argument list containing kValue, k, and O.
                    mappedValue = callback.call(T, kValue, k, O);

                    // iii. Call the DefineOwnProperty internal method of A with arguments
                    // Pk, Property Descriptor {Value: mappedValue, : true, Enumerable: true, Configurable: true},
                    // and false.

                    // In browsers that support Object.defineProperty, use the following:
                    // Object.defineProperty(A, Pk, { value: mappedValue, writable: true, enumerable: true, configurable: true });

                    // For best browser support, use the following:
                    A[k] = mappedValue;
                }
                // d. Increase k by 1.
                k++;
            }

            // 9. return A
            return A;
        };
    }


    if (!Array.isArray) {
        Array.isArray = function (arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }


    queryString.parse = function (str) {
        if (typeof str !== 'string') {
            return {};
        }

        str = str.trim().replace(/^\?/, '');

        if (!str) {
            return {};
        }

        return str.trim().split('&').reduce(function (ret, param) {
            var parts = param.replace(/\+/g, ' ').split('=');
            var key = parts[0];
            var val = parts[1];

            key = decodeURIComponent(key);
            // missing `=` should be `null`:
            // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
            val = val === undefined ? null : decodeURIComponent(val);

            if (!ret.hasOwnProperty(key)) {
                ret[key] = val;
            } else if (Array.isArray(ret[key])) {
                ret[key].push(val);
            } else {
                ret[key] = [ret[key], val];
            }

            return ret;
        }, {});
    };

    queryString.stringify = function (obj) {
        return obj ? Object.keys(obj).map(function (key) {
            var val = obj[key];

            if (Array.isArray(val)) {
                return val.map(function (val2) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent(val2);
                }).join('&');
            }

            return encodeURIComponent(key) + '=' + encodeURIComponent(val);
        }).join('&') : '';
    };

    queryString.push = function (key, new_value) {
        var params = queryString.parse(location.search);
        params[key] = new_value;
        var new_params_string = queryString.stringify(params)
        if (history && history.pushState) {
            //history.pushState({}, "", window.location.pathname + '?' + new_params_string);
        }
    }

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = queryString;
    } else {
        window.queryString = queryString;
    }
})();


(function () {

    !function (e) {
        var n = !1;
        if ("function" == typeof define && define.amd && (define(e), n = !0), "object" == typeof exports && (module.exports = e(), n = !0), !n) {
            var o = window.Cookies, t = window.Cookies = e();
            t.noConflict = function () {
                return window.Cookies = o, t
            }
        }
    }(function () {
        function g() {
            for (var e = 0, n = {}; e < arguments.length; e++) {
                var o = arguments[e];
                for (var t in o) n[t] = o[t]
            }
            return n
        }

        return function e(l) {
            function C(e, n, o) {
                var t;
                if ("undefined" != typeof document) {
                    if (1 < arguments.length) {
                        if ("number" == typeof(o = g({path: "/"}, C.defaults, o)).expires) {
                            var r = new Date;
                            r.setMilliseconds(r.getMilliseconds() + 864e5 * o.expires), o.expires = r
                        }
                        o.expires = o.expires ? o.expires.toUTCString() : "";
                        try {
                            t = JSON.stringify(n), /^[\{\[]/.test(t) && (n = t)
                        } catch (e) {
                        }
                        n = l.write ? l.write(n, e) : encodeURIComponent(String(n)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent), e = (e = (e = encodeURIComponent(String(e))).replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent)).replace(/[\(\)]/g, escape);
                        var i = "";
                        for (var c in o) o[c] && (i += "; " + c, !0 !== o[c] && (i += "=" + o[c]));
                        return document.cookie = e + "=" + n + i
                    }
                    e || (t = {});
                    for (var a = document.cookie ? document.cookie.split("; ") : [], s = /(%[0-9A-Z]{2})+/g, f = 0; f < a.length; f++) {
                        var p = a[f].split("="), d = p.slice(1).join("=");
                        this.json || '"' !== d.charAt(0) || (d = d.slice(1, -1));
                        try {
                            var u = p[0].replace(s, decodeURIComponent);
                            if (d = l.read ? l.read(d, u) : l(d, u) || d.replace(s, decodeURIComponent), this.json) try {
                                d = JSON.parse(d)
                            } catch (e) {
                            }
                            if (e === u) {
                                t = d;
                                break
                            }
                            e || (t[u] = d)
                        } catch (e) {
                        }
                    }
                    return t
                }
            }

            return (C.set = C).get = function (e) {
                return C.call(C, e)
            }, C.getJSON = function () {
                return C.apply({json: !0}, [].slice.call(arguments))
            }, C.defaults = {}, C.remove = function (e, n) {
                C(e, "", g(n, {expires: -1}))
            }, C.withConverter = e, C
        }(function () {
        })
    });


    var V7_Store = function () {

        var storeCookie = 'store_o7',
            storeGETparam = 'shop',
            validShops = ['arad', 'brasov', 'militari', 'oradea', 'orhideea', 'pantelimon', 'baneasa', 'pitesti', 'ploiesti', 'test_pantelimon', 'constanta', 'braila', 'focsani', 'suceava', 'drobeta', 'calarasi', 'pantelimon_wall'],
            shopLabels = {
                'arad': 'Arad',
                'brasov': 'Brașov',
                'militari': 'Militari',
                'oradea': 'Oradea',
                'orhideea': 'Orhideea',
                'pantelimon': 'Pantelimon',
                'baneasa': 'Băneasa',
                'pitesti': 'Pitești',
                'ploiesti': 'Ploiești',
                'test_pantelimon': 'Pantelimon',
                'constanta': 'Constanța',
                'braila': 'Brăila',
                'focsani': 'Focșani',
                'suceava': 'Suceava',
                'drobeta': 'Drobeta',
                'calarasi': 'Călărași',
                'pantelimon_wall': 'Pantelimon'
            };

        var isValid = function (store) {
            for (var shop in validShops) {
                if (validShops[shop] == store)
                    return true
            }
            return false
        };


        var get = function () {

            var store = null;

            var parsed = queryString.parse(location.search),
                storeGETvalue = parsed[storeGETparam];

            if (storeGETvalue) {
                store = storeGETvalue.toLowerCase();
            } else if (Cookies.get(storeCookie)) {
                var storeCookieValue = Cookies.get(storeCookie);
                if (storeCookieValue && storeCookieValue.toLowerCase) {
                    store = storeCookieValue.toLowerCase();
                }
            } else if (Cookies.get('storeSlug')) {
                store = Cookies.get('storeSlug');
            }

            if (store) {
                store = store.split("ă").join("a");
                store = store.split("ș").join("s");
                store = store.split("ț").join("t");
                store = store.split("Ä").join("a");
                store = store.split("Č").join("s");
                store = store.split("Č").join("t");
            }

            return store;
        };

        var getLabel = function () {

            var store = get();
            if (store) {
                return shopLabels[store];
            } else {
                return 'Store';
            }
        };

        var set = function (store) {
            if (store) {
                queryString.push(storeGETparam, store);
                var newStore = store.substring(0, 1).toUpperCase() + store.substring(1, store.length);
                Cookies.set(storeCookie, newStore);

                var city = document.getElementById('city');
                if (city) {
                    city.innerHTML = getLabel();
                }

                return true;
            }
            return false;
        };


        return {
            get: get,
            set: set,
            isValid: isValid,
            getLabel: getLabel
        }

    };


    window.V7_Store = new V7_Store();

})();


//tracker

(function () {

    MainFrame = window.MainFrame || {};

    MainFrame.Tracker = {

        appId: 'W0027',

        // 'tagbleu.net', 'easy-publication.com'
        disallowedDomains: ['dGFnYmxldS5uZXQ=', 'ZWFzeS1wdWJsaWNhdGlvbi5jb20=', 'MTkyLjE2OA==', 'cHJlc3RpbWVkaWE='],

        // the main.xml id of the catalog
        mainModuleId: 'catalog',

        trackingType: "events", //events - new tracking | pageviews - for the old tracking | both - for sending pageviews and events
        useBD: true, //big data
        config: {},
        trackers: [],
        trackedEvents: [],
        loadedTrackers: 0,
        trackersCounter: 0,
        gaVersion: 'analytics',
        hasTrackedOpening: false,
        v: '1.0.0',
        oras: '',

        initializeTracker: function () {

            var testingServer = false;

            var currentDomain = window.location.hostname;

            this.disallowedDomains.forEach(function (domain) {

                var decodedDomain = MainFrame.Tracker.base64decode(domain);
                if (currentDomain.indexOf(decodedDomain) !== -1) {
                    testingServer = true;
                }
            });

            var currentUrl = window.location.href;
            if (currentUrl.indexOf('trackingTest=true') !== -1) {
                testingServer = true;
            }

            if (testingServer) {
                MainFrame.Tracker.callTracker = MainFrame.Tracker.callTrackerMockup;
            }

            MainFrame.Tracker.init({
                appId: 'W0027',
                type: 'GA',
                vars: {'_setAccount': 'UA-5850067-16'}
                //vars: {'_setAccount': 'UA-xxxxxxx-xx'}
            });
        },

        //init tracker options
        init: function (options) {
            this.setDefaultCodes(options);
            this.gaVersion = options.version ? options.version : this.gaVersion;
            this.trackers.push(options);
            this.trackersCounter++;
            this.loadTrackers(options);

            this.appId = options.appId;
            this.oras = V7_Store.get();
        },

        //setting default available codes
        setDefaultCodes: function (options) {
            var trackedEventCodes = options.codes;

            var eventCodes = [
                'page_changed',//page changed

                //menu
                'menu_open',//menu clicked
                'menu_close',//menu clicked
                'share_facebook',//share clicked

                //products
                'product_open',//

                //wishlist
                'wishlist_open',//wishlist clicked
                'wishlist_close', //
                'wishlist_add', //
                'wishlist_remove', //
                'wishlist_empty', //
                'wishlist_print', //
                'wishlist_email', //
                'wishlist_email_error', //

                //descopera gama
                'gama_open',

                //citeste articol
                'articol_open',

                //deschidere url
                'url_open',

            ];

            options.codes = eventCodes;
        },

        //sending events and pageviews
        trackEvent: function (trackOptions) {

            var eventCode = trackOptions.eventCode;
            var actionTrigger = trackOptions.actionTrigger;
            var actionInitiator = trackOptions.actionInitiator;
            var actionTarget = trackOptions.actionTarget;
            var eventValues = trackOptions.extraValues;
            var forceTracking = false;

            var i, j;
            var trackedEvent;
            var trackedObjects = [];

            switch (eventCode) {

                //page changed
                case 'page_changed':
                    if (this.hasTrackedOpening == false) {
                        trackedObjects.push({type: "page_view", eventString: '/PageEvents/view/' + eventValues.page, eventValues: {page: eventValues.page}});
                        this.hasTrackedOpening = true;
                    } else {
                        trackedObjects.push({type: "page_view", eventString: '/PageEvents/view/' + eventValues.page, eventValues: {page: eventValues.page}});
                    }
                    break;
                //////////////////////////

                //menu
                case 'menu_open':
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ButtonsEvents" + "|" + eventValues.page, eventLabel: "menu_open", eventValues: {page: eventValues.page}});
                    break;
                case 'menu_close':
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ButtonsEvents" + "|" + eventValues.page, eventLabel: "menu_close", eventValues: {page: eventValues.page}});
                    break;
                case 'share_facebook': //share facebook
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "CatalogueEvents" + "|" + eventValues.page, eventLabel: "share|facebook"});
                    break;

                //wishlist
                case 'wishlist_open': //
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ButtonsEvents" + "|" + eventValues.page, eventLabel: "wishlist_open"});
                    break;
                case 'wishlist_close': //
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ButtonsEvents", eventLabel: "wishlist_close"});
                    break;
                case 'wishlist_add': //
                    var productId = eventValues.id;
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ProductEvents" + "|" + productId, eventLabel: "addToWishlist", eventValues: {url: productId}});
                    break;
                case 'wishlist_remove': //
                    var productId = eventValues.id;
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ProductEvents" + "|" + productId, eventLabel: "removeFromWishlist", eventValues: {url: productId}});
                    break;
                case 'wishlist_empty': //
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "WishlistEvents", eventLabel: "empty"});
                    break;
                case 'wishlist_print': //
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "WishlistEvents", eventLabel: "print"});
                    break;
                case 'wishlist_email': //
                    var trackValue = "";
                    var wishlistManager = wishlist;
                    for (var key in wishlistManager.list) {
                        var product = wishlistManager.list[key];
                        trackValue += product.id + "," + product._quantity + ";";
                    }
                    trackValue = trackValue.substring(0, trackValue.length - 1);
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "WishlistEvents" + "|" + trackValue, eventLabel: "mail", eventValues: {url: trackValue}});
                    break;
                case 'wishlist_email_error': //
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "WishlistEvents", eventLabel: "mail_error"});
                    break;

                //product
                case 'product_open':
                    var productId = eventValues.id;
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ProductEvents" + "|" + productId, eventLabel: "click", eventValues: {url: productId}});
                    break;

                //descopera gama
                case 'gama_open':
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "GamaEvents" + "|" + eventValues.url, eventLabel: "click", eventValues: {url: eventValues.url}});
                    break;

                //descopera articolul
                case 'articol_open':
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ArticolEvents" + "|" + eventValues.url, eventLabel: "click", eventValues: {url: eventValues.url}});
                    break;

                //deschide url
                case 'url_open':
                    trackedObjects.push({type: "event", eventCategory: "", eventAction: "ButtonEvents" + "|" + eventValues.url, eventLabel: "click", eventValues: {url: eventValues.url}});
                    break;
            }

            this.sendTrackedObjects(eventCode, trackedObjects, forceTracking);
        },

        //loading trackers
        loadTrackers: function (options) {
            this.initGoogleAnalyticsTracker(options.vars, this.gaVersion);
        },
        initGoogleAnalyticsTracker: function (data, version) {
            this.initGoogleAnalyticsTracker_analytics(data);
        },

        initGoogleAnalyticsTracker_analytics: function (data) {
            this.trackerLoader(('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/analytics.js');
            var r = 'ga', i = window;
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            ga('create', data['_setAccount'], 'auto');
        },

        trackerLoader: function (url) {
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.onload = function () {
                    MainFrame.Tracker.loadedTrackers++;

                    MainFrame.Tracker.sendTrackedEventNew({
                        eventCode: 'page_changed',
                        extraValues: {
                            page: window.location.pathname,
                        }
                    });
                }
                ga.src = url;

                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        },

        finishedLoading: function () {
            if (this.loadedTrackers == this.trackersCounter) {
                return true
            }
            else return false
        },

        //processing track requests
        sendTrackedEventNew: function (trackOptions) {
            //event
            this.trackEvent(trackOptions);

        },


        sendTrackedObjects: function (eventCode, trackedObjects, forceTracking) {
            if (trackedObjects.length > 0) {
                for (var i in MainFrame.Tracker.trackers) {
                    var trackerObj = MainFrame.Tracker.trackers[i];
                    var eventIsTracked = (trackerObj.codes.indexOf(eventCode) !== -1)
                    if (forceTracking === true) {
                        eventIsTracked = true;
                    }

                    if (eventIsTracked) {
                        for (var j in trackedObjects) {
                            var values = trackedObjects[j].eventValues ? trackedObjects[j].eventValues : {};
                            values.lang = 'ro';
                            MainFrame.Tracker.callTracker({
                                source: 'html5',
                                type: trackerObj.type,
                                trackedEvent: trackedObjects[j].eventString,
                                trackedEventObject: trackedObjects[j],
                                trackerObj: trackerObj,
                                eventCode: eventCode,
                                appId: this.appId,
                                values: values
                            });
                        }
                    }
                }
            }
        },

        //sending values
        callTracker: function (options) {

            this.oras = V7_Store.get();
            if (typeof this.oras === "undefined" || this.oras === null) {
                this.trackedEvents.push(options);
                return;
            }

            if (this.finishedLoading()) {
                //callTracker for queued events
                if (this.trackedEvents.length != 0) {
                    for (var i in this.trackedEvents)
                        this.callTrackerQueue()
                }

                var name = this.appId + "/" + this.oras + "/catalogue";
                var variableName = (String(options.source).indexOf("flash") >= 0) ? String(options.source).split("flash").join(name) : String(options.source).split("html5").join(name);
                if (options.trackerObj) {
                    if (options.trackerObj.vars.catalogVarName) {
                        variableName = options.trackerObj.vars.catalogVarName ? options.trackerObj.vars.catalogVarName : variableName;
                    }
                }

                var trackedEvent = variableName + options.trackedEvent;
                //var trackedEvent = options.source + options.trackedEvent;

                switch (options.type) {
                    case 'GA':
                    // falltrough
                    default:
                        if (this.gaVersion === 'analytics') {
                            if (options.trackedEventObject && options.trackedEventObject.type && options.trackedEventObject.type == "event") {
                                var gaEvent = {hitType: 'event'};
                                gaEvent.eventCategory = variableName;
                                if (typeof options.trackedEventObject.eventCategory != "undefined" && options.trackedEventObject.eventCategory != "") {
                                    gaEvent.eventCategory = options.trackedEventObject.eventCategory
                                }
                                ;
                                if (typeof options.trackedEventObject.eventAction != "undefined") {
                                    gaEvent.eventAction = options.trackedEventObject.eventAction
                                }
                                ;
                                if (typeof options.trackedEventObject.eventLabel != "undefined") {
                                    gaEvent.eventLabel = options.trackedEventObject.eventLabel
                                }
                                ;
                                if (typeof options.trackedEventObject.eventValue != "undefined") {
                                    gaEvent.eventValue = options.trackedEventObject.eventValue
                                }
                                ;

                                ga('send', gaEvent);
                            } else {
                                var gaPageViewValue = variableName + options.trackedEvent;

                                ga('send', 'pageview', gaPageViewValue);
                            }
                        } else {
                            window._gaq.push(['_trackPageview', trackedEvent]);
                        }
                }
                if (this.useBD == true) {
                    this.trackBD(options);
                }
            } else {
                this.trackedEvents.push(options)
            }
        },

        callTrackerQueue: function () {
            var trackedEvent = this.trackedEvents.pop()
            MainFrame.Tracker.callTracker(trackedEvent)
        },

        callTrackerMockup: function (options) {

            this.oras = V7_Store.get();
            if (typeof this.oras === "undefined" || this.oras === null) {
                this.trackedEvents.push(options);
                return;
            }

            var name = this.appId + "/" + this.oras + "/catalogue";
            var variableName = (String(options.source).indexOf("flash") >= 0) ? String(options.source).split("flash").join(name) : String(options.source).split("html5").join(name);
            if (options.trackerObj) {
                if (options.trackerObj.vars.catalogVarName) {
                    variableName = options.trackerObj.vars.catalogVarName ? options.trackerObj.vars.catalogVarName : variableName;
                }
            }

            var trackedEvent = variableName + options.trackedEvent;
            if (options.trackedEventObject && options.trackedEventObject.type && options.trackedEventObject.type == "event") {
                var gaEvent = {hitType: 'event'};
                gaEvent.eventCategory = variableName;
                if (typeof options.trackedEventObject.eventCategory != "undefined" && options.trackedEventObject.eventCategory != "") {
                    gaEvent.eventCategory = options.trackedEventObject.eventCategory
                }
                ;
                if (typeof options.trackedEventObject.eventAction != "undefined") {
                    gaEvent.eventAction = options.trackedEventObject.eventAction
                }
                ;
                if (typeof options.trackedEventObject.eventLabel != "undefined") {
                    gaEvent.eventLabel = options.trackedEventObject.eventLabel
                }
                ;
                if (typeof options.trackedEventObject.eventValue != "undefined") {
                    gaEvent.eventValue = options.trackedEventObject.eventValue
                }
                ;

                console.log("TEST EVENT:" + options.type);
                // console.log(gaEvent);
            } else {
                console.log("TEST PAGEVIEW:" + options.type + ': ' + trackedEvent);
            }
            /*
			if (this.useBD == true) {
				this.trackBD(options);
			}
			*/
        },

        trackBD: function (options) {

            var url = '//a.secure-load.com/favicon.ico';
            options.values.v = this.v
            var values = JSON.stringify(options.values)
            this.request({
                url: url,
                xmlResponse: false,
                method: 'POST',
                formData: true,
                credentials: true,
                formDataParams: {
                    app: this.base64encode('{\
						"appId":"' + options.appId + '",\
						"eventCode":"' + options.eventCode + '",\
						"values":' + values + ',\
						"appType":"' + options.source + '"\
						}')
                },
                onSuccess: function (data) {
                    //console.log(data)
                },
                onError: function (e) {
                    //console.log('Eroare');
                }
            });
        },

        request: function (options) {
            options = options || {};
            var url = options.url,
                method = options.method || 'GET',
                onSuccess = options.onSuccess,
                onError = options.onError,
                ps = options.ps,
                formData = options.formData || null,
                formDataParams = options.formDataParams,
                credentials = options.credentials || false,
                xmlResponse = options.xmlResponse || false,
                jsonResponse = options.jsonResponse || false;
            if (options.onSucces) {
                onSuccess = options.onSucces;
                console.log('Warning: the correct callback name parameter for the request method is "onSuccess"');
            }


            if (typeof ps !== 'undefined') {
                var paramsParts = [];
                for (var paramName in ps) {
                    var paramValue = ps[paramName],
                        paramString = paramName + '=' + paramValue;
                    paramsParts.push(paramString);
                }
                var paramsString = paramsParts.join('&');
                var urlHasParams = (url.indexOf('?') !== -1);
                if (urlHasParams) {
                    url += '&' + paramsString;
                } else {
                    url += '?' + paramsString;
                }
            }
            if (typeof formDataParams !== 'undefined') {
                var formData = new FormData();
                for (var paramName in formDataParams) {
                    formData.append(paramName, formDataParams[paramName]);
                }
            }
            var xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status == 200) {
                        var response = xhr.responseText;
                        if (xmlResponse) {
                            response = xhr.responseXML;
                        } else if (jsonResponse) {
                            response = JSON.parse(response);
                        }
                        if (onSuccess) {
                            onSuccess(response);
                        }
                    } else {
                        if (onError) {
                            onError(xhr);
                        }
                    }
                }
                //else onError(xhr);
            };
            if (credentials) {
                xhr.withCredentials = true;
            }
            if (url) {
                xhr.open(method, url, true);
                xhr.send(formData);
            }
        },

        keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

        base64encode: function (input) {

            var keyStr = this.keyStr;

            input = escape(input);

            var output = '',
                chr1, chr2, chr3,
                enc1, enc2, enc3, enc4;

            var i = 0;
            do {

                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output
                    + keyStr.charAt(enc1)
                    + keyStr.charAt(enc2)
                    + keyStr.charAt(enc3)
                    + keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = '';
                enc1 = enc2 = enc3 = enc4 = '';
            } while (i < input.length);

            return output;
        },

        base64decode: function (input) {

            var keyStr = this.keyStr;

            var output = '',
                chr1, chr2, chr3,
                enc1, enc2, enc3, enc4;

            // removing all characters that are not A-Z, a-z, 0-9, +, /, or =
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

            var i = 0;
            do {

                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }

                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = '';
                enc1 = enc2 = enc3 = enc4 = '';

            } while (i < input.length);

            return unescape(output);
        },
    }

    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if (!Array.prototype.forEach) {

        Array.prototype.forEach = function (callback, thisArg) {

            var T, k;

            if (this == null) {
                throw new TypeError(' this is null or not defined');
            }

            // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
            var O = Object(this);

            // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
            // 3. Let len be ToUint32(lenValue).
            var len = O.length >>> 0;

            // 4. If IsCallable(callback) is false, throw a TypeError exception.
            // See: http://es5.github.com/#x9.11
            if (typeof callback !== "function") {
                throw new TypeError(callback + ' is not a function');
            }

            // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
            if (arguments.length > 1) {
                T = thisArg;
            }

            // 6. Let k be 0
            k = 0;

            // 7. Repeat, while k < len
            while (k < len) {

                var kValue;

                // a. Let Pk be ToString(k).
                //   This is implicit for LHS operands of the in operator
                // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
                //   This step can be combined with c
                // c. If kPresent is true, then
                if (k in O) {

                    // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
                    kValue = O[k];

                    // ii. Call the Call internal method of callback with T as the this value and
                    // argument list containing kValue, k, and O.
                    callback.call(T, kValue, k, O);
                }
                // d. Increase k by 1.
                k++;
            }
            // 8. return undefined
        };
    }

    MainFrame.Tracker.initializeTracker();

})();

// products
(function () {

    var is_preprod = (window.location.href.indexOf('preprod=true') !== -1);
    // @todo: marcaj preprod - comentat la livrare
    // is_preprod = 'true';

    var prices_url = 'https://www.bricodepot.ro/catalog/' + (is_preprod ? 'app_preprod' : 'catalog') + '/assets/prices/price.php?shop=',
        prices = {};

    // @todo: modificare cale locala - comentat la livrare
    if (is_preprod) {
        prices_url = 'assets/prices/price.php?shop=';
    }

    var updateWishlist = function updateWishlist() {

        if (typeof wishlist !== 'undefined') {

            var products = wishlist.getProducts();
            products.forEach(function (product) {

                var price_data = prices[product.id];
                if (price_data) {
                    product.price = price_data.price;
                    product.unit = price_data.unit;
                    product.package_unit = price_data.packageUnit;
                    product.prices_date = price_data.prices_date;
                }

                if (product.price && product.price !== 'N/A') {
                    product.pretty_price = parseFloat(product.price, 10).toFixed(2);
                }
                if (product.packagePrice && product.packagePrice !== 'N/A') {
                    product.pretty_package_price = parseFloat(product.packagePrice, 10).toFixed(2);
                }

                if (product.unit.toLowerCase() !== product.package_unit.toLowerCase()) {
                    product.has_different_prices = true;
                } else {
                    product.has_different_prices = false;
                }
            });

            wishlist.save();
        }
    };

    var parsePrices = function (response) {

        var xml_data = MainFrame.Ext.Parser.parseXmlFlux(response.request.responseXML.documentElement);
        xml_data.children.p.forEach(function (price_node) {

            var price = {};
            price.id = price_node.attributes['id'];
            price.url = price_node.attributes['url'];
            price.unit = price_node.attributes['unit'];
            price.price = price_node.attributes['price'];
            price.packagePrice = price_node.attributes['packagePrice'];
            price.packageUnit = price_node.attributes['packageUnit'];
            price.prices_date = xml_data.children.prices_date.value;

            prices[price.id] = price;
        });

        updateWishlist();
    };

    var getPrices = new Promise(function (resolve, reject) {

        axios.get(prices_url + V7_Store.get())
            .then(function (response) {
                if (response.request.responseXML) {
                    parsePrices(response);
                    resolve();
                } else {
                    reject();
                }
            })
            .catch(function () {
                reject();
            });
    });

    getPrices.then();


    var refreshPrices = function refreshPrices() {

        return new Promise(function (resolve, reject) {

            axios.get(prices_url + V7_Store.get())
                .then(function (response) {
                    if (response.request.responseXML) {
                        parsePrices(response);
                        resolve();
                    } else {
                        reject();
                    }
                })
                .catch(function () {
                    reject();
                });
        });
    };


    var getProductFromResponse = function getProductFromResponse(response) {

        var product = {};


        if (response.request.responseXML) {
            var xml_data = MainFrame.Ext.Parser.parseXmlFlux(response.request.responseXML.documentElement);

            product.id = xml_data.attributes.id;
            product.ref = xml_data.children.ref.value;
            product.url = xml_data.children.url.value;
            product.lib = xml_data.children.lib.value;
            product.desc = xml_data.children.desc.value;
            product.photo = xml_data.children.photo.value;

            product.photos = [];
            if (xml_data.children.photos) {
                xml_data.children.photos.childNodes.forEach(function (photo_node) {
                    product.photos.push(photo_node.value);
                });
            }

            if (product.desc) {
                product.desc = product.desc.replace(/\n/gmi, '<br>');
            }

            if (xml_data.children.pdf) {
                product.pdf = xml_data.children.pdf.value;
                product.has_pdf = true;
            } else {
                product.has_pdf = false;
            }

            var price_data = prices[product.id];
            if (price_data) {
                product.price = price_data.price;
                product.unit = price_data.unit;
                product.prices_date = price_data.prices_date;

                var unit = price_data.unit.toLowerCase();
                var packageUnit = price_data.packageUnit.toLowerCase();
            } else {
                product.price = 'N/A';
                product.unit = '';
                product.prices_date = '';
                var unit = '';
                var packageUnit = '';
            }

            if (product.price && product.price !== 'N/A') {
                product.pretty_price = parseFloat(product.price, 10).toFixed(2);
            } else {
                product.price = 'N/A' //+ parseInt( Math.random() * 888 + 111);
                product.pretty_price = 'N/A' //+ parseInt( Math.random() * 888 + 111);
            }

            product.hasSecondPrice = (packageUnit !== unit) ? true : false;
            if (price_data) {
                product.packageUnit = product.hasSecondPrice == true ? price_data.packageUnit : price_data.unit;
                product.packagePrice = product.hasSecondPrice == true ? price_data.packagePrice : price_data.price;
            } else {
                product.packageUnit = '';
                product.packagePrice = '';
            }
            product.hasNotSecondPrice = (product.hasSecondPrice !== true) ? true : false;

        }
        return product;
    }


    var get_product_url = 'assets/products/?method=getDetailsBySKU&sku=';
    var getProduct = function getProduct(sku) {

        return new Promise(function (resolve, reject) {
            axios.request(get_product_url + sku)
                .then(function (response) {

                    var product = getProductFromResponse(response);

                    resolve(product);
                })
                .catch(function (response) {
                    reject(response);
                });
        });
    };


    var getProducts = function getProducts(skus_string) {

        return new Promise(function (resolve, reject) {

            var skus = skus_string.split(','),
                requests = [];
            skus.forEach(function (sku) {
                requests.push(axios.get(get_product_url + sku));
            });

            axios.all(requests)
                .then(function (responses) {

                    var products = [];
                    responses.forEach(function (response) {
                        products.push(getProductFromResponse(response));
                    });

                    resolve(products);
                })
                .catch(function (responses) {
                    reject(responses)
                });
        });
    };


    window.Products = {
        getProducts: getProducts,
        getProduct: getProduct,
        getPrices: getPrices,
        refreshPrices: refreshPrices
    };

})();


//menu


var main_menu = document.getElementById('main_menu'),
    app_nav = document.getElementsByClassName('app_nav')[0];

window.toggleMainMenu = function openMainMenu() {

    if (main_menu._opened === true) {

        document.body.style.overflow = '';

        app_nav.classList.remove('open');
        main_menu.classList.remove('open');

        main_menu._opened = false;

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'menu_close',
            extraValues: {
                page: window.location.pathname,
            }
        })
    } else {

        document.body.style.overflow = 'hidden';

        app_nav.classList.add('open');
        main_menu.classList.add('open');

        main_menu._opened = true;

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'menu_open',
            extraValues: {
                page: window.location.pathname,
            }
        })
    }

}


//hotspots
var updateHotspots = function updateHotspots() {
    var hotspots = document.getElementsByClassName('yellow_hotspot'),
        index = 0,
        total_hotspots = hotspots.length;
    for (; index < total_hotspots; index++) {

        var hotspot = hotspots[index],
            refs_string = hotspot.getAttribute('data-ref'),
            product_refs = refs_string.split(',');

        (function (product_refs, hotspot) {

            var checkRefsLoaded = function (products) {

                if (refs_loaded === refs_to_load) {

                    var title = hotspot.getElementsByClassName('title')[0],
                        desc = hotspot.getElementsByClassName('desc')[0];
                    var parsed_title = title.innerHTML;
                    parsed_title = parsed_title.replace('#pret', total_price.toFixed(2));
                    parsed_title = parsed_title.replace('#unit', 'buc');

                    if (products.length === 1) {
                        var prod = products[0];
                        if (prod.unit.toLowerCase() !== prod.packageUnit.toLowerCase()) {
                            parsed_title = '<p class="secondary_price">'
                                + parseFloat(prod.packagePrice, 10).toFixed(2)
                                + '<span> Lei/' + prod.packageUnit + '</span></p>'
                                + '<span>' + parsed_title + '</span>';
                        }
                    } else {
                        parsed_title = parsed_title.replace('/buc.', '');
                    }

                    title.innerHTML = parsed_title;

                    var desc_string = desc.innerHTML;
                    desc_string += '<br><span>' + product_refs.join(', ') + '</span>';
                    desc.innerHTML = desc_string;
                }
            }

            var refs_to_load = product_refs.length,
                refs_loaded = 0,
                total_price = 0,
                products = [];
            product_refs.forEach(function (ref) {
                //console.log(refs_to_load, ref)
                Products.getProduct(ref)
                    .then(function (product) {
                        //console.log(product)
                        total_price += parseFloat(product.price, 10);
                        refs_loaded++;
                        products.push(product);
                        checkRefsLoaded(products);
                    })
                    .catch(function () {
                        //console.log('err')
                        refs_loaded++;
                        checkRefsLoaded(products);
                    });
            });

        })(product_refs, hotspot);
    }
};
Products.getPrices.then(function () {
    updateHotspots();
});


//map


var main_map_container = document.getElementById('main_map_container'),
    map_store_bucuresti = document.getElementById('map_store_bucuresti'),
    map_bucuresti_popup = document.getElementById('map_bucuresti_popup'),
    intro_title_container = document.getElementById('intro_title_container');
harta_magazine = document.getElementById('harta_magazine');


var store = V7_Store.get();
if (store != null && V7_Store.isValid(store)) {
    V7_Store.set(store);
    main_map_container.style.display = 'none';
} else {
    main_map_container.style.display = 'block';
}

var map_store_oradea = document.getElementById('map_store_oradea'),
    map_store_arad = document.getElementById('map_store_arad'),
    map_store_brasov = document.getElementById('map_store_brasov'),
    map_store_ploiesti = document.getElementById('map_store_ploiesti'),
    map_store_pitesti = document.getElementById('map_store_pitesti'),
    map_store_militari = document.getElementById('map_store_militari'),
    map_store_orhideea = document.getElementById('map_store_orhideea'),
    map_store_pantelimon = document.getElementById('map_store_pantelimon');
map_store_baneasa = document.getElementById('map_store_baneasa');

map_store_constanta = document.getElementById('map_store_constanta');
map_store_braila = document.getElementById('map_store_braila');
map_store_focsani = document.getElementById('map_store_focsani');
map_store_suceava = document.getElementById('map_store_suceava');
map_store_drobeta = document.getElementById('map_store_drobeta');
map_store_calarasi = document.getElementById('map_store_calarasi');

var loadStore = function (shop) {

    if (V7_Store.isValid(shop)) {
        V7_Store.set(shop)
    }
    Products.refreshPrices().then(function () {
        updateHotspots();
    });
    //window.location.reload();
    window.location.search = "";
    main_map_container.style.display = 'none';
}

map_store_oradea.onclick = function () {
    loadStore('oradea');
};

map_store_arad.onclick = function () {
    loadStore('arad');
};

map_store_brasov.onclick = function () {
    loadStore('brasov');
};

map_store_ploiesti.onclick = function () {
    loadStore('ploiesti');
};

map_store_pitesti.onclick = function () {
    loadStore('pitesti');
};

map_store_militari.onclick = function () {
    loadStore('militari');
};

map_store_orhideea.onclick = function () {
    loadStore('orhideea');
};

map_store_pantelimon.onclick = function () {
    loadStore('pantelimon');
};

map_store_baneasa.onclick = function () {
    loadStore('baneasa');
};

map_store_constanta.onclick = function () {
    loadStore('constanta');
};
map_store_braila.onclick = function () {
    loadStore('braila');
};
map_store_focsani.onclick = function () {
    loadStore('focsani');
};
map_store_suceava.onclick = function () {
    loadStore('suceava');
};
map_store_drobeta.onclick = function () {
    loadStore('drobeta');
};
map_store_calarasi.onclick = function () {
    loadStore('calarasi');
};

map_store_bucuresti.onclick = function (e) {

    var mapWidth = harta_magazine.clientWidth,
        mapHeight = harta_magazine.clientHeight;

    var x = (58 * mapWidth) / 100,
        y = (70 * mapHeight) / 100,
        hotspotWidth = (7.5 * mapWidth) / 100;

    x -= (115 - hotspotWidth / 2);
    y -= 115;

    map_bucuresti_popup.style.left = Math.round(x) + 'px';
    map_bucuresti_popup.style.top = Math.round(y) + 'px';
    map_bucuresti_popup.style.visibility = 'visible';
}

window.showMap = function showMap(e) {
    main_map_container.style.display = 'block';
};

var city = document.getElementById('city');
city.onclick = function (e) {
    showMap();

    e.stopPropagation();
    e.preventDefault();
}

function resizeMap() {

    var mapRatio = 933 / 680;

    var availableWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
        availableHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

    var headerHeight = 60,
        titleHeight = 80;

    var remainingWidth = availableWidth,
        remainingHeight = availableHeight - (headerHeight + titleHeight);


    var containerRatio = remainingWidth / remainingHeight;

    if (containerRatio >= mapRatio) {

        harta_magazine.style.height = remainingHeight + 'px';
        var width = remainingHeight * mapRatio;
        harta_magazine.style.width = parseInt(width, 10) + 'px';

    } else {

        harta_magazine.style.width = remainingWidth + 'px';
        var height = remainingWidth / mapRatio;
        harta_magazine.style.height = parseInt(height, 10) + 'px';
    }
}

resizeMap();

window.onresize = function () {
    resizeMap();
}


//app
var wishlist = new MainFrame.Ext.Wishlist({id: 'bricodepot.dev', id_property: 'id'});

var product;

var embedProductOverlay;

function embedProduct(product_id) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<div id="product_details" class="ext-module-js" data-module="template" data-options-id="product_template_options"></div>'

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    //var iframe = embedProductOverlay.getElementByClassName('');
    //iframe.className = 'v7__embed__center v7__embed__overlay-video-container';
    document.body.appendChild(embedProductOverlay);

    var product_template_el,
        product_template;

    Products.getProduct(product_id).then(function (flux_product) {

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'product_open',
            extraValues: {
                id: product_id,
            }
        });

        product = flux_product;

        product_template_el = document.getElementById('product_details');
        product_template = new MainFrame.Ext.Template({
            target: {
                element: product_template_el,
                options: {
                    template_id: 'product_template'
                }
            },
            data: {
                object: product
            }
        });
        product_template_el.style.visibility = 'visible';

        product_template_el.onclick = function (e) {
            if (e.target && e.target.nodeName.toLowerCase() == 'a') {

            } else {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    });

    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


function embedProducts(product_ids) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<div id="product_details" class="ext-module-js" data-module="template" data-options-id="product_template_options"></div>'

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    //var iframe = embedProductOverlay.getElementByClassName('');
    //iframe.className = 'v7__embed__center v7__embed__overlay-video-container';
    document.body.appendChild(embedProductOverlay);

    var product_template_el,
        product_template;

    Products.getProducts(product_ids).then(function (flux_products) {

        product = flux_products[0];

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'product_open',
            extraValues: {
                id: product.id,
            }
        });

        product.other_products = flux_products;
        if (product.other_products.length > 3) {
            product.should_scroll_other_products = true;
        } else {
            product.should_scroll_other_products = false;
        }

        product_template_el = document.getElementById('product_details');
        product_template = new MainFrame.Ext.Template({
            target: {
                element: product_template_el,
                options: {
                    template_id: 'product_multi_template'
                }
            },
            data: {
                object: product
            }
        });
        product_template_el.style.visibility = 'visible';

        product_template_el.onclick = function (e) {
            if (e.target && e.target.nodeName.toLowerCase() == 'a') {

            } else {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    });

    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


function updateProduct(product_id) {



    var other_products = product.other_products;

    var new_product;
    other_products.forEach(function (other_product) {
        if (other_product.id == product_id) {
            new_product = other_product;
        }
    });

    var product_template_el = document.getElementById('product_details');

    var lib = product_template_el.getElementsByClassName('lib')[0];
    lib.innerHTML = new_product.lib;

    var ref = product_template_el.getElementsByClassName('ref')[0];
    ref.innerHTML = new_product.ref;

    var photo = product_template_el.getElementsByClassName('product_photo')[0];
    photo.src = new_product.photo;

    var prices_date = product_template_el.getElementsByClassName('prices_date')[0];
    prices_date.innerHTML = new_product.prices_date;

    var pdf = product_template_el.getElementsByClassName('pdf')[0];
    pdf.href = new_product.pdf;
    if (new_product.has_pdf) {
        pdf.classList.add('visible');
    } else {
        pdf.classList.remove('visible');
    }

    var desc = product_template_el.getElementsByClassName('desc_content')[0];
    desc.innerHTML = new_product.desc;

    var price = product_template_el.getElementsByClassName('price')[0];
    price.innerHTML = new_product.price + ' Lei / ' + new_product.unit;

    var second_price = product_template_el.getElementsByClassName('second_price')[0];
    second_price.innerHTML = new_product.packagePrice + ' Lei / ' + new_product.packageUnit;

    var add_button = product_template_el.getElementsByClassName('add_product')[0];
    add_button.onclick = function () {
        addProduct(new_product.id + '');
    };

    var visit_button = product_template_el.getElementsByClassName('visit_product')[0];
    visit_button.href = new_product.url;

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'product_open',
        extraValues: {
            id: new_product.id,
        }
    });

    product = new_product;

    product.other_products = other_products;
};


function scrollMultiRight() {

    var multi_product_wrapper = document.getElementsByClassName('multi_product_wrapper')[0],
        wrapper_width = multi_product_wrapper.clientWidth,
        scroll_distance = Math.floor(wrapper_width / 3),
        max_distance = (product.other_products.length - 3) * scroll_distance;

    if (multi_product_wrapper.scrollLeft + scroll_distance <= max_distance) {
        multi_product_wrapper.scrollLeft += scroll_distance;
    }

    var scroll_left_button = document.getElementsByClassName('scroll_left')[0],
        scroll_right_button = document.getElementsByClassName('scroll_right')[0];

    if (multi_product_wrapper.scrollLeft >= max_distance) {
        scroll_right_button.style.opacity = '0.5';
    }
    scroll_left_button.style.opacity = '1';
};

function scrollMultiLeft() {

    var multi_product_wrapper = document.getElementsByClassName('multi_product_wrapper')[0],
        wrapper_width = multi_product_wrapper.clientWidth,
        scroll_distance = Math.floor(wrapper_width / 3);

    if (multi_product_wrapper.scrollLeft - scroll_distance >= 0) {
        multi_product_wrapper.scrollLeft -= scroll_distance;
    }

    var scroll_left_button = document.getElementsByClassName('scroll_left')[0],
        scroll_right_button = document.getElementsByClassName('scroll_right')[0];

    if (multi_product_wrapper.scrollLeft == 0) {
        scroll_left_button.style.opacity = '0.5';
    }
    scroll_right_button.style.opacity = '1';
};


function embedWishlist() {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_open',
        extraValues: {
            page: window.location.pathname,
        }
    })

    document.body.style.overflow = 'hidden';

    var embed_wishlist_template = document.getElementById('embed_wishlist_template').innerHTML.trim();

    var embedCode = embed_wishlist_template;

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    document.body.appendChild(embedProductOverlay);

    if (wishlist.getProducts().length) {

        var wishlist_products = wishlist.getProducts();
        wishlist_template_el = document.getElementById('wishlist'),
            wishlist_template = new MainFrame.Ext.Template({
                target: {
                    element: wishlist_template_el,
                    options: {
                        template_id: 'wishlist_template',
                        items: 'all',
                    }
                },
                data: {
                    object: wishlist_products
                }
            });

        var wishlist_total_el = document.getElementById('wishlist_total'),
            wishlist_total = new MainFrame.Ext.WishlistBadge({
                target: {
                    element: wishlist_total_el
                },
                details: {
                    text: 'Total: #price lei',
                    wishlist_id: 'wishlist',
                    static: true
                }
            });

        wishlist_template_el.style.visibility = 'visible';
        wishlist_total_el.style.visibility = 'visible';
    }

    var wishlist_container = document.getElementById('wishlist_container');
    wishlist_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


var embedPopupOverlay;

function embedPopup(message) {

    var embed_popup_template = document.getElementById('embed_popup_template').innerHTML.trim();

    var embedCode = embed_popup_template;

    embedPopupOverlay = document.createElement('div');
    embedPopupOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedPopupOverlay.innerHTML = embedCode;

    document.body.appendChild(embedPopupOverlay);

    var popup_text = document.getElementById('popup_text');
    popup_text.innerHTML = message;


    var popup_container = document.getElementById('popup_container');
    popup_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    embedPopupOverlay.onclick = function (e) {
        closeEmbedPopup();
    }
};

function closeEmbedPopup() {
    document.body.removeChild(embedPopupOverlay);
};


function shareFacebook() {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'share_facebook',
        extraValues: {
            page: window.location.pathname,
        }
    })
    window.open("http://www.facebook.com/sharer.php?u=https://www.bricodepot.ro");
}

function openUrl(url, type) {
    var trackingType = "url_open";
    if (typeof type != undefined) {
        trackingType = type;
    }
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: trackingType,
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

function openGama(url) {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "gama_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

function openGamaStore(url) {
    //
    var store = window.V7_Store.get();
    url = url.replace('#{V7_store}', store);
    //
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "gama_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}


function openArticol(url) {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "articol_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

window.updateQuantity = function updateQuantity(input_id, increment) {

    var quantity_input = document.getElementById(input_id),
        quantity = parseInt(quantity_input.value, 10);

    quantity += increment;

    if (quantity > 1) {
        quantity_input.value = quantity;
    }
};

window.updateWishlistQuantity = function updateWishlistQuantity(product_id, input_id, increment) {

    var quantity_input = document.getElementById(input_id),
        quantity = parseInt(quantity_input.value, 10);

    quantity += increment;

    if (quantity >= 1) {
        quantity_input.value = quantity;
        wishlist.incrementProductQuantity(product_id, increment);
    }
};

window.removeProduct = function removeProduct(product_id) {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_remove',
        extraValues: {
            id: product_id,
        }
    })

    wishlist.removeProduct(product_id);

    var wishlist_product_el = document.getElementById('wishlist_product_' + product_id);
    wishlist_product_el.parentNode.removeChild(wishlist_product_el);
};

window.addProduct = function addProduct(product_id) {

    var quantity_input = document.getElementById('quantity'),
        quantity = parseInt(quantity_input.value, 10);

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_add',
        extraValues: {
            id: product_id,
        }
    })

    if (product) {

        // daca ramane proprietatea other_products la adaugare
        // nu va functiona clonarea (nu se poate converti o structura circulara in json)
        // deoarece other_products contine si referinta catre product
        if (product.other_products) {
            var other_products = product.other_products;
            delete product.other_products;
        }

        wishlist.addProduct(product, quantity);
        embedPopup('Articolul dumneavoastră a fost adăugat cu succes în lista de cumpărături!');

        // la final punem la loc
        if (typeof other_products !== 'undefined') {
            product.other_products = other_products;
        }
    }
};

window.clearWishlist = function clearWishlist() {

    wishlist.empty();

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_empty',
    })

    var wishlist_template_el = document.getElementById('wishlist');
    wishlist_template_el.innerHTML = '';
};

window.printWishlist = function printWishlist() {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_print',
    });

    var actionPrint = 'https://www.interactivpdf.com/2018/WEBPROJECTS/W0017.Bricodepot/externals/ldc/index_print.php';

    var list = wishlist.getProducts();

    var basket = '<products>';
    list.forEach(function (product) {

        basket += '<product>\
					<id><![CDATA[' + product.id + ']]></id>\
					<qty><![CDATA[' + product._quantity + ']]></qty>\
					<obj>\
						<price><![CDATA[' + product.price + ']]></price>\
						<lib><![CDATA[' + product.lib + ']]></lib>\
						<desc><![CDATA[' + product.desc + ']]></desc>';
        if (product.photo && product.photo.indexOf("http") == 0) {
            basket += '<photo>' + product.photo + '</photo>';
        }
        basket += '	</obj>\
				</product>'
    });

    basket += '</products>';
    var xml = '<?xml version="1.0" encoding="utf-8"?>';
    xml += '<doc>';
    xml += '<mailUrl><![CDATA[' + actionPrint.split('externals/')[0] + ']]></mailUrl>';
    xml += basket;
    xml += '</doc>';
    var fd = new FormData();
    fd.append('xmloutput', xml);
    fd.append('lang', 'RO');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', actionPrint, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var listWindow = window.open("", "");
            var windowContent = xhr.responseText;
            var scriptText = 'var imgs=document.getElementsByTagName("img");';
            scriptText += 'var index=0,totalImages=imgs.length,loadedImages=0;';
            scriptText += 'var imgCallback=function(){loadedImages++;if(loadedImages===totalImages){';
            scriptText += 'window.print()';
            scriptText += '}};for(;index<totalImages;index++){';
            scriptText += 'var imgEl=imgs[index],imgSrc=imgEl.getAttribute("src");';
            scriptText += 'var img=new Image;img.onload=imgCallback;img.onerror=imgCallback;img.src=imgSrc}';
            windowContent = windowContent.replace('</body>', '<script>' + scriptText + '</script></body>');
            listWindow.document.write(windowContent);
        }
    };
    xhr.send(fd);

};

var sendWishlistOverlay;
window.openSendWishlist = function openSendWishlist() {

    closeEmbedProduct();

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_email',
    });

    var send_wishlist_template = document.getElementById('send_wishlist_template').innerHTML.trim();

    var embedCode = send_wishlist_template;

    sendWishlistOverlay = document.createElement('div');
    sendWishlistOverlay.className = 'v7__embed__center v7__embed__overlay';
    sendWishlistOverlay.innerHTML = embedCode;

    document.body.appendChild(sendWishlistOverlay);

    if (typeof grecaptcha !== 'undefined') {
        grecaptcha.render('send_wishlist_recaptcha', {
            'theme': 'light',
            'size': 'normal',
            'hl': 'ro'
        });
    }

    var send_wishlist_container = document.getElementById('send_wishlist_container');
    send_wishlist_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    sendWishlistOverlay.onclick = function (e) {
        closeSendWishlist();
    }

};


window.sendWishlist = function sendWishlist() {

    var actionPrint = 'https://www.interactivpdf.com/2018/WEBPROJECTS/W0017.Bricodepot/externals/ldc/index.php';

    var list = wishlist.getProducts();

    var firstName = document.getElementById('ldc_firstName').value,
        lastName = document.getElementById('ldc_lastName').value,
        to = document.getElementById('ldc_to').value,
        message = document.getElementById('ldc_message').value,
        recaptcha_response = '';

    var formContainer = document.getElementById('send_wishlist_container'),
        textareas = formContainer.getElementsByTagName('textarea'),
        index = 0,
        total_textareas = textareas.length;

    for (; index < total_textareas; index++) {

        var textarea = textareas[index];
        if (textarea.name === 'g-recaptcha-response') {
            recaptcha_response = textarea.value;
        }
    }

    var basket = '<products>';
    list.forEach(function (product) {

        basket += '<product>\
					<id><![CDATA[' + product.id + ']]></id>\
					<qty><![CDATA[' + product._quantity + ']]></qty>\
					<obj>\
						<price><![CDATA[' + product.price + ']]></price>\
						<lib><![CDATA[' + product.lib + ']]></lib>\
						<desc><![CDATA[' + product.desc + ']]></desc>';
        if (product.photo && product.photo.indexOf("http") == 0) {
            basket += '<photo>' + product.photo + '</photo>';
        }
        basket += '	</obj>\
				</product>'
    });

    basket += '</products>';

    var xml = '<?xml version="1.0" encoding="utf-8"?>';
    xml += '<doc>';

    //<urlrequest><![CDATA[https://www.bricodepot.ro/catalog/]]></urlrequest>
    //<finurl><![CDATA[https://www.bricodepot.ro/catalog/index.html]]></finurl>

    xml += '<urlrequest><![CDATA[]]></urlrequest>';
    xml += '<finurl><![CDATA[]]></finurl>';
    xml += '<lang><![CDATA[ro]]></lang>';
    xml += '<firstName><![CDATA[' + firstName + ']]></firstName>';
    xml += '<lastName><![CDATA[' + lastName + ']]></lastName>';
    xml += '<to><![CDATA[' + to + ']]></to>';
    xml += '<message><![CDATA[' + message + ']]></message>';

    xml += '<mailUrl><![CDATA[' + actionPrint.split('externals/')[0] + ']]></mailUrl>';
    xml += basket;
    xml += '</doc>';

    var fd = new FormData();
    fd.append('xmloutput', xml);
    fd.append('lang', 'RO');
    fd.append('g-recaptcha-response', recaptcha_response);
    fd.append('firstName', firstName);
    fd.append('lastName', lastName);
    fd.append('to', 'to');
    fd.append('message', message);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', actionPrint, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if (xhr.responseText === 'sendResult=true') {
                embedPopup('Mail-ul dumneavoastră a fost trimis.');
            } else {
                embedPopup('Din păcate, mail-ul dumneavoastră nu a fost trimis.<br />Vă rugăm să încercaţi mai târziu.');
            }
        } else {
            embedPopup('Din păcate, mail-ul dumneavoastră nu a fost trimis.<br />Vă rugăm să încercaţi mai târziu.');
        }
    };
    xhr.send(fd);
}


function closeSendWishlist() {
    document.body.removeChild(sendWishlistOverlay);
    document.body.style.overflow = '';
}


function closeEmbedProduct() {
    document.body.removeChild(embedProductOverlay);
    document.body.style.overflow = '';
}

MainFrame.Ext.Map.prototype.bindZoneAction = function (zone_element, zone_data) {

    var self = this;

    if (zone_data.display === 'hs') {
        var target = zone_element.getElementsByClassName(self.css_classes['hotspot'])[0];
    } else {
        var target = zone_element;
    }

    var refs = self.getRefs(zone_data),
        ref = refs[0];

    target.onclick = function () {
        var product_id = ref.url;
        embedProduct(product_id)
    }
}

var embedZoomOverlay;

function embedZoom(image) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<img class="zoomed_image" src="' + image + '">';

    embedZoomOverlay = document.createElement('div');
    embedZoomOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedZoomOverlay.innerHTML = embedCode;

    document.body.appendChild(embedZoomOverlay);

    embedZoomOverlay.onclick = function (e) {
        closeEmbedZoom();
    }
};

function closeEmbedZoom() {
    document.body.style.overflow = '';
    document.body.removeChild(embedZoomOverlay);
};

function nop() {
}

$shown_after_scroll = $('.shown_after_scroll');
$(window).scroll(function () {
    if ($(this).scrollTop() >= 85) {        // If page is scrolled more than 85px (header menu)
        if (!$shown_after_scroll._faded) {
            $shown_after_scroll.fadeIn(200);
            $shown_after_scroll._faded = true;
        }
    } else {
        $shown_after_scroll.fadeOut(200);
        $shown_after_scroll._faded = false;
    }
});