// autoshrink
var AutoShrink = {

    getSiblingsHeight: function (element) {
        var siblings = $(element).siblings();
        var total_height = 0;

        $.each(siblings, function (i, v) {
            total_height += $(v).outerHeight(true);
        });

        return total_height;
    },

    getElementHeight: function (element) {
        return $(element).outerHeight(true);
    },

    getParentHeight: function (element) {
        return $(element).parent().height();
    },

    setParentHeight: function (element) {
        $(element).parent().css('height', '100%');
    },

    saveInitialFontSizeAndLineHeight: function (element) {
        var fs = $(element).css('font-size');
        var lh = $(element).css('line-height');
        var attr_fs = $(element).attr('data-initialFontSize');
        if (typeof attr_fs === typeof undefined || attr_fs === false) {
            $(element).attr('data-initialFontSize', fs);
            $(element).attr('data-initialLineHeight', lh);
        }

        $.each($(element).find('*'), function (i, v) {
            var fsc = $(v).css('font-size');
            var lhc = $(v).css('line-height');
            var attr_fsc = $(v).attr('data-initialFontSize');
            if (typeof attr_fsc === typeof undefined || attr_fsc === false) {
                $(v).attr('data-initialFontSize', fsc);
                $(v).attr('data-initialLineHeight', lhc);
            }
        });
    },

    setInitialFontSizeAndLineHeight: function (element) {
        var fs = $(element).attr('data-initialFontSize');
        var lh = $(element).attr('data-initialLineHeight');
        $(element).css('font-size', fs);
        $(element).css('line-height', lh);

        $.each($(element).find('*'), function (i, v) {
            var fsc = $(v).attr('data-initialFontSize');
            var lhc = $(v).attr('data-initialLineHeight');
            $(v).css('font-size', fsc);
            $(v).css('line-height', lhc);
        });
    },

    start: function () {

        var self = this;

        //selecteaza toate elementele cu acest atribut
        var all_autoshrinks = $('*[autoshrink]');

        // pentru fiecare autoshrink
        $.each(all_autoshrinks, function (index, element) {

            // salveaza fonturile initiale incepand cu elementul curent
            self.saveInitialFontSizeAndLineHeight(element);

            // seteaza fonturile initiale incepand cu elementul curent
            self.setInitialFontSizeAndLineHeight(element);

            // preia parintele
            self.setParentHeight();
            var parent_height = self.getParentHeight(element);

            //preia fratii
            var total_height = self.getSiblingsHeight(element) + self.getElementHeight(element);

            var x = 0;
            // cat timp continutul e mai mare decat parintele
            while ((parent_height < total_height) && (x < 67)) {
                x++;
                var font_size;
                // scade fontsize si lineheight pe element
                font_size = parseInt($(element).css('font-size').replace('px', ''), 10).toFixed(2);
                if (font_size > 9) {

                    font_size -= 0.3;
                    font_size = font_size.toFixed(2);
                    line_height = font_size;
                    line_height += font_size / 2;
                    line_height = parseFloat(line_height).toFixed(2);

                    $(element).css('font-size', (font_size + 'px'));
                    $(element).css('line-height', (line_height + 'px'));
                }

                // scade fontsize si lineheight pe ce e in interiorul elementului
                $.each($(element).find('*'), function (i, v) {
                    font_size = parseFloat($(v).css('font-size').replace('px', ''), 10).toFixed(2);
                    if (font_size > 9) {

                        font_size -= 0.3;
                        font_size = font_size.toFixed(2);
                        line_height = font_size;
                        line_height += font_size / 2;
                        line_height = parseFloat(line_height).toFixed(2);

                        $(v).css('font-size', (font_size + 'px'));
                        $(v).css('line-height', (line_height + 'px'));
                    }
                });

                // preia noua inaltime
                total_height = self.getSiblingsHeight(element) + self.getElementHeight(element);

            }

        });
    },
};

function setTimeOutForAutoshrink() {
    setTimeout(function () {
        AutoShrink.start();
    }, 100);
}

window.addEventListener('load', function () {
    setTimeOutForAutoshrink();
});
window.addEventListener('resize', function () {
    setTimeOutForAutoshrink();
});