

var main_map_container = document.getElementById('main_map_container'),
	map_store_bucuresti = document.getElementById('map_store_bucuresti'),
	map_bucuresti_popup = document.getElementById('map_bucuresti_popup'),
	intro_title_container = document.getElementById('intro_title_container');
	harta_magazine = document.getElementById('harta_magazine');


var store = V7_Store.get();
if (store != null && V7_Store.isValid(store)) {
	V7_Store.set(store);
	main_map_container.style.display = 'none'; 
} else {
	main_map_container.style.display = 'block';
}

var map_store_oradea     = document.getElementById('map_store_oradea'),
	map_store_arad       = document.getElementById('map_store_arad'),
	map_store_brasov     = document.getElementById('map_store_brasov'),
	map_store_ploiesti   = document.getElementById('map_store_ploiesti'),
	map_store_pitesti    = document.getElementById('map_store_pitesti'),
	map_store_militari   = document.getElementById('map_store_militari'),
	map_store_orhideea   = document.getElementById('map_store_orhideea'),
	map_store_pantelimon = document.getElementById('map_store_pantelimon');
	map_store_baneasa = document.getElementById('map_store_baneasa');
	
	map_store_constanta = document.getElementById('map_store_constanta');
	map_store_braila = document.getElementById('map_store_braila');
	map_store_focsani = document.getElementById('map_store_focsani');
	map_store_suceava = document.getElementById('map_store_suceava');
	map_store_drobeta = document.getElementById('map_store_drobeta');
	map_store_calarasi = document.getElementById('map_store_calarasi');

var loadStore = function (shop) {

	if(V7_Store.isValid(shop)) {
		V7_Store.set(shop)
	}
	Products.refreshPrices().then(function () {
		updateHotspots();
	});
	window.location.reload();
	main_map_container.style.display = 'none'; 
}

map_store_oradea.onclick = function () {
	loadStore('oradea');
};

map_store_arad.onclick = function () {
	loadStore('arad');
};

map_store_brasov.onclick = function () {
	loadStore('brasov');
};

map_store_ploiesti.onclick = function () {
	loadStore('ploiesti');
};

map_store_pitesti.onclick = function () {
	loadStore('pitesti');
};

map_store_militari.onclick = function () {
	loadStore('militari');
};

map_store_orhideea.onclick = function () {
	loadStore('orhideea');
};

map_store_pantelimon.onclick = function () {
	loadStore('pantelimon');
};

map_store_baneasa.onclick = function () {
	loadStore('baneasa');
};

map_store_constanta.onclick = function () {
	loadStore('constanta');
};
map_store_braila.onclick = function () {
	loadStore('braila');
};
map_store_focsani.onclick = function () {
	loadStore('focsani');
};
map_store_suceava.onclick = function () {
	loadStore('suceava');
};
map_store_drobeta.onclick = function () {
	loadStore('drobeta');
};
map_store_calarasi.onclick = function () {
	loadStore('calarasi');
};

map_store_bucuresti.onclick = function (e) {

	var mapWidth  = harta_magazine.clientWidth,
		mapHeight = harta_magazine.clientHeight;

	var x = (58 * mapWidth) / 100,
		y = (70 * mapHeight) / 100,
		hotspotWidth  = (7.5 * mapWidth) / 100;

	x -= (115 - hotspotWidth / 2);
	y -= 115;

	map_bucuresti_popup.style.left = Math.round(x) + 'px';
	map_bucuresti_popup.style.top  = Math.round(y) + 'px';
	map_bucuresti_popup.style.visibility = 'visible';
}

window.showMap = function showMap (e) {
	main_map_container.style.display = 'block';
};

var city = document.getElementById('city');
city.onclick = function (e) {
	showMap();

	e.stopPropagation();
	e.preventDefault();
}

function resizeMap () {

	var mapRatio = 933 / 680;

	var availableWidth  = window.innerWidth  || document.documentElement.clientWidth  || document.body.clientWidth,
		availableHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

	var headerHeight = 60,
		titleHeight  = 80;

	var remainingWidth = availableWidth,
		remainingHeight = availableHeight - (headerHeight + titleHeight);

	
	var containerRatio = remainingWidth / remainingHeight;

	if (containerRatio >= mapRatio) {

		harta_magazine.style.height = remainingHeight + 'px';
		var width = remainingHeight * mapRatio;
		harta_magazine.style.width  = parseInt(width, 10) + 'px';
		
	} else {

		harta_magazine.style.width = remainingWidth + 'px';
		var height = remainingWidth / mapRatio;
		harta_magazine.style.height = parseInt(height, 10) + 'px';
	}
}

resizeMap();

window.onresize = function () {
	resizeMap();
}