

var updateHotspots = function updateHotspots() {
	var hotspots = document.getElementsByClassName('yellow_hotspot'),
		index = 0,
		total_hotspots = hotspots.length;
	for ( ; index < total_hotspots; index++) {

		var hotspot = hotspots[index],
			refs_string = hotspot.getAttribute('data-ref'),
			product_refs = refs_string.split(',');

		(function (product_refs, hotspot) {

			var checkRefsLoaded = function () {
				if (refs_loaded === refs_to_load) {

					var title = hotspot.getElementsByClassName('title')[0],
						desc  = hotspot.getElementsByClassName('desc')[0];
					var parsed_title = title.innerHTML;
					parsed_title = parsed_title.replace('#pret', total_price);
					parsed_title = parsed_title.replace('#unit', 'buc');

					title.innerHTML = parsed_title;

					var desc_string = desc.innerHTML;
					desc_string += '<br><span>' + product_refs.join(', ') + '</span>';
					desc.innerHTML = desc_string;
				}
			}

			var refs_to_load = product_refs.length,
				refs_loaded  = 0,
				total_price  = 0,
				products = [];
			product_refs.forEach(function (ref) {
				//console.log(refs_to_load, ref)
				Products.getProduct(ref)
					.then(function (product) {
						//console.log(product)
						total_price += parseFloat(product.price, 10);
						refs_loaded++;
						products.push(product);
						checkRefsLoaded();
					})
					.catch(function () {
						//console.log('err')
						refs_loaded++;
						checkRefsLoaded();
					});
			});

		})(product_refs, hotspot);
	}
};
Products.getPrices.then(function () {
	updateHotspots();
});