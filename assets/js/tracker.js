
(function () {
	
	MainFrame = window.MainFrame || {};
	
	MainFrame.Tracker = {

		appId: 'W0027',

		// 'tagbleu.net', 'easy-publication.com'
		disallowedDomains: ['dGFnYmxldS5uZXQ=', 'ZWFzeS1wdWJsaWNhdGlvbi5jb20=', 'MTkyLjE2OA==', 'cHJlc3RpbWVkaWE='],

		// the main.xml id of the catalog
		mainModuleId: 'catalog',

		trackingType: "events", //events - new tracking | pageviews - for the old tracking | both - for sending pageviews and events
		useBD: true, //big data
		config: {},
		trackers: [],
		trackedEvents: [],
		loadedTrackers: 0,
		trackersCounter: 0,
		gaVersion: 'analytics',
		hasTrackedOpening: false,
		v : '1.0.0',
		oras : '',

		initializeTracker: function () {

			var testingServer = false;

			var currentDomain = window.location.hostname;

			this.disallowedDomains.forEach(function (domain) {

				var decodedDomain = MainFrame.Tracker.base64decode(domain);
				if (currentDomain.indexOf(decodedDomain) !== -1) {
					testingServer = true;
				}
			});

			var currentUrl = window.location.href;
			if (currentUrl.indexOf('trackingTest=true') !== -1) {
				testingServer = true;
			}
			
			if (testingServer) {
				MainFrame.Tracker.callTracker = MainFrame.Tracker.callTrackerMockup;
			}
			
			MainFrame.Tracker.init({
				appId: 'W0027',
				type: 'GA',
				vars: {'_setAccount': 'UA-5850067-16'}
				//vars: {'_setAccount': 'UA-xxxxxxx-xx'}
			});
		},

		//init tracker options
		init: function (options) {
			this.setDefaultCodes(options);
			this.gaVersion = options.version ? options.version : this.gaVersion;
			this.trackers.push(options);
			this.trackersCounter++;
			this.loadTrackers(options);
			
			this.appId = options.appId;
			this.oras = V7_Store.get();
		},

		//setting default available codes
		setDefaultCodes: function(options) {
			var trackedEventCodes = options.codes;
			
			var eventCodes = [
				'page_changed',//page changed
				
				//menu
				'menu_open',//menu clicked
				'menu_close',//menu clicked
				'share_facebook',//share clicked
				
				//products
				'product_open',//
				
				//wishlist
				'wishlist_open',//wishlist clicked
				'wishlist_close', //
				'wishlist_add', //
				'wishlist_remove', //
				'wishlist_empty', //
				'wishlist_print', //
				'wishlist_email', //
				'wishlist_email_error', //
				
				//descopera gama
				'gama_open',
				
				//citeste articol
				'articol_open',
				
				//deschidere url 
				'url_open',
				
			];
			
			options.codes = eventCodes;
		},
		
		//sending events and pageviews
		trackEvent: function(trackOptions) {
			
			var eventCode = trackOptions.eventCode;
			var actionTrigger = trackOptions.actionTrigger;
			var actionInitiator = trackOptions.actionInitiator;
			var actionTarget = trackOptions.actionTarget;
			var eventValues = trackOptions.extraValues;
			var forceTracking = false;

			var i, j;
			var trackedEvent;
			var trackedObjects = [];
			
			switch (eventCode) {

				//page changed
				case 'page_changed':
					if (this.hasTrackedOpening == false) {
						trackedObjects.push({type:"page_view", eventString:'/PageEvents/view/' + eventValues.page, eventValues:{page:eventValues.page}});
						this.hasTrackedOpening = true;
					} else {
						trackedObjects.push({type:"page_view", eventString:'/PageEvents/view/' + eventValues.page, eventValues:{page:eventValues.page}});
					}
					break;
				//////////////////////////
				
				//menu
				case 'menu_open':
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ButtonsEvents"+"|"+eventValues.page, eventLabel:"menu_open", eventValues:{page:eventValues.page}});
					break;
				case 'menu_close':
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ButtonsEvents"+"|"+eventValues.page, eventLabel:"menu_close", eventValues:{page:eventValues.page}});
					break;
				case 'share_facebook': //share facebook
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"CatalogueEvents"+"|"+eventValues.page, eventLabel:"share|facebook"});
					break;
					
				//wishlist
				case 'wishlist_open': //
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ButtonsEvents"+"|"+eventValues.page, eventLabel:"wishlist_open"});
					break;
				case 'wishlist_close': //
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ButtonsEvents", eventLabel:"wishlist_close"});
					break;
				case 'wishlist_add': //
					var productId = eventValues.id;
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ProductEvents"+"|"+productId, eventLabel:"addToWishlist", eventValues:{url:productId}});
					break;
				case 'wishlist_remove': //
					var productId = eventValues.id;
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ProductEvents"+"|"+productId, eventLabel:"removeFromWishlist", eventValues:{url:productId}});
					break;
				case 'wishlist_empty': //
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"WishlistEvents", eventLabel:"empty"});
					break;
				case 'wishlist_print': //
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"WishlistEvents", eventLabel:"print"});
					break;
				case 'wishlist_email': //
					var trackValue = "";
					var wishlistManager = wishlist;
					for (var key in wishlistManager.list) {
						var product = wishlistManager.list[key];
						trackValue += product.id+","+product._quantity+";";
					}
					trackValue = trackValue.substring(0, trackValue.length-1);
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"WishlistEvents"+"|"+trackValue, eventLabel:"mail", eventValues:{url:trackValue}});
					break;
				case 'wishlist_email_error': //
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"WishlistEvents", eventLabel:"mail_error"});
					break;
					
				//product
				case 'product_open':
					var productId = eventValues.id;
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ProductEvents"+"|"+productId, eventLabel:"click", eventValues:{url:productId}});
					break;
				
				//descopera gama
				case 'gama_open':
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"GamaEvents"+"|"+eventValues.url, eventLabel:"click", eventValues:{url:eventValues.url}});
					break;
				
				//descopera articolul
				case 'articol_open':
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ArticolEvents"+"|"+eventValues.url, eventLabel:"click", eventValues:{url:eventValues.url}});
					break;
					
				//deschide url
				case 'url_open':
					trackedObjects.push({type:"event", eventCategory:"", eventAction:"ButtonEvents"+"|"+eventValues.url, eventLabel:"click", eventValues:{url:eventValues.url}});
					break;
			}
			
			this.sendTrackedObjects(eventCode, trackedObjects, forceTracking);
		},

		//loading trackers
		loadTrackers: function (options) {
			this.initGoogleAnalyticsTracker(options.vars, this.gaVersion);
		},
		initGoogleAnalyticsTracker: function (data, version) {
			this.initGoogleAnalyticsTracker_analytics(data);
		},

		initGoogleAnalyticsTracker_analytics: function (data) {
			this.trackerLoader(('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/analytics.js');
			var r = 'ga', i = window;
			i['GoogleAnalyticsObject']=r;
			i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)
			},i[r].l=1*new Date();
			ga('create', data['_setAccount'], 'auto');
		},
		
		trackerLoader: function(url){
			(function() {
				var ga = document.createElement('script'); 
					ga.type = 'text/javascript'; 
					ga.async = true;
					ga.onload = function(){
						MainFrame.Tracker.loadedTrackers++;
						
						MainFrame.Tracker.sendTrackedEventNew({
							eventCode: 'page_changed',
							extraValues: {
								page: window.location.pathname,
							}
						});
					}
					ga.src = url;
					
				var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(ga, s);
			})();
		},
		
		finishedLoading: function(){
			if(this.loadedTrackers == this.trackersCounter){
				return true
			}
			else return false
		},

		//processing track requests
		sendTrackedEventNew: function(trackOptions) {
			//event
			this.trackEvent(trackOptions);
			
		},
		
		
		sendTrackedObjects: function(eventCode, trackedObjects, forceTracking) {
			if (trackedObjects.length>0) {
				for (var i in MainFrame.Tracker.trackers) {
					var trackerObj = MainFrame.Tracker.trackers[i];
					var eventIsTracked = (trackerObj.codes.indexOf(eventCode) !== -1)
					if (forceTracking === true) {
						eventIsTracked = true;
					}

					if (eventIsTracked) {
						for (var j in trackedObjects) {
							var values = trackedObjects[j].eventValues ? trackedObjects[j].eventValues : {};
								values.lang = 'ro';
							MainFrame.Tracker.callTracker({
								source: 'html5',
								type: trackerObj.type,
								trackedEvent: trackedObjects[j].eventString,
								trackedEventObject: trackedObjects[j],
								trackerObj: trackerObj,
								eventCode:eventCode,
								appId:this.appId,
								values : values
							});
						}
					}
				}
			}
		},
		
		//sending values
		callTracker: function (options) {
			
			this.oras = V7_Store.get();
			if (typeof this.oras === "undefined" || this.oras === null) {
				this.trackedEvents.push(options);
				return;
			}
			
			if(this.finishedLoading()){
				//callTracker for queued events
				if(this.trackedEvents.length!=0){
					for(var i in this.trackedEvents)
						this.callTrackerQueue()
				}
				
				var name = this.appId + "/"+this.oras+"/catalogue";
				var variableName = (String(options.source).indexOf("flash")>=0) ? String(options.source).split("flash").join(name) : String(options.source).split("html5").join(name);
				if (options.trackerObj) {
					if (options.trackerObj.vars.catalogVarName) {
						variableName = options.trackerObj.vars.catalogVarName ? options.trackerObj.vars.catalogVarName : variableName;
					}
				}
				
				var trackedEvent = variableName + options.trackedEvent;	
				//var trackedEvent = options.source + options.trackedEvent;
				
				switch (options.type) {
					case 'GA':
					// falltrough
					default:
						if (this.gaVersion === 'analytics') {
							if (options.trackedEventObject && options.trackedEventObject.type && options.trackedEventObject.type == "event") {
								var gaEvent = {hitType: 'event'};
									gaEvent.eventCategory = variableName;
									if (typeof options.trackedEventObject.eventCategory != "undefined" && options.trackedEventObject.eventCategory != "") {gaEvent.eventCategory=options.trackedEventObject.eventCategory};
									if (typeof options.trackedEventObject.eventAction != "undefined") {gaEvent.eventAction=options.trackedEventObject.eventAction};
									if (typeof options.trackedEventObject.eventLabel != "undefined") {gaEvent.eventLabel=options.trackedEventObject.eventLabel};
									if (typeof options.trackedEventObject.eventValue != "undefined") {gaEvent.eventValue=options.trackedEventObject.eventValue};
								
								ga('send', gaEvent);
							} else {
								var gaPageViewValue = variableName + options.trackedEvent;
								
								ga('send', 'pageview', gaPageViewValue);
							}
						} else {
							window._gaq.push(['_trackPageview',trackedEvent]);
						}
				}
				if (this.useBD == true) {
					this.trackBD(options);
				}
			} else{
				this.trackedEvents.push(options)
			}
		},

		callTrackerQueue: function () {
			var trackedEvent = this.trackedEvents.pop()
			MainFrame.Tracker.callTracker(trackedEvent)
		},

		callTrackerMockup: function (options) {
			
			this.oras = V7_Store.get();
			if (typeof this.oras === "undefined" || this.oras === null) {
				this.trackedEvents.push(options);
				return;
			}
			
			var name = this.appId + "/"+this.oras+"/catalogue";
			var variableName = (String(options.source).indexOf("flash")>=0) ? String(options.source).split("flash").join(name) : String(options.source).split("html5").join(name);
			if (options.trackerObj) {
				if (options.trackerObj.vars.catalogVarName) {
					variableName = options.trackerObj.vars.catalogVarName ? options.trackerObj.vars.catalogVarName : variableName;
				}
			}
			
			var trackedEvent = variableName + options.trackedEvent;
			if (options.trackedEventObject && options.trackedEventObject.type && options.trackedEventObject.type == "event") {
				var gaEvent = {hitType: 'event'};
					gaEvent.eventCategory = variableName;
					if (typeof options.trackedEventObject.eventCategory != "undefined" && options.trackedEventObject.eventCategory != "") {gaEvent.eventCategory=options.trackedEventObject.eventCategory};
					if (typeof options.trackedEventObject.eventAction != "undefined") {gaEvent.eventAction=options.trackedEventObject.eventAction};
					if (typeof options.trackedEventObject.eventLabel != "undefined") {gaEvent.eventLabel=options.trackedEventObject.eventLabel};
					if (typeof options.trackedEventObject.eventValue != "undefined") {gaEvent.eventValue=options.trackedEventObject.eventValue};
				
				console.log("TEST EVENT:"+options.type);
				console.log(gaEvent);
			} else {
				console.log("TEST PAGEVIEW:"+options.type + ': ' + trackedEvent);
			}
			/*
			if (this.useBD == true) {
				this.trackBD(options);
			}
			*/
		},

		trackBD: function(options) {
			
			var url = '//a.secure-load.com/favicon.ico';			
				options.values.v = this.v
			var values = JSON.stringify(options.values)
			this.request({
				url: url,
				xmlResponse: false,
				method: 'POST',
				formData: true,
				credentials: true,
				formDataParams: {
					app: this.base64encode('{\
						"appId":"' + options.appId + '",\
						"eventCode":"' + options.eventCode + '",\
						"values":' + values + ',\
						"appType":"' + options.source + '"\
						}')
				},
				onSuccess: function (data) {
					//console.log(data)
				},
				onError: function (e) {
					//console.log('Eroare');
				}
			});
		},
		
		request: function(options) {
            options = options || {};
            var url = options.url,
                method = options.method || 'GET',
                onSuccess = options.onSuccess,
                onError = options.onError,
                ps = options.ps,
                formData = options.formData || null,
                formDataParams = options.formDataParams,
                credentials = options.credentials || false,
                xmlResponse = options.xmlResponse || false,
                jsonResponse = options.jsonResponse || false;
            if (options.onSucces) {
                onSuccess = options.onSucces;
                console.log('Warning: the correct callback name parameter for the request method is "onSuccess"');
            }
            

            if (typeof ps !== 'undefined') {
                var paramsParts = [];
                for (var paramName in ps) {
                    var paramValue = ps[paramName],
                        paramString = paramName + '=' + paramValue;
                    paramsParts.push(paramString);
                }
                var paramsString = paramsParts.join('&');
                var urlHasParams = (url.indexOf('?') !== -1);
                if (urlHasParams) {
                    url += '&' + paramsString;
                } else {
                    url += '?' + paramsString;
                }
            }
            if (typeof formDataParams !== 'undefined') {
                var formData = new FormData();
                for (var paramName in formDataParams) {
                    formData.append(paramName, formDataParams[paramName]);
                }
            }
            var xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (xhr.status == 200) {
                        var response = xhr.responseText;
                        if (xmlResponse) {
                            response = xhr.responseXML;
                        } else if (jsonResponse) {
                            response = JSON.parse(response);
                        }
                        if (onSuccess) {
                            onSuccess(response);
                        }
                    } else {
                        if (onError) {
                            onError(xhr);
                        }
                    }
                }
				//else onError(xhr);
            };
            if (credentials) {
                xhr.withCredentials = true;
            }
            if (url) {
                xhr.open(method, url, true);
                xhr.send(formData);
            }
        },

		keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

		base64encode: function (input) {

			var keyStr = this.keyStr;

			input = escape(input);

			var output = '',
				chr1, chr2, chr3,
				enc1, enc2, enc3, enc4;

			var i = 0;
			do {

				chr1 = input.charCodeAt(i++);
				chr2 = input.charCodeAt(i++);
				chr3 = input.charCodeAt(i++);

				enc1 = chr1 >> 2;
				enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
				enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
				enc4 = chr3 & 63;

				if (isNaN(chr2)) {
					enc3 = enc4 = 64;
				} else if (isNaN(chr3)) {
					enc4 = 64;
				}

				output = output
					   + keyStr.charAt(enc1)
					   + keyStr.charAt(enc2)
					   + keyStr.charAt(enc3)
					   + keyStr.charAt(enc4);
				chr1 = chr2 = chr3 = '';
				enc1 = enc2 = enc3 = enc4 = '';
			} while (i < input.length);

			return output;
		},

		base64decode: function (input) {

			var keyStr = this.keyStr;

			var output = '',
				chr1, chr2, chr3,
				enc1, enc2, enc3, enc4;

			// removing all characters that are not A-Z, a-z, 0-9, +, /, or =
			input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');

			var i = 0;
			do {

				enc1 = keyStr.indexOf(input.charAt(i++));
				enc2 = keyStr.indexOf(input.charAt(i++));
				enc3 = keyStr.indexOf(input.charAt(i++));
				enc4 = keyStr.indexOf(input.charAt(i++));

				chr1 = (enc1 << 2) | (enc2 >> 4);
				chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
				chr3 = ((enc3 & 3) << 6) | enc4;

				output = output + String.fromCharCode(chr1);

				if (enc3 != 64) {
					output = output + String.fromCharCode(chr2);
				}

				if (enc4 != 64) {
					output = output + String.fromCharCode(chr3);
				}

				chr1 = chr2 = chr3 = '';
				enc1 = enc2 = enc3 = enc4 = '';

			} while (i < input.length);

			return unescape(output);
		},
	}

	// Production steps of ECMA-262, Edition 5, 15.4.4.18
	// Reference: http://es5.github.io/#x15.4.4.18
	if (!Array.prototype.forEach) {

	  Array.prototype.forEach = function(callback, thisArg) {

	    var T, k;

	    if (this == null) {
	      throw new TypeError(' this is null or not defined');
	    }

	    // 1. Let O be the result of calling ToObject passing the |this| value as the argument.
	    var O = Object(this);

	    // 2. Let lenValue be the result of calling the Get internal method of O with the argument "length".
	    // 3. Let len be ToUint32(lenValue).
	    var len = O.length >>> 0;

	    // 4. If IsCallable(callback) is false, throw a TypeError exception.
	    // See: http://es5.github.com/#x9.11
	    if (typeof callback !== "function") {
	      throw new TypeError(callback + ' is not a function');
	    }

	    // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
	    if (arguments.length > 1) {
	      T = thisArg;
	    }

	    // 6. Let k be 0
	    k = 0;

	    // 7. Repeat, while k < len
	    while (k < len) {

	      var kValue;

	      // a. Let Pk be ToString(k).
	      //   This is implicit for LHS operands of the in operator
	      // b. Let kPresent be the result of calling the HasProperty internal method of O with argument Pk.
	      //   This step can be combined with c
	      // c. If kPresent is true, then
	      if (k in O) {

	        // i. Let kValue be the result of calling the Get internal method of O with argument Pk.
	        kValue = O[k];

	        // ii. Call the Call internal method of callback with T as the this value and
	        // argument list containing kValue, k, and O.
	        callback.call(T, kValue, k, O);
	      }
	      // d. Increase k by 1.
	      k++;
	    }
	    // 8. return undefined
	  };
	}
	
	MainFrame.Tracker.initializeTracker();

})();