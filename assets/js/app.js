var wishlist = new MainFrame.Ext.Wishlist({id: 'bricodepot.dev', id_property: 'id'});

var product;

var embedProductOverlay;

function embedProduct(product_id, product_options) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<div id="product_details" class="ext-module-js" data-module="template" data-options-id="product_template_options"></div>'

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    //var iframe = embedProductOverlay.getElementByClassName('');
    //iframe.className = 'v7__embed__center v7__embed__overlay-video-container';
    document.body.appendChild(embedProductOverlay);

    var product_template_el,
        product_template;

    Products.getProduct(product_id).then(function (flux_product) {

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'product_open',
            extraValues: {
                id: product_id,
            }
        });


        product = flux_product;

        //
        if (product_options) {
            product.options = product_options;
            product.hasBadge = (product_options.badge && product_options.badge.length > 0) ? true : false;
            product.hasNotBadge = !product.hasBadge ;
            product.hasNewPrice = (product_options.new_price && product_options.new_price.length > 0) ? true : false;
            product.hasOldPrice = (product_options.old_price && product_options.old_price.length > 0) ? true : false;
        }else{
            product.options = {};
            product.hasBadge = false;
            product.hasNotBadge = true;
            product.hasOldPrice = false;
        }

        if(!product.hasBadge){
            product.options.badge = '';
        }else{
            product.options.badge = 'assets/img/' + product.options.badge;
        }
        //
        product.hasNotBadge_and_hasSecondPrice = product.hasNotBadge && product.hasSecondPrice;
        product.hasNotBadge_and_hasNotSecondPrice = product.hasNotBadge && product.hasNotSecondPrice;
        //

        product_template_el = document.getElementById('product_details');
        product_template = new MainFrame.Ext.Template({
            target: {
                element: product_template_el,
                options: {
                    template_id: 'product_template'
                }
            },
            data: {
                object: product
            }
        });
        product_template_el.style.visibility = 'visible';

        product_template_el.onclick = function (e) {
            if (e.target && e.target.nodeName.toLowerCase() == 'a') {

            } else {
                e.preventDefault();
                e.stopPropagation();
            }
        }

    });


    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


function embedProducts(product_ids, product_options) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<div id="product_details" class="ext-module-js" data-module="template" data-options-id="product_template_options"></div>'

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    //var iframe = embedProductOverlay.getElementByClassName('');
    //iframe.className = 'v7__embed__center v7__embed__overlay-video-container';
    document.body.appendChild(embedProductOverlay);

    var product_template_el,
        product_template;

    Products.getProducts(product_ids).then(function (flux_products) {

        product = flux_products[0];

        MainFrame.Tracker.sendTrackedEventNew({
            eventCode: 'product_open',
            extraValues: {
                id: product.id,
            }
        });

        //
        if (product_options) {
            product.options = product_options;
            product.hasBadge = (product_options.badge && product_options.badge.length > 0) ? true : false;
            product.hasNotBadge = !product.hasBadge ;
            product.hasNewPrice = (product_options.new_price && product_options.new_price.length > 0) ? true : false;
            product.hasOldPrice = (product_options.old_price && product_options.old_price.length > 0) ? true : false;
        }else{
            product.options = {};
            product.hasBadge = false;
            product.hasNotBadge = true;
            product.hasOldPrice = false;
        }

        if(!product.hasBadge){
            product.options.badge = '';
        }else{
            product.options.badge = 'assets/img/' + product.options.badge;
        }
        //
        product.hasNotBadge_and_hasSecondPrice = product.hasNotBadge && product.hasSecondPrice;
        product.hasNotBadge_and_hasNotSecondPrice = product.hasNotBadge && product.hasNotSecondPrice;
        //

        product.other_products = flux_products;
        if (product.other_products.length > 3) {
            product.should_scroll_other_products = true;
        } else {
            product.should_scroll_other_products = false;
        }


        product_template_el = document.getElementById('product_details');
        product_template = new MainFrame.Ext.Template({
            target: {
                element: product_template_el,
                options: {
                    template_id: 'product_multi_template'
                }
            },
            data: {
                object: product
            }
        });
        product_template_el.style.visibility = 'visible';

        product_template_el.onclick = function (e) {
            if (e.target && e.target.nodeName.toLowerCase() == 'a') {

            } else {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    });

    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


function updateProduct(product_id) {

    var other_products = product.other_products;

    var new_product;
    other_products.forEach(function (other_product) {
        if (other_product.id == product_id) {
            new_product = other_product;
        }
    });

    var product_template_el = document.getElementById('product_details');

    var lib = product_template_el.getElementsByClassName('lib')[0];
    lib.innerHTML = new_product.lib;

    var ref = product_template_el.getElementsByClassName('ref')[0];
    ref.innerHTML = new_product.ref;

    var photo = product_template_el.getElementsByClassName('product_photo')[0];
    photo.src = new_product.photo;

    var prices_date = product_template_el.getElementsByClassName('prices_date')[0];
    prices_date.innerHTML = new_product.prices_date;

    var pdf = product_template_el.getElementsByClassName('pdf')[0];
    pdf.href = new_product.pdf;
    if (new_product.has_pdf) {
        pdf.classList.add('visible');
    } else {
        pdf.classList.remove('visible');
    }

    var desc = product_template_el.getElementsByClassName('desc_content')[0];
    desc.innerHTML = new_product.desc;

    var price = product_template_el.getElementsByClassName('price')[0];
    price.innerHTML = new_product.price + ' Lei / ' + new_product.unit;

    var add_button = product_template_el.getElementsByClassName('add_product')[0];
    add_button.onclick = function () {
        addProduct(new_product.id + '');
    };

    var visit_button = product_template_el.getElementsByClassName('visit_product')[0];
    visit_button.href = new_product.url;

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'product_open',
        extraValues: {
            id: new_product.id,
        }
    });

    product = new_product;
    product.other_products = other_products;
};


function scrollMultiRight() {

    var multi_product_wrapper = document.getElementsByClassName('multi_product_wrapper')[0],
        wrapper_width = multi_product_wrapper.clientWidth,
        scroll_distance = Math.floor(wrapper_width / 3),
        max_distance = (product.other_products.length - 3) * scroll_distance;

    if (multi_product_wrapper.scrollLeft + scroll_distance <= max_distance) {
        multi_product_wrapper.scrollLeft += scroll_distance;
    }

    var scroll_left_button = document.getElementsByClassName('scroll_left')[0],
        scroll_right_button = document.getElementsByClassName('scroll_right')[0];

    if (multi_product_wrapper.scrollLeft >= max_distance) {
        scroll_right_button.style.opacity = '0.5';
    }
    scroll_left_button.style.opacity = '1';
};

function scrollMultiLeft() {

    var multi_product_wrapper = document.getElementsByClassName('multi_product_wrapper')[0],
        wrapper_width = multi_product_wrapper.clientWidth,
        scroll_distance = Math.floor(wrapper_width / 3);

    if (multi_product_wrapper.scrollLeft - scroll_distance >= 0) {
        multi_product_wrapper.scrollLeft -= scroll_distance;
    }

    var scroll_left_button = document.getElementsByClassName('scroll_left')[0],
        scroll_right_button = document.getElementsByClassName('scroll_right')[0];

    if (multi_product_wrapper.scrollLeft == 0) {
        scroll_left_button.style.opacity = '0.5';
    }
    scroll_right_button.style.opacity = '1';
};


function embedWishlist() {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_open',
        extraValues: {
            page: window.location.pathname,
        }
    })

    document.body.style.overflow = 'hidden';

    var embed_wishlist_template = document.getElementById('embed_wishlist_template').innerHTML.trim();

    var embedCode = embed_wishlist_template;

    embedProductOverlay = document.createElement('div');
    embedProductOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedProductOverlay.innerHTML = embedCode;

    document.body.appendChild(embedProductOverlay);

    if (wishlist.getProducts().length) {

        var wishlist_products = wishlist.getProducts();
        wishlist_template_el = document.getElementById('wishlist'),
            wishlist_template = new MainFrame.Ext.Template({
                target: {
                    element: wishlist_template_el,
                    options: {
                        template_id: 'wishlist_template',
                        items: 'all',
                    }
                },
                data: {
                    object: wishlist_products
                }
            });

        var wishlist_total_el = document.getElementById('wishlist_total'),
            wishlist_total = new MainFrame.Ext.WishlistBadge({
                target: {
                    element: wishlist_total_el
                },
                details: {
                    text: 'Total: #price lei',
                    wishlist_id: 'wishlist',
                    static: true
                }
            });

        wishlist_template_el.style.visibility = 'visible';
        wishlist_total_el.style.visibility = 'visible';
    }

    var wishlist_container = document.getElementById('wishlist_container');
    wishlist_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    embedProductOverlay.onclick = function (e) {
        closeEmbedProduct();
    }
};


var embedPopupOverlay;

function embedPopup(message) {

    var embed_popup_template = document.getElementById('embed_popup_template').innerHTML.trim();

    var embedCode = embed_popup_template;

    embedPopupOverlay = document.createElement('div');
    embedPopupOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedPopupOverlay.innerHTML = embedCode;

    document.body.appendChild(embedPopupOverlay);

    var popup_text = document.getElementById('popup_text');
    popup_text.innerHTML = message;


    var popup_container = document.getElementById('popup_container');
    popup_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    embedPopupOverlay.onclick = function (e) {
        closeEmbedPopup();
    }
};

function closeEmbedPopup() {
    document.body.removeChild(embedPopupOverlay);
};


function shareFacebook() {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'share_facebook',
        extraValues: {
            page: window.location.pathname,
        }
    })
    window.open("http://www.facebook.com/sharer.php?u=https://www.bricodepot.ro");
}

function openUrl(url, type) {
    var trackingType = "url_open";
    if (typeof type != undefined) {
        trackingType = type;
    }
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: trackingType,
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

function openGama(url) {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "gama_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

function openGamaStore(url) {
    //
    var store = window.V7_Store.get();
    url = url.replace('#{V7_Store}', store);
    //
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "gama_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}


function openArticol(url) {
    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: "articol_open",
        extraValues: {
            url: url,
        }
    })

    window.open(url);
}

window.updateQuantity = function updateQuantity(input_id, increment) {

    var quantity_input = document.getElementById(input_id),
        quantity = parseInt(quantity_input.value, 10);

    quantity += increment;

    if (quantity > 1) {
        quantity_input.value = quantity;
    }
};

window.updateWishlistQuantity = function updateWishlistQuantity(product_id, input_id, increment) {

    var quantity_input = document.getElementById(input_id),
        quantity = parseInt(quantity_input.value, 10);

    quantity += increment;

    if (quantity >= 1) {
        quantity_input.value = quantity;
        wishlist.incrementProductQuantity(product_id, increment);
    }
};

window.removeProduct = function removeProduct(product_id) {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_remove',
        extraValues: {
            id: product_id,
        }
    })

    wishlist.removeProduct(product_id);

    var wishlist_product_el = document.getElementById('wishlist_product_' + product_id);
    wishlist_product_el.parentNode.removeChild(wishlist_product_el);
};

window.addProduct = function addProduct(product_id) {

    var quantity_input = document.getElementById('quantity'),
        quantity = parseInt(quantity_input.value, 10);

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_add',
        extraValues: {
            id: product_id,
        }
    })

    if (product) {

        // daca ramane proprietatea other_products la adaugare
        // nu va functiona clonarea (nu se poate converti o structura circulara in json)
        // deoarece other_products contine si referinta catre product
        if (product.other_products) {
            var other_products = product.other_products;
            delete product.other_products;
        }

        wishlist.addProduct(product, quantity);
        embedPopup('Articolul dumneavoastră a fost adăugat cu succes în lista de cumpărături!');

        // la final punem la loc
        if (typeof other_products !== 'undefined') {
            product.other_products = other_products;
        }
    }
};

window.clearWishlist = function clearWishlist() {

    wishlist.empty();

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_empty',
    })

    var wishlist_template_el = document.getElementById('wishlist');
    wishlist_template_el.innerHTML = '';
};

window.printWishlist = function printWishlist() {

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_print',
    });

    var actionPrint = 'https://www.interactivpdf.com/2018/WEBPROJECTS/W0017.Bricodepot/externals/ldc/index_print.php';

    var list = wishlist.getProducts();

    var basket = '<products>';
    list.forEach(function (product) {

        basket += '<product>\
					<id><![CDATA[' + product.id + ']]></id>\
					<qty><![CDATA[' + product._quantity + ']]></qty>\
					<obj>\
						<price><![CDATA[' + product.price + ']]></price>\
						<lib><![CDATA[' + product.lib + ']]></lib>\
						<desc><![CDATA[' + product.desc + ']]></desc>';
        if (product.photo && product.photo.indexOf("http") == 0) {
            basket += '<photo>' + product.photo + '</photo>';
        }
        basket += '	</obj>\
				</product>'
    });

    basket += '</products>';
    var xml = '<?xml version="1.0" encoding="utf-8"?>';
    xml += '<doc>';
    xml += '<mailUrl><![CDATA[' + actionPrint.split('externals/')[0] + ']]></mailUrl>';
    xml += basket;
    xml += '</doc>';
    var fd = new FormData();
    fd.append('xmloutput', xml);
    fd.append('lang', 'RO');
    var xhr = new XMLHttpRequest();
    xhr.open('POST', actionPrint, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var listWindow = window.open("", "");
            var windowContent = xhr.responseText;
            var scriptText = 'var imgs=document.getElementsByTagName("img");';
            scriptText += 'var index=0,totalImages=imgs.length,loadedImages=0;';
            scriptText += 'var imgCallback=function(){loadedImages++;if(loadedImages===totalImages){';
            scriptText += 'window.print()';
            scriptText += '}};for(;index<totalImages;index++){';
            scriptText += 'var imgEl=imgs[index],imgSrc=imgEl.getAttribute("src");';
            scriptText += 'var img=new Image;img.onload=imgCallback;img.onerror=imgCallback;img.src=imgSrc}';
            windowContent = windowContent.replace('</body>', '<script>' + scriptText + '</script></body>');
            listWindow.document.write(windowContent);
        }
    };
    xhr.send(fd);

};

var sendWishlistOverlay;
window.openSendWishlist = function openSendWishlist() {

    closeEmbedProduct();

    MainFrame.Tracker.sendTrackedEventNew({
        eventCode: 'wishlist_email',
    });

    var send_wishlist_template = document.getElementById('send_wishlist_template').innerHTML.trim();

    var embedCode = send_wishlist_template;

    sendWishlistOverlay = document.createElement('div');
    sendWishlistOverlay.className = 'v7__embed__center v7__embed__overlay';
    sendWishlistOverlay.innerHTML = embedCode;

    document.body.appendChild(sendWishlistOverlay);

    if (typeof grecaptcha !== 'undefined') {
        grecaptcha.render('send_wishlist_recaptcha', {
            'theme': 'light',
            'size': 'normal',
            'hl': 'ro'
        });
    }

    var send_wishlist_container = document.getElementById('send_wishlist_container');
    send_wishlist_container.onclick = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    sendWishlistOverlay.onclick = function (e) {
        closeSendWishlist();
    }

};


window.sendWishlist = function sendWishlist() {

    var actionPrint = 'https://www.interactivpdf.com/2018/WEBPROJECTS/W0017.Bricodepot/externals/ldc/index.php';

    var list = wishlist.getProducts();

    var firstName = document.getElementById('ldc_firstName').value,
        lastName = document.getElementById('ldc_lastName').value,
        to = document.getElementById('ldc_to').value,
        message = document.getElementById('ldc_message').value,
        recaptcha_response = '';

    var formContainer = document.getElementById('send_wishlist_container'),
        textareas = formContainer.getElementsByTagName('textarea'),
        index = 0,
        total_textareas = textareas.length;

    for (; index < total_textareas; index++) {

        var textarea = textareas[index];
        if (textarea.name === 'g-recaptcha-response') {
            recaptcha_response = textarea.value;
        }
    }

    var basket = '<products>';
    list.forEach(function (product) {

        basket += '<product>\
					<id><![CDATA[' + product.id + ']]></id>\
					<qty><![CDATA[' + product._quantity + ']]></qty>\
					<obj>\
						<price><![CDATA[' + product.price + ']]></price>\
						<lib><![CDATA[' + product.lib + ']]></lib>\
						<desc><![CDATA[' + product.desc + ']]></desc>';
        if (product.photo && product.photo.indexOf("http") == 0) {
            basket += '<photo>' + product.photo + '</photo>';
        }
        basket += '	</obj>\
				</product>'
    });

    basket += '</products>';

    var xml = '<?xml version="1.0" encoding="utf-8"?>';
    xml += '<doc>';

    //<urlrequest><![CDATA[https://www.bricodepot.ro/catalog/]]></urlrequest>
    //<finurl><![CDATA[https://www.bricodepot.ro/catalog/index.html]]></finurl>

    xml += '<urlrequest><![CDATA[]]></urlrequest>';
    xml += '<finurl><![CDATA[]]></finurl>';
    xml += '<lang><![CDATA[ro]]></lang>';
    xml += '<firstName><![CDATA[' + firstName + ']]></firstName>';
    xml += '<lastName><![CDATA[' + lastName + ']]></lastName>';
    xml += '<to><![CDATA[' + to + ']]></to>';
    xml += '<message><![CDATA[' + message + ']]></message>';

    xml += '<mailUrl><![CDATA[' + actionPrint.split('externals/')[0] + ']]></mailUrl>';
    xml += basket;
    xml += '</doc>';

    var fd = new FormData();
    fd.append('xmloutput', xml);
    fd.append('lang', 'RO');
    fd.append('g-recaptcha-response', recaptcha_response);
    fd.append('firstName', firstName);
    fd.append('lastName', lastName);
    fd.append('to', 'to');
    fd.append('message', message);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', actionPrint, false);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if (xhr.responseText === 'sendResult=true') {
                embedPopup('Mail-ul dumneavoastră a fost trimis.');
            } else {
                embedPopup('Din păcate, mail-ul dumneavoastră nu a fost trimis.<br />Vă rugăm să încercaţi mai târziu.');
            }
        } else {
            embedPopup('Din păcate, mail-ul dumneavoastră nu a fost trimis.<br />Vă rugăm să încercaţi mai târziu.');
        }
    };
    xhr.send(fd);
}


function closeSendWishlist() {
    document.body.removeChild(sendWishlistOverlay);
    document.body.style.overflow = '';
}


function closeEmbedProduct() {
    document.body.removeChild(embedProductOverlay);
    document.body.style.overflow = '';
}

MainFrame.Ext.Map.prototype.bindZoneAction = function (zone_element, zone_data) {

    var self = this;

    if (zone_data.display === 'hs') {
        var target = zone_element.getElementsByClassName(self.css_classes['hotspot'])[0];
    } else {
        var target = zone_element;
    }

    var refs = self.getRefs(zone_data),
        ref = refs[0];

    target.onclick = function () {
        var product_id = ref.url;
        embedProduct(product_id)
    }
}

var embedZoomOverlay;

function embedZoom(image) {

    document.body.style.overflow = 'hidden';

    var embedCode = '<img class="zoomed_image" src="' + image + '">';

    embedZoomOverlay = document.createElement('div');
    embedZoomOverlay.className = 'v7__embed__center v7__embed__overlay';
    embedZoomOverlay.innerHTML = embedCode;

    document.body.appendChild(embedZoomOverlay);

    embedZoomOverlay.onclick = function (e) {
        closeEmbedZoom();
    }
};

function closeEmbedZoom() {
    document.body.style.overflow = '';
    document.body.removeChild(embedZoomOverlay);
};

function nop() {
}

$shown_after_scroll = $('.shown_after_scroll');
$(window).scroll(function () {
    if ($(this).scrollTop() >= 85) {        // If page is scrolled more than 85px (header menu)
        if (!$shown_after_scroll._faded) {
            $shown_after_scroll.fadeIn(200);
            $shown_after_scroll._faded = true;
        }
    } else {
        $shown_after_scroll.fadeOut(200);
        $shown_after_scroll._faded = false;
    }
});