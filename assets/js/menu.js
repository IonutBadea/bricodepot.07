

var main_menu = document.getElementById('main_menu'),
	app_nav   = document.getElementsByClassName('app_nav')[0];

window.toggleMainMenu = function openMainMenu () {

	if (main_menu._opened === true) {

		document.body.style.overflow = '';

		app_nav.classList.remove('open');
		main_menu.classList.remove('open');

		main_menu._opened = false;
		
		MainFrame.Tracker.sendTrackedEventNew({
			eventCode: 'menu_close',
			extraValues: {
				page: window.location.pathname,
			}
		})
	} else {
	
		document.body.style.overflow = 'hidden';

		app_nav.classList.add('open');
		main_menu.classList.add('open');

		main_menu._opened = true;

		MainFrame.Tracker.sendTrackedEventNew({
			eventCode: 'menu_open',
			extraValues: {
				page: window.location.pathname,
			}
		})
	}

}