(function () {

	var is_preprod = (window.location.href.indexOf('preprod=true') !== -1);

	var prices_url = 'https://www.bricodepot.ro/catalog/' + (is_preprod ? 'app_preprod' : 'catalog') + '/assets/prices/price.php?shop=',
	//var prices_url = 'https://www.bricodepot.ro/catalog/' + (is_preprod ? 'app_preprod' : 'catalog') + '/assets/prices/price.php?shop=',
		prices = {};

	var updateWishlist = function updateWishlist () {

		if (typeof wishlist !== 'undefined') {

			var products = wishlist.getProducts();
			products.forEach(function (product) {

				var price_data = prices[product.id];
				if (price_data) {
					product.price = price_data.price;
					product.unit  = price_data.unit;
					product.prices_date = price_data.prices_date;
				}

				if (product.price && product.price !== 'N/A') {
					product.pretty_price = parseFloat(product.price, 10).toFixed(2);
				}
			});

			wishlist.save();
		}
	};

	var parsePrices = function (response) {

		var xml_data = MainFrame.Ext.Parser.parseXmlFlux(response.request.responseXML.documentElement);
		xml_data.children.p.forEach(function (price_node) {
			
			var price = {};
			price.id = price_node.attributes['id'];
			price.url = price_node.attributes['url']; 
			price.unit = price_node.attributes['unit'];
			price.price = price_node.attributes['price'];
			price.packagePrice = price_node.attributes['packagePrice'];
			price.prices_date  = xml_data.children.prices_date.value;

			prices[price.id] = price;
		});

		updateWishlist();
	};

	var getPrices = new Promise(function (resolve, reject) { 

		axios.get(prices_url + V7_Store.get())
		.then(function (response) {
			if (response.request.responseXML) {
				parsePrices(response);
				resolve();
			} else {
				reject();
			}
		})
		.catch(function () {
			reject();
		});
	});

	getPrices.then();


	var refreshPrices = function refreshPrices () {

		return new Promise(function (resolve, reject) {

			axios.get(prices_url + V7_Store.get())
			.then(function (response) {
				if (response.request.responseXML) {
					parsePrices(response);
					resolve();
				} else {
					reject();
				}
			})
			.catch(function () {
				reject();
			});
		});
	};


	var getProductFromResponse = function getProductFromResponse (response) {

		var product = {};

		if (response.request.responseXML) {
			var xml_data = MainFrame.Ext.Parser.parseXmlFlux(response.request.responseXML.documentElement);

			product.id = xml_data.attributes.id;
			product.ref = xml_data.children.ref.value;
			product.url = xml_data.children.url.value;
			product.lib = xml_data.children.lib.value;
			product.desc = xml_data.children.desc.value;
			product.photo = xml_data.children.photo.value;

			product.photos = [];
			if (xml_data.children.photos) {
				xml_data.children.photos.childNodes.forEach(function (photo_node) {
					product.photos.push(photo_node.value);
				});
			}

			if (product.desc) {
				product.desc = product.desc.replace(/\n/gmi, '<br>');
			}

			if (xml_data.children.pdf) {
				product.pdf = xml_data.children.pdf.value;
				product.has_pdf = true;
			} else {
				product.has_pdf = false;
			}

			var price_data = prices[product.id];
			if (price_data) {
				product.price = price_data.price;
				product.unit  = price_data.unit;
				product.prices_date = price_data.prices_date;
			} else {
				product.price = 'N/A';
				product.unit  = '';
				product.prices_date = '';
			}

			if (product.price && product.price !== 'N/A') {
				product.pretty_price = parseFloat(product.price, 10).toFixed(2);
			} else {
				product.pretty_price = 'N/A';
			}
		}

		return product;
	}


	var get_product_url = 'assets/products/?method=getDetailsBySKU&sku=';
	var getProduct = function getProduct(sku) {

		return new Promise(function (resolve, reject) {
			axios.request(get_product_url + sku)
				.then(function (response) {

					var product = getProductFromResponse(response);

					resolve(product);
				})
				.catch(function (response) {
					reject(response);
				});
		});
	};


	var getProducts = function getProducts (skus_string) {

		return new Promise(function (resolve, reject) {

			var skus = skus_string.split(','),
				requests = [];
			skus.forEach(function (sku) {
				requests.push(axios.get(get_product_url + sku));
			});

			axios.all(requests)
				.then(function (responses) {

					var products = [];
					responses.forEach(function (response) {
						products.push(getProductFromResponse(response));
					});

					resolve(products);
				})
				.catch(function (responses) {
					reject(responses)
				});
		});
	};


	window.Products = {
		getProducts: getProducts,
		getProduct: getProduct,
		getPrices: getPrices,
		refreshPrices: refreshPrices
	};

})();