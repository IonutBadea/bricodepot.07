<?php
// Captcha

$key = '6LewJksUAAAAAOMmSm3-7FwASVBrKIUC5ZhuAAYm';//master

$response = $_POST['g-recaptcha-response'];
if (!$response) {
	die("sendResult=false");
}

$url = 'https://www.google.com/recaptcha/api/siteverify';
$data = array(
	'secret' => $key,
	'response' => $_POST["g-recaptcha-response"]
);
$options = array(
	'http' => array (
		'method' => 'POST',
		'content' => http_build_query($data)
	)
);
$context = stream_context_create($options);
$verify = file_get_contents($url, false, $context);
$captcha_success = json_decode($verify);

if ($captcha_success->success == false) {
	die("sendResult=false");
}

// End captcha

require 'includes/config.php';
require 'includes/classes/ldc-get-xml-data.php';
$xmldata = new xmldata();

//$_SESSION['nom']=stripslashes(htmlentities(utf8_decode($_POST['nom'])));
//if (isset($_POST['email'])) $_SESSION['email']=stripslashes(htmlentities(utf8_decode($_POST['email'])));

ob_start();
require 'includes/templates/' . LANG . '/template_textes.php';
require 'includes/templates/' . LANG . '/template_mail.php';
$mail_body = ob_get_contents();
ob_end_clean();

if (empty($xmldata->subject)) $xmldata->subject = $mail_subject;


include ('includes/classes/class.phpmailer.php');

$mail = new phpmailer();

/* $mail->From     = $xmldata->from; */
$mail->From     = "no-reply@interactifpdf.com";
$mail->FromName = "=?".$OUTPUT_HEADERS['CHARSET']."?".$OUTPUT_HEADERS['HEADERENCODING']."?".base64_encode($xmldata->firstName." ".$xmldata->lastName)."?=";;

$mail->Host     = "localhost";
$mail->Mailer   = "smtp";

$mail->Encoding = $OUTPUT_HEADERS['ENCODING'];
$mail->ContentType = 'text/html;';
$mail->CharSet=$OUTPUT_HEADERS['CHARSET'];

$mail->IsHTML(true);

/* $mail->Subject = "=?".$OUTPUT_HEADERS['CHARSET']."?".$OUTPUT_HEADERS['HEADERENCODING']."?".base64_encode($xmldata->subject)."?="; */
$mail->Subject = "=?UTF-8?B?".base64_encode($xmldata->firstName." ".$xmldata->lastName." te invita sa descoperi ofertele BRICO DEPÔT")."?=";

$mail->Body    = $mail_body;

/*if (isset($_FILES["_file"]["tmp_name"]) && isset($_FILES["_file"]["name"]) && !empty($_FILES["_file"]["tmp_name"])) {
	$mail->AddAttachment($_FILES["_file"]["tmp_name"],$_FILES["_file"]["name"]);
}*/
foreach ($xmldata->to as $to) {	
	$mail->AddAddress($to);
}
//die();
if(!$mail->Send()) die("sendResult=false"); else die("sendResult=true");
?>