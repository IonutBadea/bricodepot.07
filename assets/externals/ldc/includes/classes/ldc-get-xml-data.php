<?php

	class xmldata {
		
		public 
		
			$subject,
			$subjectTemp,
			$linktokta,
			$linktomail,
			$finurl,
			$firstname,
			$lastname,
			$firstnameTemp,
			$lastnameTemp,
			$from,
			$to = array(),
			$message,
			$messageTemp,
			$pagemark = array(),
			$annotations = array(),
			$offset,
			$prodId,
			$finalurl;



		private
		
			$xmlstring;

		
		function __construct() {
			
			
			if (isset($_POST['xmloutput'])) {
				
				$this->xmlstring = stripslashes($_POST['xmloutput']);
				
			}	else {
				
					die("Error receiving the xml data!");
				
			}
		
			
			$this->getDataFromXml();
			
		}
		
		
		private function getDataFromXml() {
			
			global $OUTPUT_HEADERS;
			
			$xml = simplexml_load_string($this->xmlstring);
			$xml->preserveWhiteSpace = false;

			foreach($xml as $key =>$val)
			{
				if($key == "to")
				{
					if (preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/',$val)) array_push($this->$key,$val);	
				}
				else
				{
					$this->$key = $val;
				}
				if($val->children())
				{
					$this->$key = $val;
				}
				else
				{
					if(!is_array($this->$key))
					{
						$this->$key = iconv("utf-8",$OUTPUT_HEADERS['CHARSET'],$val);
					}
				}
			}
			/*
			$this->linktokta = $this->urlrequest;
			if(isset($this->pagemarks))
			{
				$this->pagemark = explode(",",$this->pagemarks);
			}
			$url = parse_url($this->linktokta);
			$this->linktokta=preg_replace(array('/index.html/','/index.php/','/'.SWF_ENGINE.'/'),array('','',''),$url['scheme']."://".$url['host'].$url['path']);
			$query = array();
			if (isset($url['query'])) $query=explode("&",$url['query']);
			$queryvars = array();
			$vars = array();
			foreach($query as $value) {
				$dataandvalue = explode("=",$value);
				if (isset($dataandvalue[1])&& isset($dataandvalue[1])) $queryvars[$dataandvalue[0]] = $dataandvalue[1];
			}
			if (count($this->pagemark)>0) {
				$pagemark = implode("-",$this->pagemark);
				$queryvars['pagemark'] = $pagemark;
			}
			if (isset($this->prodId)) {
				$queryvars['prodId'] = $this->prodId;
			}
			
			$queryvars = array_filter($queryvars,array($this,"cleanQueryvars"));
			foreach($queryvars as $key =>$value) {
				switch($key) {
					case "G_SAVE_PAGE" :
						$key="page";
						break;
				}
				$vars[] = $key."=".$value;
			} 
			$this->finalurl = $this->linktokta.OPENINGPAGE;
			if (count($queryvars)!=0) $this->finalurl .= "?".implode("&",$vars);

			if (isset($this->finurl)) {
				$queryvars = array();
				$vars = array();
				if (count($this->pagemark)>0) {
					$pagemark = implode("_",$this->pagemark);
					$queryvars['pagemark'] = $pagemark;
				}
				if (isset($prodId)) {
					$queryvars['pagemark'] = $prodId;
				}
				$queryvars = array_filter($queryvars,array($this,"cleanQueryvars"));
				foreach($queryvars as $key =>$value) {
					switch($key) {
						case "G_SAVE_PAGE" :
							$key="page";
							break;
					}
					$vars[] = $key."=".$value;
				} 
				if (count($queryvars)!=0) $this->finurl .= "&app_data=".implode("&",$vars);
				$this->finalurl = $this->finurl;
			}
			*/
			
			/* foreach ($dom->getElementsByTagName("to") as $expeditor) {
				$emailsto = preg_split("/(,|;)/",$expeditor->nodeValue);
				foreach ($emailsto as $emailto) {
					if (preg_match("/^[A-Za-z0-9_\\.-]+@[A-Za-z0-9_\\.-]+[.][A-Za-z0-9_\\.-]+$/",$emailto)) $this->to[] = $emailto;					
				}
				
				
			};
			if (isset($dom->getElementsByTagName("message")->item(0)->nodeValue)) {
				$this->messageTemp = $dom->getElementsByTagName("message")->item(0)->nodeValue;
			} */
		}
		
		private function cleanQueryvars($var) {
			if (!empty($var)) return true;
				else return false;
			
		}
		
		public function addMailSubject($string) {
			
			$this->mailsubject = $string;
			
		}
		
		public function report() {
			
			foreach($this as $key => $value) {
				
				echo $key." => ".$value."<br />";
				
			}
			
		}
		
		
	}

?>