﻿<body>
	<div style = "width:690px;margin:0 auto;padding:20px;color:#000000;font-size:11px">
		<div id="header">
			<!-- <div style="background-color:#007eff;height:60px;"> -->
			<img src="<?php echo $xmldata->mailUrl?>/externals/ldc/images/logo.png" />
			<p style = "font-size:14px;color:#000000;font-weight:bold" >NOUA SELECȚIE TRIMISĂ DIN CATALOGUL INTERACTIV BRICO DEPÔT</p>
		</div>
		
		<table width="690" cellspacing="0" cellpadding="0" style="border:0px solid #ff0000;margin-left:5px">
			<?php 
			$totalprice = 0;
			$totalqty =0;
		
			foreach ($xmldata->products as $key=>$val){
				$price2prod = 0;
				if (strpos($val->obj->price, ",")!==false) {
					$tmpPrice = (str_replace(",", ".", $val->obj->price));
				}
				else
				{
					$tmpPrice = "".$val->obj->price."";
				}
				$price2prod = $val->qty*$tmpPrice;
				$totalprice += $price2prod;
				$totalqty += 1;
			?>
			<tr>
				<td>
					<table width="690" cellpadding="0" cellspacing="0" border="0" >
						<tr>
							<td style = "width:180px;padding-right:10px" >
								<p style = "margin:0;padding:0" ><img src = "<?php if(isset($val->obj->photo)) {print $val->obj->photo;} else {print "http://www.bricodepot.ro/catalog/imagini/".$val->id.".jpg";} ?>"  style = "max-width:180px;max-height:100px" /></p>
							</td>
							<td>
								<p style = "margin:0;padding:0;color:#000000;font-size:11px" ><b><?php print $val->obj->lib;if(isset($val->obj->color)){print " - ".$val->obj->color;}?></b></p>
								
								<p style = "margin:0;padding:0;color:#000000;font-size:11px" ><b><?php print $val->id;?></b></p>
								<p style = "margin:0;padding:0;color:#000000;font-size:11px" >PREŢ: <?php print $val->obj->price. " lei";?></p>
								<p style = "margin:0;padding:0;color:#000000;font-size:11px" >CANTITATE: <?php print $val->qty;?> </p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan = "2" height = "1" >
					<p style ="margin:0;padding:0;clear:both;width:685px;height:1px;max-height:1px;border-top:1px solid #ff0000;margin:10px 0px;padding-bottom:0px" >&nbsp;</p>
				</td>
			</tr>
		<?php }?>
		</table>
		
		<table width = "690"  style = "" >
			<tr><td colspan = "2" ><div style = "height:20px" >&nbsp;</div></td></tr>
			<tr align = "right" >
				<td>
					<table cellpadding = "0" cellspacing = "0" border = "0" >
						<tr>
							<td style = "text-align:right" >
								<p style = "margin:0;padding:0;font-size:11px;color:#000000;font-weight:bold;padding-right:5px" >NUMĂR DE ARTICOLE SELECTATE :</p>
							</td>
							<td class = "greyrow2" style = "background-color:#ff0000;height:26px;width:65px;text-align:center;" height = "24" >
								<p style = "margin:0;padding:0;font-weight:bold;font-size:11px" ><?php print $totalqty; ?></p>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table cellpadding = "0" cellspacing = "0" border = "0" align ="right" >
						<tr>
							<td style = "text-align:right" >
								<p style = "margin:0;padding:0;font-size:11px;color:#000000;font-weight:bold;padding-right:5px" >PREŢ TOTAL :</p>
							</td>
							<td class = "greyrow2" style = "background-color:#ff0000;height:26px;width:100px;text-align:center;"height = "24"  >
								<p style = "margin:0;padding:0;font-weight:bold;font-size:11px" ><?php print number_format($totalprice,2,"."," "); ?> lei</p>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table width = "690"  style = "" >
				<tr>
					<td>
						
						<table cellpadding = "0" cellspacing = "0" border = "0" align ="left" >
							<tr>
								<td style = "text-align:left" >
									<p style = "margin:0;padding:0;font-size:11px;color:#000000;font-weight:bold;padding-right:5px" >Acesta este doar un deviz menit sa va ajute în crearea listei cu produsele dorite. </br>Achizitionarea / rezervarea produselor se poate face doar în depozitele BRICO DEPÔT.</p>
								</td>
								
							</tr>
						</table>
					</td>
				</tr>
			</tr>
		</table>	
		<?php if(isset($xmldata->message)) {?>
		<table width = "690"  style = "" >
			<tr><td><div style = "height:20px" >&nbsp;</div></td></tr>
			<tr align = "left" >
				<td>
					<p>Găsiţi aici un mesaj în atenţia d-voastră:</p>
					<p><i><?php print $xmldata->message; ?></i></p>
				</td>
			</tr>
		</table>
		<?php } ?>
	</div>	
</body>