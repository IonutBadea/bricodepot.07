<nav class="app_nav">
    <div class="col-xs-6 no-padding">
        <!--        <a href="https://www.bricodepot.ro/catalog/">-->
        <!--        <a href="https://www.bricodepot.ro/">-->
        <!-- @todo: link catre home - comentat la livrare -->
        <a href="http://192.168.50.125/www/bricodepot.07/">
            <img src="assets/img/bricodepot_logo.png" class="logo">
            <span id="city" class="city" onclick="showMap()"></span>
        </a>
    </div>
    <ul id="breadcrumbs" class="breadcrumbs text-right hidden-xs no-padding">
        <li><a onclick="shareFacebook();"><img src="assets/img/btn-share.png"/><br/>Distribuie</a></li>
        <li><a onclick="embedWishlist();"><span id="badge" class="ext-module-js" data-module="WishlistBadge" data-option-text="#products" data-option-wishlist_id="wishlist"></span><img src="assets/img/btn-basket.png"/><br/>Listă cumpărături</a></li>
        <li><a onclick="toggleMainMenu();"><img src="assets/img/menu_icon.png" class="menu"/><br/>&nbsp;</a></li>
    </ul>
    <nav class="col-xs-6 navbar visible-xs">
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown text-right"><a onclick="toggleMainMenu();"><img src="assets/img/menu_icon.png" class="menu" style="top: -5px;"/></a>
            </li>
        </ul>
    </nav>
</nav>


<div id="main_menu" class="main_menu">
    <div class="col-xs-12 responsive_options">
        <ul>
            <li><a onclick="shareFacebook();"><img src="assets/img/btn-share.png"/>Distribuie</a></li>
            <li><a onclick="embedWishlist();"><span id="badge_responsive" class="ext-module-js" data-module="WishlistBadge" data-option-text="#products" data-option-wishlist_id="wishlist"></span><img src="assets/img/btn-basket.png"/>Listă cumpărături</a></li>
        </ul>
    </div>

    <!-- project_1 -->
    <div class="col-xs-12 col-md-4">
        <div class="menu_project project_1">
            <div class="menu_project_title">
                Unelte de sezon
            </div>
            <img class="menu_project_image" src="assets/img/HP/ambianta-1.jpg">
            <ul class="menu_project_items">
                <li>
                    <a href="pregateste-ti-gradina-pentru-iarna">Pregătește-ți grădina pentru iarnă</a>
                </li>
                <li>
                    <a href="pregateste-te-de-sezonul-rece">Pregătește-te de sezonul rece</a>
                </li>
            </ul>
        </div>
    </div>

    <!-- project_2 -->
    <div class="col-xs-12 col-md-4">
        <div class="menu_project project_2">
            <div class="menu_project_title">
                Soluții de încălzire potrivite pentru tine
            </div>
            <img class="menu_project_image" src="assets/img/HP/ambianta-2.jpg">
            <ul class="menu_project_items">
                <li>
                    <a href="descopera-solutii-de-incalzire-electrica">Descoperă soluții de încălzire electrică</a>
                </li>
                <li>
                    <a href="alege-incalzirea-pe-lemne">Alege încăzirea pe lemne</a>
                </li>
            </ul>
        </div>
    </div>

    <!-- project_3 -->
    <div class="col-xs-12 col-md-4">
        <div class="menu_project project_3">
            <div class="menu_project_title">
                Sisteme electrice și iluminat
            </div>
            <img class="menu_project_image" src="assets/img/HP/ambianta-3.jpg">
            <ul class="menu_project_items">
                <li>
                    <a href="alege-un-sistem-electric-eficient">Alege un sistem electric eficient</a>
                </li>
                <li>
                    <a href="decoreaza-ti-casa-in-stilul-tau">Decorează-ți casa în stilul tău</a>
                </li>
            </ul>
        </div>
    </div>

</div>