<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a><?php echo $breadcrumbs_category; ?></a></li>
        <li class="active"><span><?php echo $breadcrumbs_subcategory; ?></span></li>
    </ol>
</div>