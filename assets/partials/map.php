<div id="main_map_container">
	<div id="intro_header">
		<img id="intro_header_logo" src="assets/img/logo.png"/>
	</div>

	<div id="intro_title_container">
		<img src="assets/img/intro_title.png" id="intro_title">
	</div>

	<div id="container_harta">
		<div id="harta_magazine">

			<img src="assets/img/intro_map.png" id="harta">

			<div id="map_store_oradea"    class="map_store" style="left: 18%; top: 19%;"></div>
			<div id="map_store_arad"      class="map_store" style="left: 13%; top: 33%;"></div>
			<div id="map_store_brasov"    class="map_store" style="left: 49%; top: 44%;"></div>
			<div id="map_store_ploiesti"  class="map_store" style="left: 57%; top: 58%;"></div>
			<div id="map_store_pitesti"   class="map_store" style="left: 45%; top: 63%;"></div>
			<div id="map_store_bucuresti" class="map_store" style="left: 59%; top: 71%;"></div>

			<div id="map_bucuresti_popup">
				<div id="map_store_militari"   class="popup_store" style="border-bottom: 1px solid #a91a0d;">Militari</div>
				<div id="map_store_orhideea"   class="popup_store" style="border-bottom: 1px solid #a91a0d;">Orhideea</div>
				<div id="map_store_pantelimon" class="popup_store" style="border-bottom: 1px solid #a91a0d;">Pantelimon</div>
				<div id="map_store_baneasa"    class="popup_store" style="border-bottom: 1px solid #cd2210;">Baneasa</div>
				<img id="popup_arrow" src="assets/img/red_arrow_bottom.png"/>
			</div>
			
			<div id="map_store_constanta"   class="map_store" style="left: 83%; top: 77%;"></div>
			<div id="map_store_braila"   class="map_store" style="left: 77%; top: 57%;"></div>
			<div id="map_store_focsani"   class="map_store" style="left: 68%; top: 47%;"></div>
			<div id="map_store_suceava"   class="map_store" style="left: 56%; top: 10%;"></div>
			<div id="map_store_drobeta"   class="map_store" style="left: 23%; top: 71%;"></div>
			<div id="map_store_calarasi"   class="map_store" style="left: 72%; top: 77%;"></div>
			
		</div>
	</div>
</div>