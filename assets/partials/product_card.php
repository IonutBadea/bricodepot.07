<div class="project_description produs <?php echo $product_class; ?>">

    <?php echo $product_image_sticker; ?>

    <div class="project_description_header">
        <div class="project_description_title <?php echo $product_class_sticker; ?>">
            <?php echo $product_title; ?>
        </div>
    </div>

    <div class="project_description_image col-xs-12 text-center">
        <a href="javascript:nop()" class="noZensmooth" onclick='<?php

        if (!$product_alternative_checked) {
            echo "embedProduct(\"" . $product_ref . "\"," . json_encode($product_price) . ")";
        } else {
            echo "embedProducts(\"" . $product_alternative . "\"," . json_encode($product_price) . ")";
        }

        ?>'>
            <img class="map__image img-responsive" src="<?php echo thumbImageProduct($product_ref); ?>">
        </a>
    </div>

    <?php if (!empty($product_addontext)) : ?>
        <div class="addontext">
            <?php echo $product_addontext ?>
        </div>
    <?php endif; ?>

</div>


<?php echo $product_image_badge; ?>
