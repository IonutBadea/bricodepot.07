<?php

/*
product_card((object)[
    'title' => 'LAMPĂ SOLARĂ BALL LED',
    'ref' => '140842',
    'sticker' => 'sticker_recomandam.png'
]);
*/

function product_card($object)
{
    $title = isset($object['title']) ? $object['title'] : '';
    $ref = isset($object['ref']) ? $object['ref'] : '';
    $image = isset($object['image']) ? $object['image'] : $ref;
    $class = isset($object['class']) ? $object['class'] : '';
    $sticker = isset($object['sticker']) ? $object['sticker'] : '';
    $addontext = isset($object['addontext']) ? trim($object['addontext']) : '';
    $alternative = isset($object['alternative']) ? $object['alternative'] : array();
    $badge =  isset($object['badge']) ? $object['badge'] : '';
    $price =  isset($object['price']) ? $object['price'] : array();

    $product_title = $title;
    $product_ref = $ref;
    $product_image = $image;
    $product_class = $class;
    $product_image_sticker = !empty($sticker)
        ? '<img class="sticker" src="assets/img/' . $sticker . '">'
        : '';
    $product_class_sticker = !empty($sticker)
        ? 'with_sticker'
        : '';
    $product_addontext = $addontext;
    $product_image_badge = !empty($badge)
        ? '<img class="badge_product" src="assets/img/' . $badge . '">'
        : '';
    $product_alternative_checked = false;
    $product_alternative = "";
    foreach ($alternative as $key => $value) {
        $product_alternative .= ',' . $value;
        $product_alternative_checked = true;
    }
    $product_alternative = $ref . $product_alternative;
    $product_price = array(
        "badge" =>  $badge ,
        "old_price" =>  isset($price['old_price']) ? $price['old_price'] : '' ,
        "new_price" => isset($price['new_price']) ? $price['new_price'] : '',
        "unit" => isset($price['unit']) ? $price['unit'] : 'buc',
        "currency" => isset($price['currency']) ? $price['currency'] : 'lei',
    );

    require('product_card.php');
}


/*
breadcrumb((object)[
    'category' => '',
    'subcategory' => ''
]);
*/
function breadcrumb($object)
{

    $category = isset($object->category) ? $object->category : '';
    $subcategory = isset($object->subcategory) ? $object->subcategory : '';

    $breadcrumbs_category = $category;
    $breadcrumbs_subcategory = $subcategory;

    require('breadcrumbs.php');
}

/*
 * optimizedImageProduct(string_ref)
 */
function optimizedImageProduct($ref)
{
    $src = 'assets/products/images/images_optimized/' . $ref . '.jpg';
    return $src;
}

/*
 * thumbImageProduct(string)
 */
function thumbImageProduct($ref)
{
    $src = 'assets/products/images/images_thumb/' . $ref . '.jpg';
    return $src;
}
