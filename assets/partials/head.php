<?php
    require_once('assets/partials/functions.php');
?>

<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<title><?php echo $title; ?> - Bricodepot</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assets/css/style.css<?php echo "?r=".date("U") ?>">
<link rel="stylesheet" href="assets/css/pages.css">
<link rel="stylesheet" href="assets/css/map.css">
<link rel="stylesheet" href="assets/css/blocks.css">
<link rel="stylesheet" href="assets/css/popup.css">
<link rel="stylesheet" href="assets/css/product.css">
<link rel="stylesheet" href="assets/css/wishlist.css<?php echo "?r=".date("U") ?>">
<!--<link rel="stylesheet" href="assets/css/menu.css">-->
<link rel="stylesheet" href="assets/css/store_map.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700" rel="stylesheet">

<link rel="stylesheet" href="assets/css/lib/slick-carousel/slick.css">
<link rel="stylesheet" href="assets/css/lib/slick-carousel/slick-theme.css">
<link rel="stylesheet" href="assets/css/slider-text-ambianta.css">
<link rel="stylesheet" href="assets/css/youtube.css">

<link rel="stylesheet" href="assets/css/optim_desktop.css">
<link rel="stylesheet" href="assets/css/optim_768.css">
<link rel="stylesheet" href="assets/css/optim_480.css">


<link rel="stylesheet" href="assets/css/all_styles.css<?php echo "?r=".date("U") ?>">



<script src='https://www.google.com/recaptcha/api.js'></script>
