<script type="text/x-template" id="options">
    {
    "target": {
    },
    "data": {
    },
    "details": {
    "static": "true",
    "block_settings": {
    "ratio": 1,
    "items_per_line": 4,
    "responsive": {
    "768": {
    "items_per_line": 2
    },
    "400": {
    "items_per_line": 2
    }
    }
    }
    }
    }
</script>
<!-- template produs -->
<script type="text/x-template" id="product_template">
    <div class="product_container">
        <h2>#{lib}</h2>
        <div class="col-xs-12" style="padding-top: 10px;">
            <div class="col-xs-12 col-md-6">
                <p class="code"><label>Cod Brico:</label> #{ref}</p>
                <div class="image">
                    { if #{hasBadge} } <img src="#{options.badge}" class="badge_product"/> { endif }
                    <img src="#{photo}">
                </div>
                <p class="prices_date">#{prices_date}</p>
                { if #{has_pdf} }<a href="#{pdf} class=" pdf">Descarcă manual PDF</a>{ endif }
            </div>
            <div class="col-xs-12 col-md-6" style="padding: 0;">
                <p class="desc">
                    <label>Descriere</label>
                    <br>
                    #{desc}
                </p>

                <div class="halfs">

                    <div class="half">
                        { if #{hasBadge} }
                        <p class="price" style="line-height:21px;"><span style="text-decoration: line-through;">#{options.old_price}</span> Lei / #{options.unit}</p>
                        <p class="second_price price" style="line-height:21px;">#{price} Lei / #{unit}</p>
                        { endif }

                        { if #{hasNotBadge_and_hasSecondPrice} }
                        <p class="price" style="line-height:21px;">#{price} Lei / #{unit}</p>
                        <p class="second_price price" style="line-height:21px;">#{packagePrice} Lei / #{packageUnit}</p>
                        { endif }

                        { if #{hasNotBadge_and_hasNotSecondPrice} }
                        <p class="price">#{price} Lei / #{unit}</p>
                        { endif }
                    </div>

                    <div class="half">
                        <div class="quantity">
                            <button class="minus" onclick="updateQuantity('quantity', -1)">-</button>
                            <input id="quantity" type="number" value="1" onchange="if (this.value < 1) this.value = 1">
                            <button class="plus" onclick="updateQuantity('quantity', 1)">+</button>
                        </div>
                    </div>
                </div>

                <button class="red" onclick="addProduct('#{id}')"><img src="assets/img/epsAddButtonIcon.png">Adaugă la lista de cumpărături</button>
                <a class="red" href="#{url}" target="_blank"><img src="assets/img/epsUrlButtonIcon.png">Consultă produsul pe site</a>
            </div>
        </div>
        <button class="close_popup" onclick="closeEmbedProduct()">&times;</button>
    </div>
</script>

<!-- template produse multi -->
<script type="text/x-template" id="product_multi_template">
    <div class="product_container multi">
        <h2 class="lib">#{lib}</h2>
        <div class="col-xs-12" style="padding-top: 10px;">
            <div class="col-xs-12 col-md-6">
                <p class="code"><label>Cod Brico:</label> <span class="ref">#{ref}</span></p>
                <div class="image">
                    { if #{hasBadge} } <img src="#{options.badge}" class="badge_product"/> { endif }
                    <img src="#{photo}" class="product_photo">
                </div>
                <p class="prices_date">#{prices_date}</p>
                <a href="#{pdf}" class="pdf { if #{has_pdf} }visible{ endif }">Descarcă manual PDF</a>
            </div>
            <div class="col-xs-12 col-md-6" style="padding: 0;">
                <p class="desc">
                    <label>Descriere</label>
                    <br>
                    <span class="desc_content">#{desc}</span>
                </p>

                <div class="halfs">

                    <div class="half">
                        { if #{hasBadge} }
                        <p class="price" style="line-height:21px;"><span style="text-decoration: line-through;">#{options.old_price}</span> Lei / #{options.unit}</p>
                        <p class="second_price price" style="line-height:21px;">#{price} Lei / #{unit}</p>
                        { endif }

                        { if #{hasNotBadge_and_hasSecondPrice} }
                        <p class="price" style="line-height:21px;">#{price} Lei / #{unit}</p>
                        <p class="second_price price" style="line-height:21px;">#{packagePrice} Lei / #{packageUnit}</p>
                        { endif }

                        { if #{hasNotBadge_and_hasNotSecondPrice} }
                        <p class="price">#{price} Lei / #{unit}</p>
                        { endif }
                    </div>

                    <div class="half">
                        <div class="quantity">
                            <button class="minus" onclick="updateQuantity('quantity', -1)">-</button>
                            <input id="quantity" type="number" value="1" onchange="if (this.value < 1) this.value = 1">
                            <button class="plus" onclick="updateQuantity('quantity', 1)">+</button>
                        </div>
                    </div>
                </div>

                <button class="red add_product" onclick="addProduct('#{id}')"><img src="assets/img/epsAddButtonIcon.png">Adaugă la lista de cumpărături</button>
                <a class="red visit_product" href="#{url}" target="_blank"><img src="assets/img/epsUrlButtonIcon.png">Consultă produsul pe site</a>
            </div>
            <div class="col-xs-12 col-md-12" style="position: relative;">
                <div class="multi_product_wrapper">
                    { repeat product in other_products }
                    <div class="col-xs-12 col-md-4 multi_product" onclick="updateProduct('#{product.id}')">
                        <img src="#{product.photo}">
                        <p>#{product.lib}</p>
                    </div>
                    { endrepeat }
                </div>
                { if #{should_scroll_other_products} }
                <button class="scroll_left" onclick="scrollMultiLeft()" style="opacity: 0.5;"></button>
                <button class="scroll_right" onclick="scrollMultiRight()"></button>
                { endif }
            </div>
        </div>
        <button class="close_popup" onclick="closeEmbedProduct()">&times;</button>
    </div>
</script>

<!-- template wishlist -->
<script type="text/x-template" id="embed_wishlist_template">
    <div id="wishlist_container" class="wishlist_container">
        <h2><img src="assets/img/epsAddButtonIcon.png"> Lista de cumpărături</h2>
        <div class="wrapper">
            <div class="wrapper_header">
                <div class="col-xs-12 col-md-3 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
                    Produs
                </div>
                <div class="hidden-xs col-md-4 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
                    Descriere
                </div>
                <div class="hidden-xs col-md-2 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
                    Preț
                </div>
                <div class="hidden-xs col-md-3 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
                    Cantitate
                </div>
            </div>
            <div class="wrapper_overflow">
                <div id="wishlist" class="ext-module-js" data-module="template" data-options-id="wishlist_options"></div>
            </div>
            <div class="wrapper_footer">
                <div class="total">
                    <span id="wishlist_total" class="ext-module-js wishlist-total" data-module="WishlistBadge"></span>
                </div>
            </div>
        </div>
        <div class="footer">
            <div>
                <button class="remove_all" onclick="clearWishlist()"><img src="assets/img/epsClearButtonIcon.png"> Elimină toate produsele</button>

                <button class="send_wishlist" onclick="openSendWishlist()">Trimite lista prin email</button>
                <button class="print_wishlist" onclick="printWishlist()">Printează lista</button>
            </div>
            <p class="disclaimer">
                Acesta este doar un deviz menit să vă ajute în crearea listei cu produsele dorite.<br>
                Achiziţionarea / rezervarea produselor se poate face doar în depozitele BRICO DEPÔT.
            </p>
        </div>
        <button class="close_popup" onclick="closeEmbedProduct()">&times;</button>
    </div>
</script>

<script type="text/x-template" id="wishlist_template">
    <div class="wishlist_item" id="wishlist_product_#{ref}">
        <div class="col-xs-12 col-md-3 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
            #{lib}
            <br/>
            <span class="ref">##{ref}</span>
        </div>
        <div class="hidden-xs col-md-4 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
            <div class="desc">
                #{desc}
            </div>
        </div>
        <div class="flex_vertical_center col-xs-6 col-md-2 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
            <p class="price">#{pretty_price} <span>lei/#{unit}</span></p>
            { if #{has_different_prices} }
            <p class="secondary_price">#{pretty_package_price} <span>lei/#{package_unit}</span></p>
            { endif }
        </div>
        <div class="flex_vertical_center col-xs-6 col-md-3 no-padding" style="padding-left: 5px !important; padding-right: 5px !important;">
            <div>
                <div class="quantity">
                    <button class="minus" onclick="updateWishlistQuantity('#{ref}', 'quantity_#{ref}', -1)">-</button>
                    <input id="quantity_#{ref}" type="number" value="#{_quantity}" onchange="if (this.value < 1) this.value = 1">
                    <button class="plus" onclick="updateWishlistQuantity('#{ref}', 'quantity_#{ref}', 1)">+</button>
                </div>
                <button class="delete" onclick="removeProduct('#{ref}');"></button>
            </div>
        </div>
    </div>
</script>

<!-- template popup -->
<script type="text/x-template" id="send_wishlist_template">
    <div id="send_wishlist_container" class="send_wishlist_container">
        <h2><img src="assets/img/mailTitleIcon.png"> Trimite lista de cumpărături pe email</h2>
        <div class="wrapper">
            <input type="text" id="ldc_firstName" value="" placeholder="Prenumele dumneavoastră* :">
            <input type="text" id="ldc_lastName" value="" placeholder="Numele dumneavoastră* :">
            <input type="text" id="ldc_to" value="" placeholder="Adresa de email unde se trimite lista* :">
            <textarea id="ldc_message" placeholder="Comentarii"></textarea>

            <div id="send_wishlist_recaptcha" class="g-recaptcha" data-sitekey="6LewJksUAAAAAKaHp1kT64iA4Z8ID1x5tgWqhZ6r"></div>

            <button class="send" onclick="sendWishlist()">Trimite</button>
        </div>
        <button class="close_popup" onclick="closeSendWishlist()">&times;</button>
    </div>
</script>

<!-- template popup -->
<script type="text/x-template" id="embed_popup_template">
    <div id="popup_container" class="popup_container">
        <div class="wrapper">
            <p id="popup_text"></p>
            <button onclick="closeEmbedPopup()">Ok</button>
        </div>
    </div>
</script>