<script src="assets/js/lib/jquery/jquery.min.js"></script>
<script src="assets/js/lib/slick-carousel/slick.min.js"></script>

<script src="assets/ext/lib/axios.min.js"></script>
<script src="assets/ext/lib/zenscroll.min.js"></script>
<script src="assets/ext/dev/core/ExtCore.js"></script>
<script src="assets/ext/dev/map/ExtMap.js"></script>
<script src="assets/ext/dev/template/ExtTemplate.js"></script>
<script src="assets/ext/dev/blocks_grid/ExtBlocksGrid.js"></script>
<script src="assets/ext/dev/wishlist/ExtWishlist.js"></script>
<script src="assets/ext/dev/wishlist/ExtWishlistBadge.js"></script>
<script src="assets/js/store.js"></script>
<script src="assets/js/tracker.js"></script>
<script src="assets/js/products.js"></script>
<script src="assets/js/menu.js"></script>
<script src="assets/js/hotspots.js"></script>
<script src="assets/js/map.js"></script>
<script src="assets/js/app.js"></script>