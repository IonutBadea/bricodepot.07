<?php  
	
	ini_set('max_execution_time', 0);
	// ini_set('memory_limit', '-1');

	function dd($value) {
		die(var_dump($value));
	}
	function trace($val) {
		echo "<pre>";
			var_dump($val);
		echo "</pre>";
	}


	class Parser
	{

		function __construct($foo = null)
		{
			$this->missingRefs = array();
			$this->csvContent = file_get_contents('in/base.csv');
			$this->reprocessRefs = array();
			//$this->reprocessRefs = array();
		}

		

		public function getProductDetails($html,$save=true,$sku=false,$photo='',$url=false)
		{
			if (!isset($sku)) {
				return;
			}
			
			$doc = new DOMDocument();
			libxml_use_internal_errors(true);
			$doc->loadHTML($html);
			libxml_use_internal_errors(false);
			
			$xpath = new DOMXPath($doc);
			$title = $xpath->query("//div[@class='col-md-9 product_title']/h1");//title
			
			if($title->length === 0) {
				if(isset($_GET['sku'])) {
					//header('Content-type: application/xml');
					//print file_get_contents('error.xml');
					header("Status: 404 Not Found");
					header("HTTP/1.0 404 Not Found");
					echo "<h1>Error ".$_GET['sku']." </h1>";
					return http_response_code(404);
					die();
				}
				// $this->missingRefs[] = $sku;
				return;
			}
			
			//title
			$title = $title->item(0)->nodeValue;
			
			//description
			// $description = $xpath->query("//div[@class='details']");
			preg_match_all("/\<div class=\"details\"\>(.*?)\<\/div\>/us", $html, $description);//description
			$description = $description[1][0];
			
			$description = str_replace("</p><p>", "\n", $description);
			$description = str_replace("<br />", "", $description);
			$description = strip_tags($description);//imi scoate absolut toate tagurile
			
			$description = str_replace("&nbsp;&nbsp;", " ", $description);
			$description = str_replace("&nbsp;", " ", $description);
			$description = str_replace("\t", "", $description);
			$description = str_replace("\n\n", "", $description);
			$description = str_replace("\n\r", "", $description);
			
			//pdf
			$pdf = $xpath->query("//p[@class='productLongDesc']/a");
			$pdfString = '';
			if($pdf->length)
			{
				$pdfUrl = "https://www.bricodepot.ro".($pdf->item(0)->getAttribute('href'));
				$pdfString = "\n\t<pdf><![CDATA[{$pdfUrl}]]></pdf>";
			}

			//photos
			$photos = $xpath->query("//div[@class='product_image']/a");//images
			$photoString ='';
			$thumbsString ='';
			$thumb = '';
			if($photos->length > 1) {
				foreach ($photos as $key => $photoTag) {
					$photoUrl = $photoTag->getAttribute('href');
					$thumbImg = $photoTag->getElementsByTagName('img')->item(0);
					$thumbUrl = $thumbImg->getAttribute('src');
					
					$photoString .= "\n\t\t<photo><![CDATA[{$photoUrl}]]></photo>";
					$thumbsString .= "\n\t\t<thumb><![CDATA[{$thumbUrl}]]></thumb>";
					if ($photo === "") {
						$photo = $photoUrl;
						$thumb = $thumbUrl;
					}
				}
				$photoString = "<photos>{$photoString}\n\t</photos>";
				$thumbsString = "<thumbs>{$thumbsString}\n\t</thumbs>";
			} else {
				if ($photo === "") {
					foreach ($photos as $key => $photoTag) {
						$photo = $photoTag->getAttribute('href');
						$thumbImg = $photoTag->getElementsByTagName('img')->item(0);
						$thumb = $thumbImg->getAttribute('src');
						break;
					}
				}
			}
			trace($photo);
			$downloadPhotoName = $sku.".jpg";
			
			$json = $xpath->query("//script[@type='application/ld+json']");
			$json = json_decode($json->item(1)->nodeValue,true);
			
			$newXml = file_get_contents('product_template.xml');
			$newXml = str_replace(
				array(
					'##ID##',
					'##TITLE##',
					'##DESCRIPTION##',
					'##PHOTO##',
					'##THUMB##',
					'##URL##',
					'##PHOTOS##',
					'##THUMBS##',
					'##PDF##'
				), 
				array(
					$sku ? $sku : $json['sku'],
					trim(utf8_decode($title)),
					//trim($title),
					trim($description),
					$photo ? $photo : $json['image'],
					$thumb ? $thumb : $json['image'],
					$url ? $url : $json['url'],
					$photoString,
					$thumbsString,
					$pdfString
				), 
				$newXml
			);
			
			
				$open = fopen('xmls/'.($sku ? $sku : $json['sku']).".xml",'wr');
				fwrite($open, $newXml);
				fclose($open);

				
			if (!$save) 
			{
				header('Content-type: application/xml');
				print $newXml;
				die;
			}		
			
		}

		protected function getCsvAsArray($getAll=false)
		{
			$refs = array();

			$csvFile = fopen('in/base.csv', 'r');
			$k=0;
			while (($line = fgetcsv($csvFile, 1000, ";")) !== FALSE) {

				if($k > 0 && count($line) == 2 && trim($line[0]))
				{
					/*if (isset($refs[trim($line[0])])) {
						trace(trim($line[0])." exista deja");
					}*/
					$sku = trim($line[0]);
					$url = trim($line[1]);
					if (strlen($sku) > 0) {
						$returnFromFile = file_exists('xmls/'.$sku.".xml");
						
						if (array_search($sku, $this->reprocessRefs) !== false) {
							$returnFromFile = false;
						}
						
						$refs[$sku] = array(
							'photo' => '',
							'sku' => $sku,
							'url' => $url,
							'exists' => $returnFromFile
						  );
					}
				}
				$k++;
			}
			return $refs;
		}
		
		//all csv
		public function getDetails()
		{
			$csvArray = $this->getCsvAsArray();
			
			$filtered = array_filter($csvArray, function($item){
				return !$item['exists'];
			});
			
			
			$chunked = array_chunk($filtered, 100);
			
			$this->loadProductUrls($chunked);
			
		}
		protected function loadProductUrls($products)
		{
			if (count($products) == 0) {
				$this->createProductsXml();
				$this->downloadImages();
			} else {
				$currentProducts = array_shift($products);
				
				$multiCurl = array();
				$result = array();

				$mh = curl_multi_init();
				foreach ($currentProducts as $i => $url) 
				{

					if(!$url['exists'])
					{
						$url = trim($url['url']);

						$multiCurl[$i] = curl_init();
						curl_setopt($multiCurl[$i], CURLOPT_URL,$url);
						curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
						curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
						curl_setopt($multiCurl[$i], CURLOPT_SSL_VERIFYPEER, false);  
						curl_setopt($multiCurl[$i], CURLOPT_SSL_VERIFYHOST, false);  
						curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, true); 
						curl_setopt($multiCurl[$i], CURLOPT_CONNECTTIMEOUT, 0.1);
						curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT, 0.4);

						curl_multi_add_handle($mh, $multiCurl[$i]);
					}
				}

				$index=null;
				do {
					curl_multi_exec($mh,$index);
				} while($index > 0);
				
				
				// get content and remove handles
				foreach($multiCurl as $k => $ch) {
				  $result[$k] = curl_multi_getcontent($ch);

				  curl_multi_remove_handle($mh, $ch);
				}
				// close
				curl_multi_close($mh);	
				
				foreach ($result as $rkey => $body) 
				{
					if(!$body)
					{
						$this->missingRefs[] = $currentProducts[$rkey]['sku']." - ".$currentProducts[$rkey]['url'];
					}
					else
					{
						$html = 'downloads/'.trim(pathinfo($currentProducts[$rkey]['url'],PATHINFO_BASENAME));

						if (!trim(pathinfo($currentProducts[$rkey]['url'],PATHINFO_BASENAME))) {
							print_r($currentProducts[$rkey]);
							die;
						}

						if($open = fopen($html,'wr'))
						{
							$body = str_replace('<head>', '<head><base href="https://www.bricodepot.ro" target="_blank">', $body);
							fwrite($open, $body);
							fclose($open);

							$this->getProductDetails($body,true,$currentProducts[$rkey]['sku'],$currentProducts[$rkey]['photo'],$currentProducts[$rkey]['url']);
						}
					}
				}
				unset($multiCurl);
				unset($result);
				unset($mh);
				
				sleep(2);
				
				$this->loadProductUrls($products);
			}
		}
		public function createProductsXml()
		{
			$csvArray = $this->getCsvAsArray();



			$allProducts = '';
			
			foreach ($csvArray as $i => $url) {
				if(file_exists('xmls/'.$url['sku'].".xml"))	{
					$allProducts .= file_get_contents('xmls/'.$url['sku'].".xml")."\n";
				}	else	{
					$this->missingRefs[] = $url['sku']." - ".$url['url'];
				}				
			}

			$opened = fopen('misingRefs.txt','wr');
			fwrite($opened, implode("\n", $this->missingRefs));
			fclose($opened);

			if(count($this->missingRefs))	{
				// header("Refresh:0");
			}


print_r($allProducts)			;
print_r($this->missingRefs)			;
			//header('Content-type: application/xml');
			$all = "<products>\n{$allProducts}\n</products>";

			$open = fopen('xmls/products.xml','wr');

			fwrite($open, $all);
			fclose($open);
			
			//print $all;
			print "FIN products.xml";
		}
		
		//single sku
		public function getDetailsBySKU($sku)
		{
			$create = false;
			if (isset($_GET['reload'])) {
				$create = true;
			}
			if(file_exists('xmls/'.$sku.".xml") && $create == false)
			{
				header('Content-type: application/xml');
				print file_get_contents('xmls/'.$sku.".xml");

				die;
			}

			$this->getCsvLine($sku);
		}
		protected function getCsvLine($sku)
		{
			//preg_match("/;{$sku}\;.*\;(.*)\;(.*)/", $this->csvContent, $csvLineArray);
			preg_match("/{$sku};(.*)/", $this->csvContent, $csvLineArray);
			
			if (isset($csvLineArray[1]) === false) {
				echo $sku." missing in csv";
				header('X-PHP-Response-Code: 404', true, 404);
				
				/*
				$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, 'https://www.bricodepot.ro/militari/cauta?Query='.$sku);
					curl_setopt($ch, CURLOPT_HEADER,0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0.1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 0.4);
			 	$html = curl_exec($ch);
				curl_close($ch);
				
				//preg_match_all("/\<a href=\"(.*?)\" class=\"product_name\"\>/us", $html, $url);//url
				
				$doc = new DOMDocument();
				libxml_use_internal_errors(true);
				$doc->loadHTML($html);
				libxml_use_internal_errors(false);

				$xpath = new DOMXPath($doc);
				$urls = $xpath->query("//a[@class='product_name']");
				
				$productUrl = 'https://www.bricodepot.ro/'.$urls->item(0)->getAttribute('href');
				
				echo "</br>".$productUrl;
				*/
				die();
			}
			$csvLineArray[1] = trim($csvLineArray[1]);
			
			$html = $this->getUrlContentSingle($sku, $csvLineArray[1]);
			
			$this->getProductDetails($html,false,$sku,'',$csvLineArray[1]);
		}
		public function getUrlContentSingle($sku, $url)
		{
			
			$ch = curl_init(trim($url));
				curl_setopt($ch, CURLOPT_HEADER,0);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); 
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0.1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 0.4);
			$body = curl_exec ($ch) or die(curl_error($ch)); 
			
			if(!$body) {
				$this->missingRefs[] = $sku." - ".$url;
			}
			else
			{
				$html = 'downloads/'.trim(pathinfo($url,PATHINFO_BASENAME));

				if (!trim(pathinfo($url,PATHINFO_BASENAME))) {
					print_r($url);
					die;
				}

				if($open = fopen($html,'wr'))
				{
					$body = str_replace('<head>', '<head><base href="https://www.bricodepot.ro" target="_blank">', $body);
					fwrite($open, $body);
					fclose($open);

					$this->getProductDetails($body,true,$sku,'',$url);
				}
			}
			
			return $body;
		}
		
		//recreate from previously downloaded files
		public function recreateFilesFromLocalHtml() {
			$csvArray = $this->getCsvAsArray();
			foreach ($csvArray as $i => $product) {
				$htmlUrl = 'downloads/'.trim(pathinfo($product['url'],PATHINFO_BASENAME));
				if (file_exists($htmlUrl) === true) {
					$htmlFile = file_get_contents($htmlUrl);
					$this->getProductDetails($htmlFile,true,$product['sku'],'',$product['url']);
				}
			}
		}
		//check if there are any tables in the description
		public function checkFilesForTables() {
			$xmlFiles = $this->getXmlFiles("xmls/");
			$result = "";
			foreach ($xmlFiles as $key => $xml) {
				$doc = new DOMDocument();
				$doc->load("xmls/".$xml);
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query("//product/desc");
			
				$description = $entries->item(0)->nodeValue;
				if (strpos($description, "<table") != false || strpos($description, "style") != false) {
					if (strpos($description, "<table") != false) {
						$result .= $xml." table \n";
					}
					if (strpos($description, "style") != false) {
						$result .= $xml." style \n";
					}
				}
			}
			$opened = fopen('tableInFiles.txt','wr');
			fwrite($opened, $result);
			fclose($opened);
			
			echo "FIN";
		}
		
		//check maps
		public function checkMapsInCsv() {
			$csvArray = $this->getCsvAsArray();
			$xmlFiles = $this->getXmlFiles("../../desktop/data/");
			foreach ($xmlFiles as $key => $xml) {
				if (strpos($xml, "_") !== false) {
					continue;
				}
				
				$doc = new DOMDocument();
				$doc->load("../../desktop/data/".$xml);
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query("//page/ref[@type='".'ldc'."']");
				foreach ($entries as $key => $entry) {
					$urlValue = $entry->getElementsByTagName('url')->item(0)->nodeValue;
					if (isset($csvArray[$urlValue]) === false) {
						echo "</br>eroare! referinta ".$urlValue." din fisierul ".$xml." e pusa in map dar nu e in csv-ul clientei";
					}
				}
			}
			echo "FIN";
		}
		public function checkMapsInProducts() {
			$doc = new DOMDocument();
			$doc->load("xmls/products.xml");
			$xpath = new DOMXPath($doc);
			$entries = $xpath->query("//products/product");
			
			$products = array();
			foreach ($entries as $key => $entry) {
				array_push($products, $entry->getAttribute("id"));
			}
			
			$xmlFiles = $this->getXmlFiles("../../desktop/data/");
			foreach ($xmlFiles as $key => $xml) {
				if (strpos($xml, "_") !== false) {
					continue;
				}
				
				$doc = new DOMDocument();
				$doc->load("../../desktop/data/".$xml);
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query("//page/ref[@type='".'ldc'."']");
				foreach ($entries as $key => $entry) {
					$urlValue = $entry->getElementsByTagName('url')->item(0)->nodeValue;
					$index = array_search($urlValue, $products);
					if ($index === false) {
						echo "</br>eroare! referinta  ".$urlValue." din ".$xml." nu exista in products.xml";
					}
				}
			}
			
			echo "FIN";
		}
		public function checkMapsInFiles() {
			$xmlFiles = $this->getXmlFiles("../../desktop/data/");
			$allRefs = "";
			foreach ($xmlFiles as $key => $xml) {
				if (strpos($xml, "_") !== false) {
					continue;
				}
				
				$doc = new DOMDocument();
				$doc->load("../../desktop/data/".$xml);
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query("//page/ref[@type='".'ldc'."']");
				foreach ($entries as $key => $entry) {
					$urlValue = $entry->getElementsByTagName('url')->item(0)->nodeValue;
					$allRefs.= $urlValue."</br>";
					
					if (file_exists('xmls/'.(trim($urlValue)).".xml") === false) {
						echo "</br>eroare nu exista fisierul xml pentru - ".$urlValue." - ".$xml;
					}
				}
			}
			echo "</br>";
			echo "</br>all refs";
			echo $allRefs;
		}
		protected function getXmlFiles($folder) {
			$files = scandir($folder);
			$files = array_diff($files, array(".",".."));
			array_filter($files,function($file) use ($folder){
				return pathinfo($folder."/".$file, PATHINFO_EXTENSION) == 'xml';
			});
			return $files;
		}
		public function downloadImages() {
			$this->imgErrors = array();
			
			$xmlFiles = $this->getXmlFiles("xmls");
			$allPhotos = array();
			foreach ($xmlFiles as $index => $xmlFileName) {
				if ($xmlFileName == "products.xml") {
					continue;
				}
				$doc = new DOMDocument();
				$doc->load("xmls/".$xmlFileName);
				$xpath = new DOMXPath($doc);
				$entries = $xpath->query("//photo");
				
				$productId = str_replace(".xml","",$xmlFileName);
				if ($entries->length > 1) {
					for ($index=1; $index < $entries->length; $index++) {
						$suffix = "";
						if ($index > 1) {
							$suffix = "_".($index-1);
						}
						array_push($allPhotos, array("productId"=>$productId, "fileName"=>$productId.$suffix.".jpg", "photoUrl"=>$entries->item($index)->nodeValue));
					}
				} else if ($entries->length == 1) {
					array_push($allPhotos, array("productId"=>$productId, "fileName"=>$productId.".jpg", "photoUrl"=>$entries->item(0)->nodeValue));
				}
			}
			
			$chunked = array_chunk($allPhotos, 100);
			
			$this->downloadImagesFiles($chunked);
			
		}
		protected function downloadImagesFiles($products) {
			if (count($products) == 0) {
				trace("--FIN images--");
				
				$errorsFile = fopen("errors_img.txt", 'wr');
				foreach ($this->imgErrors as $k=>$error) {
					echo "</br>".$error;
					fwrite($errorsFile, $error."\n");
				}
				fclose($errorsFile);
				
			} else {
				$currentProducts = array_shift($products);
				
				$multiCurl = array();
				$result = array();
				
				$mh = curl_multi_init();
				foreach ($currentProducts as $i => $item) {
					$urlLink = $item["photoUrl"];
					$fileName = "images/downloads/".$item["fileName"];
					if ((file_exists($fileName)) === false) {
						trace("downloading");
						trace($fileName." - ".$urlLink);
						
						$multiCurl[$i] = curl_init();
							curl_setopt($multiCurl[$i], CURLOPT_URL,$urlLink);
							curl_setopt($multiCurl[$i], CURLOPT_HEADER,0);
							curl_setopt($multiCurl[$i], CURLOPT_RETURNTRANSFER,1);
							curl_setopt($multiCurl[$i], CURLOPT_SSL_VERIFYPEER, false);  
							curl_setopt($multiCurl[$i], CURLOPT_SSL_VERIFYHOST, false);  
							curl_setopt($multiCurl[$i], CURLOPT_FOLLOWLOCATION, true); 
							curl_setopt($multiCurl[$i], CURLOPT_CONNECTTIMEOUT, 0.1);
							curl_setopt($multiCurl[$i], CURLOPT_TIMEOUT, 0.4);

						curl_multi_add_handle($mh, $multiCurl[$i]);
					}
				}
				$index=null;
				do {
					curl_multi_exec($mh,$index);
				} while($index > 0);
				
				
				// get content and remove handles
				foreach($multiCurl as $k => $ch) {
				  $result[$k] = curl_multi_getcontent($ch);
				  curl_multi_remove_handle($mh, $ch);
				}
				// close
				curl_multi_close($mh);	
				
				foreach ($result as $rkey => $body) {
					$item = $currentProducts[$rkey];
					
					$urlLink = $item["photoUrl"];
					$fileName = "images/downloads/".$item["fileName"];
					
					if(!$body || strlen($body) < 1000) {
						array_push($this->imgErrors, "Error - ".$urlLink);
					} else {
						if($open = fopen($fileName,'wr')) {
							fwrite($open, $body);
							fclose($open);
						}
					}
				}
				unset($multiCurl);
				unset($result);
				unset($mh);
				
				sleep(2);
				
				$this->downloadImagesFiles($products);
			}
		}
		public function optimizeImages() {
			$filesToProcess = $this->getFilesFromDir("images/downloads/");
			
			if (count($filesToProcess)>0) {
				foreach ($filesToProcess as $k=>$fileName) {
					$currentFileName = $fileName;
					if ((file_exists("images/images_optimized/".$currentFileName)) === false) {
						$this->compressImage("images/downloads/".$currentFileName, "images/images_optimized/".$currentFileName, 90);
					}
				}
			}
			$this->resizeImages();
			trace("--FIN optimize--");
				
		}
		
		protected function compressImage($source_url, $destination_url, $quality) {
			$info = getimagesize($source_url);

			if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
			elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
			elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
			
			if (isset($image)) {
				//save file
				imagejpeg($image, $destination_url, $quality);
			}
			//return destination file
			return $destination_url;
		}
		
		public function getFilesFromDir($dirPath) {
			$filesToProcess = array();
			if (file_exists($dirPath)){
				if($handle = opendir($dirPath)) {
					while(false !==($fileName = readdir($handle))) {
						if($fileName != '.' && $fileName !='..') {
							array_push($filesToProcess, $fileName);
						}
					}
				}
			}
			return $filesToProcess;
		}
		
		public function resizeImages() {
			$images_optimized_path = "images/images_optimized/";
			$images_resized_path = "images//images_thumb/";
			
			$filesToProcess = $this->getFilesFromDir($images_optimized_path);
			$min_WidthHeight = 300;
			$max_WidthHeight = 300;
			if (count($filesToProcess)>0) {
				foreach ($filesToProcess as $k=>$fileName) {
					$currentFileName = $fileName;
					$imgUrl = $images_optimized_path.$currentFileName;
					$info = getimagesize($imgUrl);
					if ($info['mime'] == 'image/jpeg') $crop_image = imagecreatefromjpeg($imgUrl);
					elseif ($info['mime'] == 'image/gif') $crop_image = imagecreatefromgif($imgUrl);
					elseif ($info['mime'] == 'image/png') $crop_image = imagecreatefrompng($imgUrl);
					if (!isset($crop_image)) {
						continue;
					}
					
					$orig_w = imagesx($crop_image);
					$orig_h = imagesy($crop_image);

					$output_w = $min_WidthHeight;
					$output_h = $min_WidthHeight;

					if ($orig_w>$min_WidthHeight || $orig_h>$min_WidthHeight) {
						if ($orig_w>$orig_h){			
							$output_w=$orig_w;
							$output_h = $orig_w;
						} else {			
							$output_w=$orig_h;
							$output_h = $orig_h;
						}
					}
					
					if ($output_w>$max_WidthHeight || $output_h>$max_WidthHeight) {
						$rapW = $output_w/$output_h;
						$rapH = $output_h/$output_w;
						if ($output_w>$max_WidthHeight) {
							$output_w = $max_WidthHeight;
							$output_h = $output_w*$rapH;
						}
						if ($output_h>$max_WidthHeight) {
							$output_h = $max_WidthHeight;
							$output_w = $output_h*$rapW;
						}
					}
					
					$scale = 1;
					$new_w = ceil($orig_w * $scale);
					$new_h = ceil($orig_h * $scale);
					
					if ($new_w>$max_WidthHeight || $new_h>$max_WidthHeight) {
						$rapW = $new_w/$new_h;
						$rapH = $new_h/$new_w;
						if ($new_w>$max_WidthHeight) {
							$new_w = $max_WidthHeight;
							$new_h = $new_w*$rapH;
						}
						if ($new_h>$max_WidthHeight) {
							$new_h = $max_WidthHeight;
							$new_w = $new_h*$rapW;
						}
					}
					
					$offest_x = ($output_w - $new_w) / 2;
					$offest_y = ($output_h - $new_h) / 2;

					// create new image and fill with background colour
					$new_img = imagecreatetruecolor($output_w, $output_h);
					$bgcolor = imagecolorallocate($new_img, 255, 255, 255); 
					imagefill($new_img, 0, 0, $bgcolor); // fill background colour

					// copy and resize original image into center of new image
					imagecopyresampled($new_img, $crop_image, $offest_x, $offest_y, 0, 0, $new_w, $new_h, $orig_w, $orig_h);
					imagejpeg($new_img, $images_resized_path.$currentFileName, 100);
					imagedestroy($crop_image);
					imagedestroy($new_img);
					
				}
			}
			trace("--FIN resize images--");
		}
		
	}


	$parser = new Parser();

	switch ($_GET['method']) {
		case 'getDetailsBySKU':
			$sku = trim($_GET['sku']);
			if(isset($_GET['sku'])) {
				$parser->getDetailsBySKU($sku);
			} else {
				die('no sku');
			}
			break;

		case 'getDetails':
			$parser->getDetails();
			break;

		case 'downloadImages':
			$parser->downloadImages();
			break;
			
		case 'optimizeImages':
			$parser->optimizeImages();
			break;
			
		case 'resizeImages':
			$parser->resizeImages();
			break;
			
		case 'recreateFilesFromLocalHtml':
			$parser->recreateFilesFromLocalHtml();
			break;
		case 'createProductsXml':
			$parser->createProductsXml();
			break;
		case 'checkMapsInCsv':
			$parser->checkMapsInCsv();
			break;
		case 'checkMapsInProducts':
			$parser->checkMapsInProducts();
			break;
		case 'checkMapsInFiles':
			$parser->checkMapsInFiles();
			break;
		case 'checkFilesForTables':
			$parser->checkFilesForTables();
			break;

		default:
			die('Choose method');
			break;
	}
	

?>