<?php

ini_set('max_execution_time', 120);
ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED & ~E_WARNING);


$shop = strtolower($_GET['shop']);

$stores = [
    'militari',
    'pantelimon',
    'orhideea',
    'brasov',
    'ploiesti',
    'baneasa',
    'pitesti',
    'arad',
    'oradea',
    'constanta',
    'braila',
    'focsani',
    'suceava',
    'calarasi',
    'drobeta'
];

$zone1 = [
    'militari',
    'pantelimon',
    'orhideea',
    'baneasa',
    'brasov',
    'ploiesti',
    'constanta',
    'suceava',
    'oradea'
];

$zone2 = [
    'pitesti',
    'arad',
    'braila',
    'calarasi',
    'drobeta',
    'focsani'
];


// Login
if (isset($_GET['login'])) {
    session_start();
    if (empty($_SESSION['auth'])) {
        if (isset($_POST['login']) && isset($_POST['password'])) {
            loginUser($_POST['login'], $_POST['password']);
        } else {
            outputLoginBox();
        }
    } else {
        echo("<script>parent.postMessage('logged_in', '*');</script>");
    }
    exit;
}


// Options

// Cache expiration
//define('TTL_CACHE', 3600);
define('TTL_CACHE', 7200);
define('SKIP_TXT_UNTIL_DATE', strtotime('08 May 2015'));
define('SHOW_DEFAULT_STOCKS_UNTIL_DATE', strtotime('05 June 2015'));
define('CAMPAIGN_DAY', strtotime('08 June 2018'));


$cache_xml_path = realpath(dirname(__FILE__) . '/prices');
$default_xml_path = realpath(dirname(__FILE__) . '/defaults');
$manual_xml_path = realpath(dirname(__FILE__) . '/prices');
$export_txt_path = realpath(dirname(__FILE__) . '/../../../dynamicPrices/sourcesTxt');
$export_txt_products_path = realpath(dirname(__FILE__) . '/../../../productUpdates');
$export_info_path = realpath(dirname(__FILE__) . '/prices/info');

$default_xml_files = getDefaultXMLFiles($default_xml_path);
$manual_xml_files = getManualXMLFiles($manual_xml_path);
$kvi_xls_file = realpath(dirname(__FILE__) . '/xlsx/KVI.xlsx');
$arivaj_xls_file = realpath(dirname(__FILE__) . '/xlsx/ARIVAJ.xlsx');
$formula_file = realpath(dirname(__FILE__) . '/xlsx/FORMULA.xlsx');

$includes_price_path = realpath(dirname(__FILE__) . '/../../../includes/price_include.php');

$lastUpdateDate = 0;

// Edit prices
if ($_POST && isset($_GET['edit_price']) && isset($_GET['ref_id'])) {
    global $stores;

    $name = $_POST['name'];
    $value = $_POST['value'];
    $pk = $_POST['pk'];

    $shop = $pk;
    $ref_id = $_GET['ref_id'];

    if (strpos($ref_id, ' ')) {
        $ref_id = str_replace(' ', '+', $ref_id);
    }

    if (!in_array($shop, $stores)) {
        die('No valid shop');
    }

    if (!$ref_id) {
        die('Invalid reference id');
    }

    $manual_xml_file = $manual_xml_files[$shop];
    $manual_refs = [];

    if ($manual_xml_file) {
        $manual_refs = parsePricesXML($manual_xml_file, 'manual');
    } else {
        $manual_xml_file = "{$manual_xml_path}/manual_{$shop}.xml";
    }

    if (!is_array($manual_refs[$ref_id])) {
        $manual_refs[$ref_id] = [];
    }
    $manual_refs[$ref_id]['id'] = $ref_id;
    $manual_refs[$ref_id][$name] = $value;

    $xml = generatePricesFromRefsXML($manual_refs);

    if (file_put_contents($manual_xml_file, $xml, LOCK_EX) === FALSE) {
        header('HTTP/1.0 403 Forbidden');
        echo 'Cannot write to file';
    } else {
        @unlink("$cache_xml_path/cache_{$shop}.xml");
    }

    exit;
}


if (empty($shop) || !in_array($shop, $stores)) {
    die('No valid shop');
}


if ($_GET['ref_id']) {
    $ref_id = $_GET['ref_id'];
    $all_prices = [];

    if (strpos($ref_id, ' ')) {
        $ref_id = str_replace(' ', '+', $ref_id);
    }

    if (isset($_GET['modified'])) {
        foreach ($stores as $store) {
            $all_prices[$store] = getPricesRefsForStore($store, $ref_id);
        }
    } else {
        $all_prices[$shop] = getPricesRefsForStore($shop, $ref_id);
    }

    outputHTML($all_prices, $ref_id);

} elseif (isset($_GET['modified'])) {
    $refs = getPricesRefsForStore($shop);
    foreach ($stores as $store) {
        if ($store === $shop) {
            continue;
        }
        $store_refs = getPricesRefsForStore($store);
        comparePricesRefsForStore($store_refs);
    }
    $xml = generatePricesFromRefsXML($refs);
    outputXML($xml);
} else {
    $cache_file = "$cache_xml_path/cache_{$shop}.xml";
    if (file_exists($cache_file) && (filemtime($cache_file) > time() - TTL_CACHE)) {
        outputXML(file_get_contents($cache_file));
    } else {
        $refs = getPricesRefsForStore($shop);
        $xml = generatePricesFromRefsXML($refs);
        file_put_contents($cache_file, $xml);
        outputXML($xml);
    }
}


function getPricesRefsForStore($store, $ref_id)
{
    global $shop, $zone1, $zone2;
    global $includes_price_path, $export_txt_path,$export_txt_products_path;
    global $kvi_xls_file, $arivaj_xls_file, $formula_file;
    global $default_xml_files, $manual_xml_files;
    global $lastUpdateDate;

    $refs = [];

    $txt_refs = [];
    $kvi_refs = [];
    $arivaj_refs = [];
    $formula_refs = [];


    $default_refs = parsePricesXML($default_xml_files[$store], 'default');

    $manual_refs = parsePricesXML($manual_xml_files[$store], 'manual');
	
	
    if (!empty($includes_price_path)) {
        include($includes_price_path);

        $txt_store = getTXTStore($store);
        $kvi_store = getKVIStore($store);

        $export_txt_pattern = $txt_store . '_export_'.date('Y_m_d') . '*';
        $export_txt_file = getFilesWithPattern($export_txt_path, $export_txt_pattern)[0];
		
        if (empty($export_txt_file)) {
        }

        $pastDays = 1;
        while (empty($export_txt_file)) {
            $export_txt_pattern = $txt_store . '_export_'. date('Y_m_d', strtotime("-$pastDays days")) . '*';
            $export_txt_file = getFilesWithPattern($export_txt_path, $export_txt_pattern)[0];
            $pastDays++;
        }

        $export_txt = parseTXTFile($export_txt_file);
        
        $kvi_xls = parseXLSFile($kvi_xls_file);
        $arivaj_xls = parseXLSFile($arivaj_xls_file);
        $formula_xls = parseXLSFile($formula_file);

        foreach ($formula_xls as $key => $value) {
            if ($key === 0) continue;
            $id = trim($value[0]);
            if (!empty($id)) {
                $formula_refs[$id] = [
                    'id' => $id,
                    'coeficient' => $value[1]
                ];
            }
        }

        foreach ($export_txt as $key => $value) {
            $id = trim($value[0]);
            if (!empty($id)) {
                $txt_refs[$id] = [
                    'id' => $id,
                    'price' => format_price($value[6]),
                    'packagePrice' => array_key_exists($id, $formula_refs) ? format_price($value[6]/$formula_refs[$id]['coeficient']) : format_price($value[8]),
                    'unit' => $value[7],
                    'packageUnit' => $value[9],
                    'stock' => $value[2] ? $value[2] : 0,
                    'color' => getPriceColor('txt')
                ];


                // in curand / stoc epuizat
                if ($txt_refs[$id]['stock'] <= 0) {
                    if (strtotime('midnight') <= CAMPAIGN_DAY) {
                        $txt_refs[$id]['stock'] = '-1';
                    } else {
                        $txt_refs[$id]['stock'] = '0';
                    }
                }

                // if ($txt_refs[$id]['stock'] === '0' && time() < SHOW_DEFAULT_STOCKS_UNTIL_DATE) {
                //     $txt_refs[$id]['stock'] = 'initial';
                // }
            }
        }

        foreach ($kvi_xls as $key => $value) {
            if ($key === 0) {
                // checkKVITemplateXLS($value);
                continue;
            }

            $id = trim($value[0]);
            if (!empty($id)) {
                $kvi_refs[$id] = [
                    'id' => $id,
                    'price' => format_price($value[$kvi_store]),
                    'packagePrice' => format_price($value[$kvi_store + 1]),
                    'color' => getPriceColor('kvi')
                    // 'unit' => null
                ];
            }
        }

        $is_in_zone1 = in_array($shop, $zone1);
        foreach ($arivaj_xls as $key => $value) {
            if ($key === 0) {
                // checkArivajTemplateXLS($value);
                continue;
            }

            $id = trim($value[0]);
            $id = preg_replace('/^\p{Z}+|\p{Z}+$/u', '', $id);
            if (!empty($id)) {
                $arivaj_refs[$id] = [
                    'id' => $id,
                    'price' => $is_in_zone1 ? format_price($value[4]) : format_price($value[8]),
                    'packagePrice' => $is_in_zone1 ? format_price($value[5]) : format_price($value[9]),
                    'unit' => $is_in_zone1 ? $value[7] : $value[11],
                    'packageUnit' => $is_in_zone1 ? $value[6] : $value[10],
                    'color' => getPriceColor('arivaj')
                ];
            }
        }
        
        foreach ($default_refs as $key => $value) {

            $args = [$default_refs, $txt_refs, $kvi_refs, $arivaj_refs, $manual_refs];
            $prodTxtPath = $export_txt_products_path."/".$txt_store.".txt";
            $productInfos = array();
            if (strpos($key, '+')) {
                $ref_parts = explode('+', $key);
                $ref_price = 0;
                $ref_packagePrice = 0;

                foreach ($ref_parts as $ref_part) {
                    array_unshift($args, $ref_part);
                    $ref_value = searchValueByReference($args);
                    $ref_price += floatval($ref_value['price']);
                    $ref_packagePrice += floatval($ref_value['packagePrice']);

                    if(count(getProductInfo($prodTxtPath,$ref_part)) > 1)
                    {
                        $productInfos = getProductInfo($prodTxtPath,$ref_part);
                    }
                }

                $refs[$key]['id'] = $key;
                $refs[$key]['price'] = format_price($ref_price);
                $refs[$key]['packagePrice'] = format_price($ref_packagePrice);
                $refs[$key]['unit'] = 'set';
                $refs[$key]['color'] = getPriceColor('txt');


            } else {
                array_unshift($args, $key);
                $refs[$key] = searchValueByReference($args);
                
                $productInfos = getProductInfo($prodTxtPath,$key);
                
                
                
                
            }

            if(isset($productInfos[1]))
                {
                    $refs[$key]['url'] = $productInfos[1];

                    $tempLastUpdateDate = strtotime($productInfos[2]);

                    if($tempLastUpdateDate > $lastUpdateDate)
                    {
                        $lastUpdateDate = $tempLastUpdateDate;
                    }

                }

            // Remove stock info is is not in arivaj
            if (empty($arivaj_refs[$key])) {
                unset($refs[$key]['stock']);
            } else {
                if (empty($refs[$key]['stock'])) {
                    $refs[$key]['stock'] = 0;
                }
            }

        }

    }


    if (isset($ref_id)) {
        $result = [];
        if (isset($default_refs[$ref_id])) {
            $result['default_ref'] = $default_refs[$ref_id];
        }
        if (isset($manual_refs[$ref_id])) {
            $result['manual_ref'] = $manual_refs[$ref_id];
        }
        if (isset($txt_refs[$ref_id])) {
            $result['txt_ref'] = $txt_refs[$ref_id];
        }
        if (isset($kvi_refs[$ref_id])) {
            $result['kvi_refs'] = $kvi_refs[$ref_id];
        }
        if (isset($arivaj_refs[$ref_id])) {
            $result['arivaj_ref'] = $arivaj_refs[$ref_id];
        }
        return $result;
    }


    return $refs;
}


function comparePricesRefsForStore($store_refs)
{
    global $refs;
    foreach ($refs as $key => $value) {
        $diff = array_diff($value, $store_refs[$key]);
        if (!empty($diff) && (isset($diff['price']) || isset($diff['packagePrice']))) {
            if (!isset($ref[$key]['modified'])) {
                $refs[$key]['modified'] = '1';
            }
        } else {
            $refs[$key]['modified'] = '0';
        }
    }
}


function outputXML($data)
{
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/xml; charset=utf-8');
    echo $data;
    exit;
}


function outputHTML($prices, $ref_id)
{
    global $shop;

    $data  = '
        <html>
            <script src="http://code.jquery.com/jquery-2.0.3.min.js"></script> 
            <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
            <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
            <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
            <style>
                body {
                    background: transparent;
                    margin: 0;
                    padding: 0;
                }
                .table {
                    font-size: 12px;
                    margin-bottom: 0;
                }
                .table > tbody > tr > th {
                    font-weight: normal;
                    padding: 0 5px;
                    text-align: center;
                    vertical-align: middle;
                }
                .table > thead:first-child > tr:first-child > th {
                    text-align: center;
                }
                .modal-header {
                    padding: 7px 15px;
                }
                .modal-body {
                    height: 220px;
                    padding: 0;
                    overflow: hidden;
                    overflow-y: scroll;
                }
                .current {
                    border: 1px solid red;
                }
                .current >  th {
                    border: 0 none !important;
                }
                .current + tr > th {
                    border: 0 none !important;
                }
                .table > thead > tr > th {
                    border-bottom: 0 none;
                }
                .modal-footer {
                    padding: 10px 15px;
                }
                .modal-footer .btn {
                    padding: 3px 8px;
                }
                .form-control {
                    width: 100px !important;
                }
              </style>
              <script>
                var price_changed = false;
                function closeModal() {
                    if (price_changed) {
                        parent.postMessage("price_changed", "*");
                    }
                    parent.postMessage("close_modal", "*");
                }
                function changePrice() {
                    parent.postMessage("price_changed", "*");
                }
            </script>
        <body>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" onClick="closeModal()"><span>&times;</span></button>
                        <h4 class="modal-title">Prices</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Shop</th>
                                    <th style="width: 81px;">Legend</th>
                                    <th>Default</th>
                                    <th>TXT</th>
                                    <th>KVI</th>
                                    <th>Arivaj</th>
                                    <th>Manual</th>
                                </tr>
                            </thead>
                            <tbody>';

    foreach ($prices as $key => $value) {
        $data .= '
                                <tr class="'. (($key === $shop) ? 'current' : 'not_current') .'">
                                    <th style="font-size: 14px;">'. ucfirst($key) .'</th>
                                    <th style="width: 81px; font-size: 11px;">
                                        <div>Price</div>
                                        <div>Package Price</div>
                                        <div>Unit</div>
                                    </th>
                                    <th>
                                        <div>'. (($value['default_ref']['price']) ? $value['default_ref']['price'] : 'N/A') .'</div>
                                        <div>'. (($value['default_ref']['packagePrice']) ? $value['default_ref']['packagePrice'] : 'N/A') .'</div>
                                        <div>'. (($value['default_ref']['unit']) ? $value['default_ref']['unit'] : 'N/A') .'</div>
                                    </th>
                                    <th>
                                        <div>'. (($value['txt_ref']['price']) ? $value['txt_ref']['price'] : 'N/A') .'</div>
                                        <div>'. (($value['txt_ref']['packagePrice']) ? $value['txt_ref']['packagePrice'] : 'N/A') .'</div>
                                        <div>'. (($value['txt_ref']['unit']) ? $value['txt_ref']['unit'] : 'N/A') .'</div>
                                    </th>
                                    <th>
                                        <div>'. (($value['kvi_ref']['price']) ? $value['kvi_ref']['price'] : 'N/A') .'</div>
                                        <div>'. (($value['kvi_ref']['packagePrice']) ? $value['kvi_ref']['packagePrice'] : 'N/A') .'</div>
                                        <div>'. (($value['kvi_ref']['unit']) ? $value['kvi_ref']['unit'] : 'N/A') .'</div>
                                    </th>
                                    <th>
                                        <div>'. (($value['arivaj_ref']['price']) ? $value['arivaj_ref']['price'] : 'N/A') .'</div>
                                        <div>'. (($value['arivaj_ref']['packagePrice']) ? $value['arivaj_ref']['packagePrice'] : 'N/A') .'</div>
                                        <div>'. (($value['arivaj_ref']['unit']) ? $value['arivaj_ref']['unit'] : 'N/A') .'</div>
                                    </th>
                                    <th>
                                        <div>
                                            '. ($value['default_ref']['unit'] === 'set' ? 'N/A' : '
                                                <a href="#" title="Edit Price" class="editable" data-name="price" data-pk="'. $key .'" data-value="'. $value['manual_ref']['price'] .'">
                                                    '. (($value['manual_ref']['price']) ? $value['manual_ref']['price'] : 'N/A') .'
                                                </a>
                                            ') .'
                                        </div>
                                        <div>
                                            '. ($value['default_ref']['unit'] === 'set' ? 'N/A' : '
                                                <a href="#" title="Edit Price" class="editable" data-name="packagePrice" data-pk="'. $key .'" data-value="'. $value['manual_ref']['packagePrice'] .'">
                                                    '. (($value['manual_ref']['packagePrice']) ? $value['manual_ref']['packagePrice'] : 'N/A') .'
                                                </a>
                                            ') .'
                                        </div>
                                        <div>'. (($value['manual_ref']['unit']) ? $value['manual_ref']['unit'] : 'N/A') .'</div>
                                    </th>
                                </tr>';
    }
    $data .= '
                            </tbody>
                        </table>';

    $data .= '
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" onClick="closeModal()">Close</button>
                        <!-- <button type="button" class="btn btn-primary" onClick="changePrice()">Save changes</button> -->
                    </div>
                </div>
            </div>
                <script>
                    $(document).ready(function() {
                        $.fn.editable.defaults.mode = "popup";
                        $.fn.editable.defaults.ajaxOptions = {type: "POST"};
                        $(".editable").editable({
                            placement: "left",
                            url: "price.php?ref_id='. $ref_id .'&edit_price",
                            emptytext: "N/A",                            
                            success: function(response, newValue) {
                                price_changed = true;
                            }
                        });
                        $(".modal-body").scrollTop($(".current").offset().top - 100);
                    });
                </script>

        </body>
        </html>';


    header('Content-Type: text/html; charset=utf-8');
    echo $data;

    exit;
}


function outputLoginBox()
{
    $data  = '<html>';
    $data .= '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">';
    $data .= '<body>';
    $data .= '<style>
                body {
                  margin: 0;
                  padding: 0;
                  background-color: transparent;
                }
                :focus {
                    outline: 0 none;
                }
                form {
                    margin: 0;
                }
                .modal-body h5 {
                    text-align: center;
                    color: red;
                }
                .form-signin {
                  max-width: 330px;
                  padding: 15px;
                  margin: 0 auto;
                }
                .form-signin .form-signin-heading {
                    text-align: center;
                }
                .form-signin .form-signin-heading,
                .form-signin .checkbox {
                  margin-bottom: 10px;
                }
                .form-signin .checkbox {
                  font-weight: normal;
                }
                .form-signin .form-control {
                  position: relative;
                  height: auto;
                  -webkit-box-sizing: border-box;
                     -moz-box-sizing: border-box;
                          box-sizing: border-box;
                  padding: 10px;
                  font-size: 14px;
                  margin-bottom: 10px !important;
                }
                .form-signin .form-control:focus {
                  z-index: 2;
                }
                .form-signin input[type="text"] {
                  margin-bottom: -1px;
                  border-bottom-right-radius: 0;
                  border-bottom-left-radius: 0;
                }
                .form-signin input[type="password"] {
                  margin-bottom: 10px;
                  border-top-left-radius: 0;
                  border-top-right-radius: 0;
                }
                
            </style>

            <script>
                function closeModal() {
                    parent.postMessage("close_modal", "*");
                }
            </script>

            <div tabindex="-1" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="closeModal()"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Please sign in</h4>
                  </div>

                  <form method="post" action="'. $_SERVER['PHP_SELF'] .'?login">
                      <div class="modal-body">
                        '. (isset($_GET['err']) ? '<h5>Invalid credentials</h5>' : '') .'
                        <div class="form-signin">
                        <input type="text" name="login" class="form-control" placeholder="Login" required autofocus>
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="closeModal()">Close</button>
                        <button type="submit" class="btn btn-primary">Login</button>
                      </div>
                  </form>

                </div>
              </div>
            </div>';


    $data .= '</body>';
    $data .= '</html>';

    header('Content-Type: text/html; charset=utf-8');
    echo $data;
}


function checkUser()
{
}


function loginUser($username, $password)
{
    if ($username == 'admin' && $password == 'prestimedia') {
        session_start();
        $_SESSION['auth'] = 1;
        echo("<script>parent.postMessage('logged_in', '*')</script>");
    } else {
        header("Location: ". $_SERVER['PHP_SELF'] ."?login&err");
    }
}


function getDefaultXMLFiles($path)
{
    global $stores;
    $files = [];
    foreach ($stores as $store) {
        $filePath = realpath($path ."/default_{$store}.xml");
        if ($filePath && file_exists($filePath)) {
            $files[$store] = $filePath;
        }
    }
    return $files;
}

function getProductInfo($path,$ref)
{
    
    $filePath = realpath($path);

    $content = file_get_contents($filePath);

    preg_match("/\d+\,$ref,(.*),(.*)/", $content, $output_array);
   
    return $output_array;
}


function getManualXMLFiles($path)
{
    global $stores;
    $files = [];
    foreach ($stores as $store) {
        $filePath = realpath($path ."/manual_{$store}.xml");
        if ($filePath && file_exists($filePath)) {
            $files[$store] = $filePath;
        }
    }
    return $files;
}


function parsePricesXML($filePath, $type)
{
    if (empty($filePath)) {
        return [];
    }

    $doc = new DOMDocument();
    $doc->load($filePath);
    $path = new DOMXPath($doc);
    $refs = $path->query('//prices/p');

    $prices = [];

    foreach($refs as $ref) {
        if ($ref->hasAttribute('id')) {
            $id = trim($ref->getAttribute('id'));
            $prices[$id] = [
                'id' => $id,
                'price' => trim($ref->getAttribute('price')),
                'packagePrice' => trim($ref->getAttribute('packagePrice')),
                'unit' => trim($ref->getAttribute('unit')),
                'unit2' => trim($ref->getAttribute('unit2')),
                'packageUnit' => trim($ref->getAttribute('packageUnit')),
                'color' => getPriceColor($type),
                'priceColor' => trim($ref->getAttribute('priceColor'))
            ];
            if($ref->getAttribute('stock')) {
                $prices[$id]['stock'] = trim($ref->getAttribute('stock'));
            }
        }
    }

    return $prices;
}


function parseTXTFile($filePath, $delimiter = ';')
{
    $refs = [];

    if (time() < SKIP_TXT_UNTIL_DATE) {
        return $refs;
    }

    if (($handle = fopen($filePath, 'r')) !== FALSE) {
        while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
            $refs[] = $data;
        }
        fclose($handle);
    }

    return $refs;
}


function parseXLSFile($filePath)
{
    if (empty($filePath)) {
        return [];
    }

    $reader = PHPExcel_IOFactory::createReaderForFile($filePath);
    $reader->setReadDataOnly(true);

    $excel = $reader->load($filePath);
    $sheet = $excel->getSheet(0)->toArray();

    return $sheet;
}


function checkKVITemplateXLS($value)
{
    // print_r($value);
}


function checkArivajTemplateXLS($value)
{
    // print_r($value);
}


function getKVIStore($store)
{
    switch ($store) {
        case 'militari': return 2;
        case 'pantelimon': return 4;
        case 'orhideea': return 6;
        case 'brasov': return 8;
        case 'ploiesti': return 10;
        case 'pitesti': return 12;
        case 'arad': return 14;
        case 'oradea': return 16;
        case 'constanta': return 18;
        case 'braila': return 20;
        case 'focsani': return 22;
        case 'suceava': return 24;
        case 'calarasi': return 26;
        case 'drobeta': return 28;
        default: return 2;
    }
}


function getTXTStore($store)
{
    switch ($store) {
        case 'militari': return '701';
        case 'pantelimon': return '702';
        case 'orhideea': return '703';
        case 'brasov': return '704';
        case 'ploiesti': return '705';
        case 'baneasa': return '706';
        case 'constanta': return '707';
        case 'pitesti': return '708';
        case 'braila': return '709';
        case 'arad': return '710';
        case 'focsani': return '711';
        case 'suceava': return '712';
        case 'oradea': return '713';
        case 'drobeta': return '715';
        case 'calarasi': return '716';
        default: return '701';
    }
}

function getPriceColor($source)
{
    switch ($source) {
        case 'manual': return '#ff1493'; // pink
        case 'arivaj': return '#9acd32'; // green
        case 'kvi': return '#a0522d'; // brown
        case 'txt': return '#4169e1'; // blue
        case 'default': return '#ffa500'; // orange
        default: return '#ffa500'; // orange
     }
}


function getFilesWithPattern($path, $pattern)
{
    $files = array();
    foreach (glob("{$path}/{$pattern}") as $file) {
        $files[] = $file;
    }
    return $files;
}


function generatePricesFromRefsXML($refs)
{
    global $lastUpdateDate;

    $dom = new DOMDocument('1.0', 'utf-8');
    $dom->formatOutput = true;
    $dom->preserveWhiteSpace = false;

    $prices_el = $dom->createElement('prices');
    $dom->appendChild($prices_el);

    $prices_date_el = $dom->createElement('prices_date');
    $prices_el->appendChild($prices_date_el);
    $prices_date_el->appendChild($dom->createCDATASection("Preturi actualizate in data de ".date('d.m.Y',$lastUpdateDate).", ora ".date('H:i',$lastUpdateDate)));


    foreach ($refs as $ref) {
        $p_el = $dom->createElement('p');

        $p_attr_id = $dom->createAttribute('id');
        $p_attr_id->value = $ref['id'];
        $p_el->appendChild($p_attr_id);

        if (isset($ref['price'])) {
            $p_attr_price = $dom->createAttribute('price');
            $p_attr_price->value = $ref['price'];
            $p_el->appendChild($p_attr_price);
        }

        if (isset($ref['packagePrice'])) {
            $p_attr_packagePrice = $dom->createAttribute('packagePrice');
            $p_attr_packagePrice->value = $ref['packagePrice'];
            $p_el->appendChild($p_attr_packagePrice);
        }

        if (isset($ref['unit'])) {
            $p_attr_unit = $dom->createAttribute('unit');
            $p_attr_unit->value = $ref['unit'];
            $p_el->appendChild($p_attr_unit);
        }
        if (isset($ref['unit2'])) {
            $p_attr_unit = $dom->createAttribute('unit2');
            $p_attr_unit->value = $ref['unit2'];
            $p_el->appendChild($p_attr_unit);
        }

        if (isset($ref['packageUnit'])) {
            $p_attr_packageUnit = $dom->createAttribute('packageUnit');
            $p_attr_packageUnit->value = $ref['packageUnit'];
            $p_el->appendChild($p_attr_packageUnit);
        }

        if (isset($ref['stock'])) {
            $p_attr_stock = $dom->createAttribute('stock');
            $p_attr_stock->value = $ref['stock'];
            $p_el->appendChild($p_attr_stock);
        }

        if (isset($ref['modified'])) {
            $p_attr_modified = $dom->createAttribute('modified');
            $p_attr_modified->value = $ref['modified'];
            $p_el->appendChild($p_attr_modified);
        }

        if (isset($ref['color'])) {
            $p_attr_color = $dom->createAttribute('color');
            $p_attr_color->value = $ref['color'];
            $p_el->appendChild($p_attr_color);
        }
        if (isset($ref['priceColor'])) {
            $p_attr_color = $dom->createAttribute('priceColor');
            $p_attr_color->value = $ref['priceColor'];
            $p_el->appendChild($p_attr_color);
        }
        if (isset($ref['url'])) {
            $p_attr_color = $dom->createAttribute('url');
            $p_attr_color->value = $ref['url'];
            $p_el->appendChild($p_attr_color);
        }

        $prices_el->appendChild($p_el);
    }

    return $dom->saveXML();
}


function searchValueByReference($args)
{
    $ref = $args[0];
    $txt_ref = $args[2];

    $value = [];

    foreach ($args as $key => $arr) {
        if ($key === 0) {
            continue;
        }

        if (is_array($arr[$ref])) {
            $value = array_merge($value, array_filter($arr[$ref], function($value) {
                return ($value || is_numeric($value));
            }));
        }
    }

    return $value;
}


function is_decimal($val)
{
    return is_numeric($val) && floor($val) != $val;
}


function format_price($val)
{
    $val = trim($val);
    return is_decimal($val) ? number_format($val, 2) : $val;
}


?>