<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Catalog";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div id="homepage_container" class="">

    <!-- ------------------------------------------------------------------
           SECTION 0
       ------------------------------------------------------------------ -->

    <section class="page-1-ambianta" style="background-image: url('assets/img/HP/ambianta-generala.jpg');">
        <div class="col-xs-12 page-wrapper">
            <div class="col-xs-12 col-sm-12 col-md-12" __style="margin-left: 4%;">
                <h1>
                    Toamnă afară,<br>atmosferă de vară<br>în casa ta
                </h1>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-6" style="margin-bottom: 4%;">
                <div class="page-text-wrapper">
                    <p>
                        Despre ce e toamna pentru tine? Covor de frunze ruginii, dimineți zgribulite și eșarfe multicolore, apusuri roșiatice și plimbări romantice? Sau poate e despre o transformare? Natura însăși se transformă și se pregătește de iarnă. Tu te-ai pregătit? Dacă nu ai făcut-o încă, descoperă proiectele din catalogul Brico Depôt. Vei găsi aici soluții și idei potrivite pentru tine.
                    </p>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 text-center" style="margin-bottom: 4%;">
                <a href="#section-1" class="discover">Descoperă <img src="assets/img/scroll_down_icon.png"> proiectele</a>
            </div>
            <!-- <a href="#section-1" class="discover">Descoperă <img src="assets/img/scroll_down_icon.png"> proiectele</a> -->
        </div>
        <br style="clear: both;">

    </section>

    <!-- ------------------------------------------------------------------
            bannner
        ------------------------------------------------------------------ -->
    <section class="banner" id="banner" style="margin:10px auto;">
        <div class="col-xs-12" style="padding:0">
            <a href="https://www.bricodepot.ro/militari/preturi-mici-zilnic">
                <img class="img-responsive" src="assets/img/HP/banner.jpg" style="width:100%;"/>
            </a>
        </div>
    </section>


    <!-- ------------------------------------------------------------------
          SECTION 1
      ------------------------------------------------------------------ -->

    <section class="catalog_masonry_container" id="section-2">
        <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

            <div class="block" data-size="2x2" data-size-768="2x2" data-size-480="2x2.5">
                <div class="home project_description project_1 " onclick="embedZoom('assets/img/HP/ambianta-1.jpg')">

                    <div class="project_description_header">
                        <div class="col-xs-12 col-sm-5 col-md-4 project_description_title home">
                            <p>Unelte de sezon</p>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-8 project_description_details home">
                            Nu doar oamenii sunt sensibili la capriciile vremii, ci și plantele. Vremea rece și schimbătoare din lunile de toamnă reprezintă o adevărată provocare pentru grădina ta. Alege dintr-o gamă variată de unelte pentru tuns iarba, tăiat crengile uscate sau curățat pământul de frunze. Nu uita să te aprovizionezi și cu lemne pentru iarnă, nu știi când se va așterne prima zăpadă!
                        </div>
                    </div>

                    <div class="project_description_background" style="background-image: url('assets/img/HP/ambianta-1.jpg');">
                        <img class="hd_icon" src="assets/img/hd-icon.png">
                    </div>

                </div>
            </div>

            <div class="block" data-size="2x1">
                <a class="project" href="pregateste-ti-gradina-pentru-iarna">
                    <div class="project_block corner_2x1 corner_color_project_1" style="background-image: url('assets/img/HP/ambianta-1.1.jpg');">
                        <p>
                            Pregătește-ți grădina <br/> pentru iarnă
                        </p>
                    </div>
                </a>
            </div>
            <div class="block" data-size="2x1">
                <a class="project" href="pregateste-te-de-sezonul-rece">
                    <div class="project_block corner_2x1 corner_color_project_1" style="background-image: url('assets/img/HP/ambianta-1.2.jpg');">
                        <p>
                            Pregătește-te de <br/> sezonul rece
                        </p>
                    </div>
                </a>
            </div>
        </div>
    </section>

    <!-- ------------------------------------------------------------------
           SECTION 2
       ------------------------------------------------------------------ -->

    <section class="catalog_masonry_container" id="section-3">
        <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

            <div class="block" data-size="2x2" data-size-768="2x2" data-size-480="2x2.5">
                <div class="home project_description project_2 " onclick="embedZoom('assets/img/HP/ambianta-2.jpg')">

                    <div class="project_description_header">
                        <div class="col-xs-12 col-sm-5 col-md-4 project_description_title home">
                            <p>Soluții de încălzire potrivite pentru tine</p>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-8 project_description_details home">
                            E din nou sezonul șosetelor pufoase și puloverelor groase în casă! Dacă te gândești să-ți pui chiar mănuși pentru a te încălzi, poate că e timpul să ne faci o vizită. Consultă mai întâi și gama variată de soluții de încălzire din catalog, adaptată nevoilor tale, ca să vii la sigur. Electric sau pe lemne? Ce stil preferi? Hai să descoperim împreună!
                        </div>
                    </div>

                    <div class="project_description_background" style="background-image: url('assets/img/HP/ambianta-2.jpg');">
                        <img class="hd_icon" src="assets/img/hd-icon.png">
                    </div>

                </div>
            </div>

            <div class="block" data-size="2x1">
                <a class="project" href="descopera-solutii-de-incalzire-electrica">
                    <div class="project_block corner_2x1 corner_color_project_2" style="background-image: url('assets/img/HP/ambianta-2.1.jpg');">
                        <p>Descoperă soluții de<br/> încălzire electrică</p>
                    </div>
                </a>
            </div>
            <div class="block" data-size="2x1">
                <a class="project" href="alege-incalzirea-pe-lemne">
                    <div class="project_block corner_2x1 corner_color_project_2" style="background-image: url('assets/img/HP/ambianta-2.2.jpg');">
                        <p>Alege încăzirea<br/> pe lemne</p>
                    </div>
                </a>
            </div>

        </div>
    </section>

    <!-- ------------------------------------------------------------------
           SECTION 3
       ------------------------------------------------------------------ -->

    <section class="catalog_masonry_container" id="section-4">
        <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">


            <div class="block" data-size="2x2" data-size-768="2x2" data-size-480="2x2.5">
                <div class="home project_description project_2 " onclick="embedZoom('assets/img/HP/ambianta-3.jpg')">

                    <div class="project_description_header">
                        <div class="col-xs-12 col-sm-5 col-md-4 project_description_title home">
                            <p>Sisteme electrice și iluminat</p>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-8 project_description_details home">
                            O casă bună este, în primul rând, funcțională. Trăim în secolul tehnologiei, avem dispozitive și echipamente care ne ușurează munca și ne ajută să ne relaxăm. Te-ai gândit, însă, când ai făcut ultima oară o verificare a sistemului electric? De când nu ai mai schimbat la față prizele sau întrerupătoarele? Hai în magazinele noastre, ți-am pregătit soluții adaptate si sisteme de iluminat potrivite nevoilor tale.
                        </div>
                    </div>

                    <div class="project_description_background" style="background-image: url('assets/img/HP/ambianta-3.jpg');">
                        <img class="hd_icon" src="assets/img/hd-icon.png">
                    </div>

                </div>
            </div>
         

            <div class="block" data-size="2x1">
                <a class="project" href="alege-un-sistem-electric-eficient">
                    <div class="project_block corner_2x1 corner_color_project_3" style="background-image: url('assets/img/HP/ambianta-3.1.jpg');">
                        <p>Alege un sistem<br/> electric eficient</p>
                    </div>
                </a>
            </div>
            <div class="block" data-size="2x1">
                <a class="project" href="decoreaza-ti-casa-in-stilul-tau">
                    <div class="project_block corner_2x1 corner_color_project_3" style="background-image: url('assets/img/HP/ambianta-3.2.jpg');">
                        <p>Decorează-ți casa<br/> în stilul tău</p>
                    </div>
                </a>
            </div>

        </div>
    </section>

</div>

<?php
require_once("assets/partials/modules_templates.php");
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

</body>
</html>