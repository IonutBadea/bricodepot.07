<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Descoperă soluții de încălzire electrică";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Soluții de încălzire potrivite pentru tine</a></li>
        <li class="active"><span>Descoperă soluții de încălzire electrică</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
                SECTION 0
            ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header" >
                    <div class="project_description_title">
                        Descoperă soluții de încălzire electrică
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            Diminețile sunt mai reci și stai zgribulit(ă) în halat și pijama, cât prepari micul dejun și faci cafeaua în bucătărie. Ți-e rece în baie cât te speli pe dinți. Ai vrea să fie cald tot timpul, dar nu ai suficient spațiu pentru un radiator, un calorifer sau o aerotermă. Și poate că nu ai nici bugetul necesar, acum, pentru o astfel de investiție. În plus, un echipament de încălzire sigur nu se va potrivi cu noul design al apartamentului sau casei tale.
                        </p>
                        <p>
                            Ne-am gândit la nevoile tale și credem că am găsit cele mai bune soluții pentru tine. Atât ca design, culori, eficiență, cât și ca preț. Gamele de produse de încălzire electrică sunt eficiente energetic și o să te convingi de acest lucru când vei primi factura la energie. În plus, poți regla  temperatura, după preferințe și necesități. Economisești bani, dar te bucuri și de un confort sporit pe toată perioada sezonului rece. Sau pur și simplu când ți-e doar frig.
                        </p>
                        <p>
                            Te invităm să descoperi produsele și accesoriile noastre, concepute pentru spații mici sau generoase, dar și pentru designer-ul din tine.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top: 0px">
                    <a href="#start" class="discover">Descoperă <img
                            src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_2.1/ambianta-proiect-2.1.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->


        <!-- ------------------------------------------------------------------
                 SECTION 1
             ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('131131,131132,131133,131134')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('131131'); ?>">
                    <div class="yellow_hotspot" data-ref="131131" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">RADIATOR ULEI 9 ELEMENŢI 2 000 W</p>
                    </div>
                </a>
            </div>
        </div>


        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "RADIATOR CUARŢ 1000 W",
                "ref" => "135488",
                "sticker" =>'sticker_alte-optiuni.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "CONVECTOR TURBO 3000 W*",
                "ref" => "135468",
                "sticker" => 'sticker_alte-optiuni.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "AEROTERMA INFRAROSU",
                "ref" => "138125",
                "sticker" => 'sticker_alte-optiuni.png',
                "badge" => "badge-preturi-mici.jpg",
                "price" => array(
                    'old_price' => '99.00',
                    'unit' => 'buc',
                    'currency' => 'Lei'
                )
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "ÎNCĂLZITOR PTC SUSPENDAT PE PERETE 2000 W IP22",
                "ref" => "143681",
                "sticker" => 'sticker_alte-optiuni.png',
                "badge" => "badge-preturi-mici.jpg",
                "price" => array(
                    'old_price' => '149.00',
                    'unit' => 'buc',
                    'currency' => 'Lei'
                )
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 2
         ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SOBA GAZ GREENGEAR 4200 W",
                "ref" => "143689",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "AEROTERMĂ INDUSTRIALĂ 3000 W",
                "ref" => "135476",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "AEROTERMĂ 2000 W NEGRU DESIGN CU FUNCŢIE DE OSCILARE",
                "ref" => "143674",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/incalzire-racire-si-instalatii/elemente-incalzire-mobila.html";
            require('assets/partials/discover.php');
            ?>
        </div>


        <!-- ------------------------------------------------------------------ -->


        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRELUNGITOR 3 PRIZE ALB 13 A / 16 A CABLU DE 2 M H05VVF 3G1,5MM2 FĂRĂ COMUTATOR",
                "ref" => "141021",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRELUNGITOR 3 PRIZE ALB 13 A / 16 A CABLU DE 4 M H05VVF 3G1,5MM2 FĂRĂ COMUTATOR",
                "ref" => "141022",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRELUNGITOR 3X16A 7M ALB",
                "ref" => "141023",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PRELUNGITOR 4 PRIZE ALB 13 A / 16 A CABLU DE 2 M H05VVF 3G1,5MM2 CU COMUTATOR",
                "ref" => "141025",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>


    </div>

    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>