<!DOCTYPE html>
<html>
<head>
    <?php
    $title = "Decorează-ți casa în stilul tău";
    require_once("assets/partials/head.php");
    ?>
</head>
<body>

<?php
require_once("assets/partials/menu.php");
?>

<div class="app_nav app_breadcrumbs">
    <ol class="breadcrumb breadcrumb-arrow">
        <li><a href="https://www.bricodepot.ro/catalog/">HOME</a></li>
        <li><a href="#">Sisteme electrice și iluminat</a></li>
        <li class="active"><span>Decorează-ți casa în stilul tău</span></li>
    </ol>
</div>

<div id="homepage_container" class="">


    <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">

        <!-- ------------------------------------------------------------------
            SECTION 0
        ------------------------------------------------------------------ -->

        <!-- 2x2 - 2x2 HEADER -->
        <div class="block" data-size="2x2" data-size-768="2xauto" style="float:right;">
            <div class="project_description discover mobilier">
                <div class="project_description_header">
                    <div class="project_description_title">
                        Decorează-ți casa în stilul tău
                    </div>
                    <div class="project_description_details" autoshrink>
                        <p>
                            Niciodată nu e prea multă lumină în casă. Cu atât mai mult toamna, când diminețile sunt mai întunecate și ziua e mult mai scurtă. Iar uneori simți că ai nevoie de și mai multă lumină să-ți poți duce activitățile la bun sfârșit. Știm că, în același timp, te gândești și la factura de energie.
                            Acum le poți îmbina. Lumină mai multă, factură redusă. În funcție de preferințele și nevoile tale, poți opta pentru aplice, plafoniere sau lustre, dotate cu becuri LED. Becurile LED sunt foarte populare, pentru că nu consumă mult, au efecte de lumină atractivă și te pot ajuta să iluminezi o zonă anume foarte ușor.
                        </p>
                        <p>
                            O atmosferă romantică reușită are neapărat nevoie și de un iluminat adecvat. Te invităm în magazine, de unde poți alege sursele de iluminat potrivite pentru o personalizare completă a casei tale.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 text-center" style="top: 0px">
                    <a href="#start" class="discover">Descoperă <img
                            src="assets/img/brown_scroll_down_icon.png"> proiectele</a>
                </div>
            </div>
        </div>

        <div class="block" data-size="2x2">
            <!-- <div class="map ext-module-js" data-module="Map" data-bind-to="map" data-option-data="assets/data/maps/map.xml" data-option-size="1000|1000"> -->
            <img class="map__image img-responsive" src="assets/img/PROJECT_3.2/ambianta-proiect.3.2.jpg" style="width: 100%; height:100%;"/>
            <!-- </div> -->
        </div>
        <!-- END 2x2 - 2x2 HEADER -->


        <!-- ------------------------------------------------------------------
             SECTION 1
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('138432,138431,138430,138433')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('138432'); ?>">
                    <div class="yellow_hotspot" data-ref="138432" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">APHAEA SPOT 3 X E14 METAL/STICLĂ</p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LIBERTAS 3XE14 APLICĂ PĂTRATĂ CU PICĂTURI DIN STICLĂ",
                "ref" => "138096",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "VACUNA PLAFONIERĂ 3 X E14 28 W CROM",
                "ref" => "138086",
                "alternative" => array("138419"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DIALL E14 REFLECTOR 40 W 2700 K PACHET DE 2 BUCĂŢI",
                "ref" => "145496",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DIALL E14 LUMÂNARE 40 W 2700 K 1 BUCATĂ",
                "ref" => "144259",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 2
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('138088,138091,138087,138089')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('138088'); ?>">
                    <div class="yellow_hotspot" data-ref="138088" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">CAELUS M1 PLAFONIERĂ 6 X G9 40 W CROM</p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "GLACIES PLAFONIERĂ 5 X G9 40 W CROM",
                "ref" => "138253",
                "alternative" => array("138254"),
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "SPIRA PLAFONIERĂ 3 X G9 METAL CROM",
                "ref" => "138137",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DIALL E27 GLS 60 W 4000K 1 BUCATĂ",
                "ref" => "144274",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DIALL G9 CAPSULĂ 28W 2700 K PACHET DE 2 BUCĂŢI",
                "ref" => "144280",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>

        <!-- ------------------------------------------------------------------
             SECTION 3
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('138255,138251,138252')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('138255'); ?>">
                    <div class="yellow_hotspot" data-ref="138255" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">MANTUS PLAFONIERĂ 1 X E27 60 W CROM </p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PARCAE 3XG9 PLAFONIERĂ CU MĂRGELE DIN STICLĂ CU BAZĂ SEMIÎNCHISĂ",
                "ref" => "138097",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "AULA 1XE27 PLAFONIECA CU ABAJUR DIN METAL",
                "ref" => "138485",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DIALL G9 CAPSULĂ 20W 4000K PACHET DE 2 BUCĂŢI",
                "ref" => "145168",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "LED DECORATIV DIALL E27 SFERĂ 120MM 40W 1800 K",
                "ref" => "145165",
                "sticker" => 'sticker_nu-uita.png'
            ));
            ?>
        </div>


        <!-- ------------------------------------------------------------------
             SECTION 4
         ------------------------------------------------------------------ -->

        <!-- 2x2 -->
        <div class="block" data-size="2x2">
            <div class="map">
                <a href="javascript:nop()" class="noZensmooth" onclick="embedProducts('140792,140793')">
                    <img class="map__image" src="<?php echo optimizedImageProduct('140792'); ?>">
                    <div class="yellow_hotspot" data-ref="140792" style="right: 5%; bottom: 5%;">
                        <p class="title">#pret <span>Lei/#unit.</span></p>
                        <p class="desc">HESTIA PLAFONIERĂ LED 18,5 W ALB NEUTRU</p>
                    </div>
                </a>
            </div>
        </div>

        <!-- ------------------------------------------------------------------ -->

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "PLAFONIERĂ LED LETO",
                "ref" => "138777",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "APLICA DAVENPORT LED IP44 18CM",
                "ref" => "142395",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>
        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            product_card(array(
                "title" => "APLICA SHEYE LED IP65 28CM",
                "ref" => "142397",
                "sticker" => 'sticker_recomandam.png'
            ));
            ?>
        </div>

        <!-- 1x1 -->
        <div class="block" data-size="1x1">
            <?php
            $title = "DESCOPERĂ GAMA<br/>COMPLETĂ PE BRICODEPOT.RO";
            $link = "https://www.bricodepot.ro/#{V7_Store}/iluminat.html?filtre/categorie/iluminat-interior-deco,becuri-si-tuburi-luminoase";
            require('assets/partials/discover.php');
            ?>
        </div>

        <!--  ------------------------------------------------------------------ -->

    </div>

    <?php
    require_once("assets/partials/modules_templates.php");
    ?>

</div>

<?php
require_once("assets/partials/scroll_top.php");
require_once("assets/partials/map.php");
require_once("assets/partials/scripts.php");
?>

<script>
</script>
</body>
</html>