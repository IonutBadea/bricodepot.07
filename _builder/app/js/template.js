


var Template = {


	generateHeaderString: function generateHeaderString (block) {

		var header_text = header_text_template;
		if (block.data.title) {
			header_text = header_text.replace('##title', block.data.title);
		}
		if (block.data.text) {
			header_text = header_text.replace('##details', block.data.text);
		}

		return header_text;
	},

	generateProductString: function generateProductString (block) {

		var product_text = product_template;
		if (block.data.ref) {
			product_text = product_text.replace('##ref', block.data.ref);
		}
		if (block.data.title) {
			product_text = product_text.replace('##title', block.data.title);
		}

		return product_text;
	},

	generateArticleString: function generateArticleString (block) {

		var article_text = article_template;
		if (block.data.title) {
			article_text = article_text.replace('##title', block.data.title);
		}
		if (block.data.text) {
			article_text = article_text.replace('##details', block.data.text);
		}
		if (block.data.url) {
			article_text = article_text.replace('##url', block.data.url);
		}

		return article_text;
	},

	generateVideoYoutubeString: function generateVideoYoutubeString (block) {

		var video_text = video_youtube_template;
		if (block.data.url) {
			video_text = video_text.replace('##src', block.data.url);
		}

		return video_text;
	},

	generateBlocksString: function generateBlocksString (items) {

		var blocks_string = '';

		var indentation = '            ';

		items.forEach(function (item, row_index) {

			blocks_string += indentation + '<!-- row ' + (row_index + 1) + ' -->\n';

			item.blocks.forEach(function (block, block_index) {
				
				var start = '';
				if (row_index === 1 && block_index === 0) {
					var start = ' id="start"';
				}

				var float = '';
				if (block.type === 'header:text') {
					float = ' style="float: right;"'
				}

				if (block.inside === 'top') {
					blocks_string += indentation + '<div class="block" data-size="1x2"' + start +'>\n';
					blocks_string += indentation + '    <div class="inside1x2" style="margin-bottom: 20px; padding:0 !important;">\n    ';
				} else if (block.inside === 'bottom') {
					blocks_string += indentation + '    <div class="inside1x2" style="padding:0 !important;">\n    ';
				} else {
					blocks_string += indentation + '<div class="block" data-size="' + block.size + '"' + start + float +'>\n';
				}

				switch (block.type) {

					case 'header:image':
						blocks_string += header_image_template;
						break;

					case 'header:text':
						blocks_string += Template.generateHeaderString(block);
						break;

					case 'product':
						blocks_string += Template.generateProductString(block);
						break;

					case 'article':
						blocks_string += Template.generateArticleString(block);
						break;

					case 'video:youtube':
						blocks_string += Template.generateVideoYoutubeString(block);
						break;

					case 'empty':
						blocks_string += indentation + '    \n';
						break;

					default: {
						blocks_string += indentation + '    <div style="width: 100%; height: 100%; background: #ccc;">' + block.size + '</div>\n';
						break;
					}
				}

				if (block.inside === 'top') {
					blocks_string += indentation + '    </div>\n';
				} else if (block.inside === 'bottom') {
					blocks_string += indentation + '    </div>\n';
					blocks_string += indentation + '</div>\n\n';
				} else {
					blocks_string += indentation + '</div>\n\n';
				}
			});

			blocks_string += indentation + '<!-- END row ' + (row_index + 1) + ' -->\n';
			blocks_string += '\n';
		});

		return blocks_string;
	},
};