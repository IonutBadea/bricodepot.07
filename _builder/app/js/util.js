


var Util = {

	getTemplateCloneWithId: function getTemplateCloneWithId (templates, id) {

		var index = 0,
			total_templates = templates.length;
		for ( ; index < total_templates; index++) {

			var template = templates[index];
			if (template.id == id) {

				var blocks = [];
				template.blocks.forEach(function (block) {
					blocks.push({
						size: block.size + '',
						type: block.type,
						inside: block.inside,
						data: block.data
					});
				});

				var clone = {
					id: template.id,
					title: template.title,
					class: template.class,
					blocks: blocks
				};

				return clone;
			}
		}
	},

	generateSlug: function generateSlug (title) {

		var slug = title.toLowerCase();

		slug = slug.trim();

		slug = slug.replace(/ă|â/gmi, 'a');
		slug = slug.replace(/î/gmi, 'i');
		slug = slug.replace(/ș/gmi, 's');
		slug = slug.replace(/ț/gmi, 'ț');

		slug = slug.replace(/,|\.|;|:|!|\?/gmi, '');

		slug = slug.replace(/ /gmi, '-');

		return slug;
	},
};