

var main_app = new Vue({
    
    el: '#app',
    
    data: {
        items: [],
        presets: presets,
        templates: templates,
        types: types,
        category: '',
        subcategory: '',
        result: ''
    },
    
    methods: {

        addPreset: function (template_id) {
            
            var self = this;

            var template = Util.getTemplateCloneWithId(self.presets, template_id);
            self.items.push(template);
        },

        addTemplate: function (template_id) {
            
            var self = this;

            var template = Util.getTemplateCloneWithId(self.templates, template_id);
            self.items.push(template);
        },

        removeTemplate: function (item_to_remove) {

            var self = this;

            var index_to_remove = -1;

            var index = 0,
                total_items = self.items.length;
            for ( ; index < total_items; index++) {

                var item = self.items[index];
                if (item == item_to_remove) {
                    index_to_remove = index;
                    break;
                }
            }

            if (index_to_remove !== -1) {
                self.items.splice(index_to_remove, 1);
            }
        },

        generate: function () {

            var self = this;

            var page_string = page_template;

            page_string = page_string.replace('##title', self.category);
            page_string = page_string.replace('##category', self.category);
            page_string = page_string.replace('##subcategory', self.subcategory);

            var blocks_string = Template.generateBlocksString(self.items);
            page_string = page_string.replace('##blocks', blocks_string);

            self.result = page_string;
        },

        download: function () {

            var self = this;

            if (!self.result) {
                self.generate();
            }

            var blob = new Blob([self.result], {type: "text/plain;charset=utf-8"}),
                filename = Util.generateSlug(self.category || 'categorie');
            saveAs(blob, filename + '.php');
        },

        clear: function () {

            var self = this;

            self.category    = '';
            self.subcategory = '';
            self.items  = [];
            self.result = '';
        }
    }
})