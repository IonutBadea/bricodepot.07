
var article_template = '                <div class="project_description inspiratie" style="background-image: url(\'##src\');">\n\
                    <div>\n\
                        <div class="project_description_header">\n\
                            <div class="col-xs-12 col-sm-4 col-md-2 project_description_title">\n\
                                <img class="img-responsive" src="assets/img/bricosfat-logo.png">\n\
                            </div>\n\
                            <div class="col-xs-12 col-sm-8 col-md-10 project_description_details">\n\
                                ##details\n\
                            </div>\n\
                        </div>\n\
                        <div class="project_description_subtitle">\n\
                            ##title\n\
                        </div>\n\
                    </div>\n\
                    <div style="text-align: center;">\n\
                        <a href="#" onclick="openArticol(\'##url\')">\n\
                            <div class="project_read">\n\
                                Citește articolul\n\
                            </div>\n\
                        </a>\n\
                    </div>\n\
                </div>\n';


var video_youtube_template = '                <div class="project_description inspiratie">\n\
                    <iframe width="100%" height="100%" src="##src" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>\n\
                </div>\n';