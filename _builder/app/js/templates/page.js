
var page_template = '<!DOCTYPE html>\n\
<html>\n\
<head>\n\
    <?php\n\
    $title = "##title";\n\
    require_once("assets/partials/head.php");\n\
    ?>\n\
</head>\n\
<body>\n\
    <?php\n\
    require_once("assets/partials/menu.php");\n\
    breadcrumb((object)[\n\
        \'category\' => \'##category\',\n\
        \'subcategory\' => \'##subcategory\'\n\
    ]);\n\
    ?>\n\
    <div id="homepage_container" class="">\n\
        <div id="grid" class="blocks ext-module-js" data-module="BlocksGrid" data-options-id="options">\n\
##blocks\n\
        </div>\n\
        <?php\n\
        require_once("assets/partials/modules_templates.php");\n\
        ?>\n\
    </div>\n\
    <?php\n\
    require_once("assets/partials/scroll_top.php");\n\
    require_once("assets/partials/map.php");\n\
    require_once("assets/partials/scripts.php");\n\
    ?>\n\
    <script>\n\
    </script>\n\
</body>\n\
</html>';