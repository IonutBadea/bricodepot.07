
var header_text_template = '                <div class="project_description discover mobilier">\n\
                    <div class="project_description_header">\n\
                            <div class="project_description_title">\n\
                            ##title\n\
                        </div>\n\
                        <div class="project_description_details">\n\
                            <p>##details</p>\n\
                        </div>\n\
                    </div>\n\
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">\n\
                        <a href="#start" class="discover">Descoperă <img src="assets/img/brown_scroll_down_icon.png"> proiectele</a>\n\
                    </div>\n\
                </div>\n';

var header_image_template = '                    \
<img class="map__image img-responsive" src="##src" style="width: 100%"/>\n';