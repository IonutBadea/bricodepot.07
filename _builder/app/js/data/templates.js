var templates = [
    {
        id: 0,
        title: '2x2 | 2x2',
        class: 't_2x2-2x2',
        blocks: [
            {
                size: '2x2',
                data: {}
            },
            {
                size: '2x2',
                data: {}
            }
        ]
    },
    {
        id: 1,
        title: '1x2 | 3x2',
        class: 't_1x2-3x2',
        blocks: [
            {
                size: '1x2',
                data: {}
            },
            {
                size: '3x2',
                data: {}
            }
        ]
    },
    {
        id: 2,
        title: '3x2 | 1x2',
        class: 't_3x2-1x2',
        blocks: [
            {
                size: '3x2',
                data: {}
            },
            {
                size: '1x2',
                data: {}
            }
        ]
    },
    {
        id: 3,
        title: '1x1 | 1x1 | 1x1 | 1x1',
        class: 't_1x1-1x1-1x1-1x1',
        blocks: [
            {
                size: '1x1',
                data: {}
            },
            {
                size: '1x1',
                data: {}
            },
            {
                size: '1x1',
                data: {}
            },
            {
                size: '1x1',
                data: {}
            }
        ]
    },
    {
        id: 4,
        title: '1x1 - 1x1 | 2x2 | 1x1 - 1x1',
        class: 't_1x11x1_2x2_1x11x1',
        blocks: [
            {
                size: '1x1',
                inside: 'top',
                data: {}
            },
            {
                size: '1x1',
                inside: 'bottom',
                data: {}
            },
            {
                size: '2x2',
                data: {}
            },
            {
                size: '1x1',
                data: {}
            },
            {
                size: '1x1',
                data: {}
            }
        ]
    },
    {
        id: 5,
        title: '4x2',
        class: 't_4x2',
        blocks: [
            {
                size: '4x2',
                data: {}
            }
        ]
    },
];