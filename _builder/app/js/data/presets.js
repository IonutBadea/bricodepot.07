
var presets = [
    {
        id: 0,
        title: 'Header (Imagine și Descriere + descoperă): 2x2 | 2x2',
        class: 't_2x2-2x2',
        blocks: [
            {
                size: '2x2',
                type: 'header:text',
                data: {}
            },
            {
                size: '2x2',
                type: 'header:image',
                data: {}
            }
        ]
    },
    {
        id: 1,
        title: 'Articol: 4x2',
        class: 't_4x2',
        blocks: [
            {
                size: '4x2',
                type: 'article',
                data: {}
            }
        ]
    },
    {
        id: 2,
        title: 'Video Youtube: 4x2',
        class: 't_4x2',
        blocks: [
            {
                size: '4x2',
                type: 'video:youtube',
                data: {}
            }
        ]
    },
];