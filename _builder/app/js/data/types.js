
var types = [
    {
        text: 'Gol',
        value: 'emtpty'
    },
    {
        text: 'Produs',
        value: 'product'
    },
    {
        text: 'Map simplu',
        value: 'map:simple'
    },
    {
        text: 'Map cu hotspot',
        value: 'map:hotspot'
    },
    {
        text: 'Descriere',
        value: 'description'
    },
    {
        text: 'Descopera',
        value: 'more'
    },
    {
        text: 'Imagine',
        value: 'image'
    },
    {
        text: 'Text header',
        value: 'header:text'
    },
    {
        text: 'Imagine header',
        value: 'header:image'
    },
    {
        text: 'Articol',
        value: 'article'
    },
    {
        text: 'Video YouTube',
        value: 'video:youtube'
    }
];